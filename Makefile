# maven vars:
mvn=mvn
test=true
mvn_flags=

# set maven test flags depending on value of "test" variable
ifneq ($(test), true)
 $(info Not performing tests!)
 mvn_test_flags = -Dmaven.test.skip=true
endif

all: pdfutils intf core client gui

pdfutils:
	$(mvn) clean install -f ../PdfUtils/pom.xml $(mvn_test_flags)

intf:
	$(mvn) clean install -f ../TranskribusInterfaces/pom.xml -Dmaven.test.skip=true

core:	
	$(mvn) clean install -f ../TranskribusCore/pom.xml $(mvn_test_flags)

client:
	$(mvn) clean install -f ../TranskribusClient/pom.xml $(mvn_test_flags)

gui:
	$(mvn) clean install $(mvn_test_flags)
	
# codesigns Transkribus.exe and Transkribus-*.jar file
codesign:
	./codesign_exe.sh
	#./codesign_jar.sh
	
# unpacks jre's, changes permissions for scripts and creates .zip packages
package:
	$(mvn) antrun:run@unzip_jre $(mvn_test_flags)
	$(mvn) antrun:run@change_permissions_for_scripts $(mvn_test_flags)
	$(mvn) assembly:single $(mvn_test_flags)
	
# creates a complete, codesigned package for releases (or also snapshots)
codesigned_package: gui codesign package
		
deploy:
	$(mvn) antrun:run # this copies the currently built (!) version to the deploy folders
		
clean:
	$(mvn) clean
	
#start_gui:
#	TODO
