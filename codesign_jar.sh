#!/bin/bash

cd target
jarfile=$(ls *.jar | head -n 1)
if [ -z "$jarfile" ]; then
    echo "No jarfile found to sign!"
    exit 1
fi
echo "Signing jar: $jarfile"

jarsigner -tsa http://ts.ssl.com -providerClass sun.security.pkcs11.SunPKCS11 -providerArg X:/TRP/config/code_signing_certificate/yubikey-pkcs11-java.cfg -keystore NONE -storetype PKCS11 $jarfile "Certificate for PIV Authentication"
