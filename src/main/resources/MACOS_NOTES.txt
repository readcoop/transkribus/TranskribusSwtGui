NOTES FOR RUNNING THE APPLICATION IN MACOS
------------------------------------------

- unzip the application bundle anywhere you do have write access and open the folder
- start Transkribus by double-clicking either the file "Transkribus" or "Transkribus.command"
 	- note that providing a regular MacOS application bundle was not possible anymore as security 
 	constraints of MacOS are preventing it to start properly
- if you get a warning that Transkribus is an unverified application, 
	you have to start if via the context-menu the first time:
	- ctrl-click the file (or open the context menu on it using a double-finger click)
	- in the context menu that is appearing, click "Open"
- if during startup you get another warning that the integrated java is also an unsigned application, 
	repeat the process from above for the "java" file in the subfolder jre/zulu<...>/bin
- if you don't want to use the integrated jre or it is still not working, just remove the entire "jre" subfolder 
	and install Java >= 11 systemwide from here: https://www.oracle.com/java/technologies/downloads/
- during the first startup, the program has to setup itself for the OS that is used - at the end of the
	console log it will say "Performed initial SWT re-configuration for java version > 8
	Now trying to restart the program - restart manually if nothing happens!" - close the window and restart
	it again to start the UI
- To access the starter file inside your applications folder, create an alias for "Transkribus.command" 
	(context-menu -> create alias) and move that alias into the applications folder