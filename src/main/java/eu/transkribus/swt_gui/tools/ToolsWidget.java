package eu.transkribus.swt_gui.tools;

import java.util.List;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.internal.SWTEventListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.ui.forms.events.ExpansionAdapter;
import org.eclipse.ui.forms.events.ExpansionEvent;
import org.eclipse.ui.forms.widgets.ExpandableComposite;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.transkribus.core.model.beans.Text2ImageConf;
import eu.transkribus.core.model.beans.TrpTranscriptMetadata;
import eu.transkribus.core.model.beans.enums.EditStatus;
import eu.transkribus.core.util.CoreUtils;
import eu.transkribus.swt.util.Fonts;
import eu.transkribus.swt.util.Images;
import eu.transkribus.swt.util.LabeledCombo;
import eu.transkribus.swt.util.SWTUtil;
import eu.transkribus.swt_gui.dialogs.ChooseTranscriptDialog;
import eu.transkribus.swt_gui.htr.ModelTrainingComposite;
import eu.transkribus.swt_gui.htr.TextRecognitionComposite;
import eu.transkribus.swt_gui.la.LayoutAnalysisComposite;
import eu.transkribus.swt_gui.mainwidget.storage.Storage;
import eu.transkribus.swt_gui.util.CurrentTranscriptOrCurrentDocPagesSelector;

public class ToolsWidget extends Composite {
	private final static Logger logger = LoggerFactory.getLogger(ToolsWidget.class);

	LayoutAnalysisComposite laComp;
	Button startLaBtn;

	TextRecognitionComposite trComp;
	ModelTrainingComposite modelTrComp;

	Button polygon2baselinesBtn, baseline2PolygonBtn, p2palaBtn, duButton, fieldsBtn;
	Button b2pSimplifyPolygonsCheck;
	LabeledCombo b2pBackendCombo;
	CurrentTranscriptOrCurrentDocPagesSelector otherToolsPagesSelector;

	Button t2iBtn;

	TranscriptVersionChooser refVersionChooser, hypVersionChooser;

	Button computeAdvancedBtn, compareSamplesBtn;
	Button compareVersionsBtn;
	Composite werGroup;
	ExpandableComposite werExp, laToolsExp, expRecog, expOther, modelTrainingExp;

	Composite container; // this is the base container, where all expandable composite are put into
	Composite otherToolsContainer, p2palaContainer, t2iContainer, duContainer, otherOtherToolsGroup;
	ScrolledComposite sc;

	public static class TranscriptVersionChooser extends Composite {
		public Button useCurrentBtn;
		public Button chooseVersionBtn;

		public TrpTranscriptMetadata selectedMd;

		public TranscriptVersionChooser(String label, Composite parent, int style) {
			super(parent, style);

			this.setLayout(new GridLayout(4, false));

			Label l = new Label(this, 0);
			l.setText(label);

			chooseVersionBtn = new Button(this, SWT.PUSH);
			chooseVersionBtn.setText("Choose...");
			chooseVersionBtn.setToolTipText("Click to choose another transcript version...");

			useCurrentBtn = new Button(this, SWT.PUSH);
			useCurrentBtn.setText("Use current");
			useCurrentBtn.setToolTipText("Click to use the currently opened transcript version");

			useCurrentBtn.addSelectionListener(new SelectionAdapter() {
				@Override public void widgetSelected(SelectionEvent e) {
					setToCurrent();
				}
			});

			chooseVersionBtn.addSelectionListener(new SelectionAdapter() {
				@Override public void widgetSelected(SelectionEvent e) {
					chooseTranscriptVersion();
				}
			});

			updateSelectedVersion();
		}

		public void updateSelectedVersion() {
			String l = selectedMd == null ? "Choose..." : getTranscriptLabel(selectedMd);

			if(!SWTUtil.isDisposed(chooseVersionBtn)){
				chooseVersionBtn.setText(l);
				chooseVersionBtn.pack();
				layout();
			}
		}

		public void chooseTranscriptVersion() {
			ChooseTranscriptDialog d = new ChooseTranscriptDialog(getShell());
			if (d.open() != Dialog.OK)
				return;

			TrpTranscriptMetadata md = d.getTranscript();
			if (md == null) {
				logger.debug("selected version was null...");
				return;
			}

			selectedMd = md;
			updateSelectedVersion();
		}

		public void setToCurrent() {
			if (Storage.getInstance().hasTranscript()) {
				selectedMd = Storage.getInstance().getTranscriptMetadata();
				updateSelectedVersion();
			}
		}
		public void setToGT() {
			List<TrpTranscriptMetadata> transcripts = Storage.getInstance().getTranscriptsSortedByDate(true, -1);
			if (Storage.getInstance().hasTranscript()) {
				for (TrpTranscriptMetadata version : transcripts) {
					if (version.getStatus() == EditStatus.GT) {
						selectedMd = version;
						updateSelectedVersion();
						return;
					}
				}
				if(transcripts.size() >= 2) {
					selectedMd = transcripts.get(1);
				}else{
					selectedMd = Storage.getInstance().getTranscriptMetadata();
				}
				updateSelectedVersion();
			}
		}
	}

	public ToolsWidget(Composite parent, int style) {
		super(parent, style);
		this.setLayout(SWTUtil.createGridLayout(1, false, 0, 0));

		sc = new ScrolledComposite(this, SWT.V_SCROLL | SWT.H_SCROLL);
		sc.setLayoutData(new GridData(GridData.FILL_BOTH));
		sc.setLayout(SWTUtil.createGridLayout(1, false, 0, 0));

		container = new Composite(sc, SWT.NONE);
		container.setLayout(new GridLayout(1, false));
		container.setLayoutData(new GridData(GridData.FILL_BOTH));

//		Label ncsrIconL = new Label(SWTUtil.dummyShell, 0);
//		ncsrIconL.setImage(ncsrIcon);

		initModelTrainingTools(container);
		initLayoutAnalysisTools(container);
		initRecogTools(container);
		initWerGroup(container);

		initOtherTools(container);

		sc.setContent(container);
	    sc.setExpandHorizontal(true);
	    sc.setExpandVertical(true);
//	    sc.setMinSize(container.computeSize(SWT.DEFAULT, SWT.DEFAULT));
//	    sc.setMinSize(container.computeSize(0, SWT.DEFAULT));
	    layoutContainer();
	}

	private void layoutContainer() {
		container.layout();
	    sc.setMinSize(0, container.computeSize(SWT.DEFAULT, SWT.DEFAULT).y-1);
//	    sc.setMinSize(container.computeSize(0, SWT.DEFAULT));
	}

	public String getSelectedLaMethod() {
		return laComp.getSelectedMethod();
	}

	private void initModelTrainingTools(Composite container) {
		modelTrainingExp = new ExpandableComposite(container, ExpandableComposite.COMPACT);
		modelTrainingExp.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		Composite grp = new Composite(modelTrainingExp, SWT.SHADOW_ETCHED_IN);
		grp.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1));
		grp.setLayout(new GridLayout(2, false));

		modelTrComp = new ModelTrainingComposite(grp, 0);
		modelTrComp.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 2, 1));

		modelTrainingExp.setClient(grp);
		modelTrainingExp.setText("Model Training");
		modelTrainingExp.setExpanded(true);
		Fonts.setBoldFont(modelTrainingExp);
		modelTrainingExp.addExpansionListener(new ExpansionAdapter() {
			public void expansionStateChanged(ExpansionEvent e) {
				layoutContainer();
			}
		});

	}

	private void initLayoutAnalysisTools(Composite container) {
		laToolsExp = new ExpandableComposite(container, ExpandableComposite.COMPACT);
		laToolsExp.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		Composite laToolsGroup = new Composite(laToolsExp, SWT.SHADOW_ETCHED_IN);
		laToolsGroup.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1));
		laToolsGroup.setLayout(new GridLayout(2, false));

		laComp = new LayoutAnalysisComposite(laToolsGroup, 0);
		laComp.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 2, 1));

		startLaBtn = new Button(laToolsGroup, 0);
		startLaBtn.setText("Run");
		startLaBtn.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		startLaBtn.setImage(Images.ARROW_RIGHT);

		laToolsExp.setClient(laToolsGroup);
		laToolsExp.setText("Layout Analysis");
		laToolsExp.setExpanded(true);
		Fonts.setBoldFont(laToolsExp);
		laToolsExp.addExpansionListener(new ExpansionAdapter() {
			public void expansionStateChanged(ExpansionEvent e) {
				layoutContainer();
			}
		});
	}

	private void initRecogTools(Composite container) {
		expRecog = new ExpandableComposite(container, ExpandableComposite.COMPACT);
		expRecog.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

		trComp = new TextRecognitionComposite(expRecog, 0);
		trComp.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 3, 1));

		expRecog.setClient(trComp);
		expRecog.setText("Text Recognition");
		Fonts.setBoldFont(expRecog);
		expRecog.setExpanded(true);
		expRecog.addExpansionListener(new ExpansionAdapter() {
			public void expansionStateChanged(ExpansionEvent e) {
				layoutContainer();
			}
		});
	}


	private void initOtherTools(Composite container) {
		expOther = new ExpandableComposite(container, ExpandableComposite.COMPACT);
		expOther.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		otherToolsContainer = new Composite(expOther, SWT.SHADOW_ETCHED_IN);
		otherToolsContainer.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		otherToolsContainer.setLayout(new GridLayout(1, true));

		p2palaContainer = new Composite(otherToolsContainer, 0);
		p2palaContainer.setLayoutData(new GridData(GridData.FILL_BOTH));
		p2palaContainer.setLayout(SWTUtil.createGridLayout(1, false, 0, 0));

		p2palaBtn = new Button(p2palaContainer, SWT.PUSH);
		p2palaBtn.setText("P2PaLA...");
		p2palaBtn.setToolTipText("Creates regions with structure tags and baselines from pre-trained models");
		p2palaBtn.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

		t2iContainer = new Composite(otherToolsContainer, 0);
		t2iContainer.setLayoutData(new GridData(GridData.FILL_BOTH));
		t2iContainer.setLayout(SWTUtil.createGridLayout(1, false, 0, 0));
		t2iBtn = new Button(t2iContainer, SWT.PUSH);
		t2iBtn.setText("Text2Image...");
		t2iBtn.setToolTipText("Tries to match the text contained in the transcriptions to a line segmentation");
		t2iBtn.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		t2iBtn.setData(new Text2ImageConf());

		duContainer = new Composite(otherToolsContainer, 0);
		duContainer.setLayoutData(new GridData(GridData.FILL_BOTH));
		duContainer.setLayout(SWTUtil.createGridLayout(1, false, 0, 0));
		duButton = new Button(duContainer, SWT.PUSH);
		duButton.setText("Document Understanding...");
		duButton.setToolTipText("Automatically adds annotations.");
		duButton.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

		fieldsBtn = new Button(otherToolsContainer, 0);
		fieldsBtn.setText("Fields...");
		fieldsBtn.setToolTipText("Start a Fields recognition job...");
		fieldsBtn.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

		otherOtherToolsGroup = new Group(otherToolsContainer, 0);
		otherOtherToolsGroup.setLayoutData(new GridData(GridData.FILL_BOTH));
		otherOtherToolsGroup.setLayout(new GridLayout(1, true));

		otherToolsPagesSelector = new CurrentTranscriptOrCurrentDocPagesSelector(otherOtherToolsGroup, SWT.NONE, true,true);
		otherToolsPagesSelector.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

		polygon2baselinesBtn = new Button(otherOtherToolsGroup, SWT.PUSH);
		polygon2baselinesBtn.setText("Add Baselines to Polygons");
		polygon2baselinesBtn.setToolTipText("Creates baselines for all surrounding polygons - warning: existing baselines will be lost (text is retained however!)");
		polygon2baselinesBtn.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		polygon2baselinesBtn.setVisible(false); // FIXME not working currently -> set button invisible

		Composite b2pContainer = new Composite(otherOtherToolsGroup, 0);
		b2pContainer.setLayout(SWTUtil.createGridLayout(3, false, 0, 0));
		b2pContainer.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

		baseline2PolygonBtn = new Button(b2pContainer, SWT.PUSH);
		baseline2PolygonBtn.setText("Add Polygons to Baselines");
		baseline2PolygonBtn.setToolTipText("Creates polygons for all baselines - warning: existing polygons will be lost (text is retained however!)");
		baseline2PolygonBtn.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));

		b2pSimplifyPolygonsCheck = new Button(b2pContainer, SWT.CHECK);
		b2pSimplifyPolygonsCheck.setText("Simplify Polygons");
		b2pSimplifyPolygonsCheck.setToolTipText("Return simplified polygons instead of the original ones");
		b2pSimplifyPolygonsCheck.setSelection(true);

		b2pBackendCombo = new LabeledCombo(b2pContainer, "Method: ");
		b2pBackendCombo.setItems(new String[] { "Legacy", "Legacy_berlin" } );
		b2pBackendCombo.select(0);
		b2pBackendCombo.setToolTipText("The method to use for baseline-to-polygon computation");

		expOther.setClient(otherToolsContainer);
		new Label(otherToolsContainer, SWT.NONE);
		expOther.setText("Other Tools");
		Fonts.setBoldFont(expOther);
		expOther.setExpanded(true);
		expOther.addExpansionListener(new ExpansionAdapter() {
			public void expansionStateChanged(ExpansionEvent e) {
				layoutContainer();
			}
		});

	}

	public void setDuVisible(boolean visible) {
		logger.debug("setting du visible: "+visible);
		if (visible) {
			duContainer.setParent(otherToolsContainer);
			duContainer.moveAbove(otherOtherToolsGroup);
		} else {
			duContainer.setParent(SWTUtil.dummyShell);
		}
		layoutContainer();
	}

	public void setP2PalaVisible(boolean visible) {
        logger.debug("setting P2PaLA visible: {}", visible);
		if (visible) {
			p2palaContainer.setParent(otherToolsContainer);
            p2palaContainer.moveAbove(otherOtherToolsGroup);
		} else {
            p2palaContainer.setParent(SWTUtil.dummyShell);
		}
		layoutContainer();
	}

	public void setFieldsVisible(boolean visible) {
		logger.debug("setFieldsVisible = "+visible);
		if (visible) {
			fieldsBtn.setParent(otherToolsContainer);
			fieldsBtn.moveAbove(otherOtherToolsGroup);
		} else {
			fieldsBtn.setParent(SWTUtil.dummyShell);
		}
		layoutContainer();
	}

	public void setT2IVisible(boolean visible) {
		logger.debug("t2iVisible = "+visible);
		if (visible) {
			t2iContainer.setParent(otherToolsContainer);
			t2iContainer.moveAbove(otherOtherToolsGroup);
		} else {
			t2iContainer.setParent(SWTUtil.dummyShell);
		}
		layoutContainer();
	}

	private void initWerGroup(Composite container) {
		werExp = new ExpandableComposite(container, ExpandableComposite.COMPACT);
		werExp.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		werGroup = new Composite(werExp, SWT.SHADOW_ETCHED_IN);
		werGroup.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1));
//		metadatagroup.setText("Document metadata");
		werGroup.setLayout(new GridLayout(2, false));

		refVersionChooser = new TranscriptVersionChooser("Reference:\n(Correct Text) ", werGroup, 0);
		refVersionChooser.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 2, 1));

		hypVersionChooser = new TranscriptVersionChooser("Hypothesis:\n(HTR Text) ", werGroup, 0);
		hypVersionChooser.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 2, 1));

//		computeWerBtn.pack();

		compareVersionsBtn = new Button(werGroup, SWT.PUSH);
		compareVersionsBtn.setText("Compare Text Versions...");
		compareVersionsBtn.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, true, 2, 1));
		compareVersionsBtn.setToolTipText("Shows the difference of the two selected versions");

//		computeWerBtn = new Button(werGroup, SWT.PUSH);
//		computeWerBtn.setText("Quick Compare");
//		computeWerBtn.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, true, 0, 1));
//		computeWerBtn.setToolTipText("Compares the two selected transcripts and computes word error rate and character error rate.");

		computeAdvancedBtn = new Button(werGroup,SWT.PUSH);
		computeAdvancedBtn.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, true, 2, 1));
		computeAdvancedBtn.setText("Compare...");

		compareSamplesBtn = new Button(werGroup, SWT.PUSH);
		compareSamplesBtn.setText("Compare Samples...");
		compareSamplesBtn.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, true, 2, 1));
		compareSamplesBtn.setToolTipText("Shows the difference of the two selected versions");

		werExp.setClient(werGroup);
		werExp.setText("Compute Accuracy...");
		Fonts.setBoldFont(werExp);
		werExp.setExpanded(true);
		werExp.addExpansionListener(new ExpansionAdapter() {
			public void expansionStateChanged(ExpansionEvent e) {
				layoutContainer();
			}
		});
	}

	public Button getCompareVersionsBtn() {
		return compareVersionsBtn;
	}

	public void updateVisibility(boolean setEnabled){
		werExp.setExpanded(setEnabled);
		werExp.setEnabled(setEnabled);
		laToolsExp.setExpanded(setEnabled);
		laToolsExp.setEnabled(setEnabled);
		laComp.updateSelectionChooserForLA();
		expOther.setExpanded(setEnabled);
		expOther.setEnabled(setEnabled);
		trComp.getRunBtn().setEnabled(setEnabled);
		layoutContainer();
	}

	public TrpTranscriptMetadata getCorrectText(){
		return this.refVersionChooser.selectedMd;
	}

	public TrpTranscriptMetadata getHpothesisText(){
		return this.hypVersionChooser.selectedMd;
	}

	public static String getTranscriptLabel(TrpTranscriptMetadata t) {
		final String labelStr = CoreUtils.newDateFormatUserFriendly().format(t.getTime())
				+ " - " + t.getUserName()
				+ " - " + t.getStatus().getStr()
				+ (t.getToolName() != null ? " - " + t.getToolName() : "");

		return labelStr;
	}

}
