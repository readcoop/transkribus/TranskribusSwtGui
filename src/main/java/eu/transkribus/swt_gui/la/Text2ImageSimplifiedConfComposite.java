package eu.transkribus.swt_gui.la;

import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.transkribus.core.model.beans.DocSelection;
import eu.transkribus.core.model.beans.Text2ImageConf;
import eu.transkribus.core.util.ModelUtil;
import eu.transkribus.swt.util.LabeledText;
import eu.transkribus.swt.util.SWTUtil;
import eu.transkribus.swt_gui.models.ModelChooserButton;
import eu.transkribus.swt_gui.util.CurrentTranscriptOrDocPagesOrCollectionSelector;

public class Text2ImageSimplifiedConfComposite extends Composite {
	private static final Logger logger = LoggerFactory.getLogger(Text2ImageSimplifiedConfComposite.class);

	CurrentTranscriptOrDocPagesOrCollectionSelector pagesSelector;
	ModelChooserButton baseModelBtn;
	
	Button performLaBtn;
	Button keepUnmatchedLinesBtn;
	Button preserveLineOrderBtn;
	Button writeSimilarityTagBtn;
	Button keepLineBreaks;
	LabeledText blockThreshTxt;
	LabeledText lineThreshTxt;
	Button setDefaultsBtn;
	
	public Text2ImageSimplifiedConfComposite(Composite parent, Text2ImageConf conf) {
		super(parent, 0);
		int nCols = 2;
		this.setLayout(new GridLayout(nCols, false));
		
		pagesSelector = new CurrentTranscriptOrDocPagesOrCollectionSelector(this, SWT.NONE, false, true, true);		
		pagesSelector.setLayoutData(new GridData(SWT.FILL, SWT.BOTTOM, true, false, 1, 1));
		
		baseModelBtn = new ModelChooserButton(this, true, ModelUtil.TYPE_TEXT, ModelUtil.PROVIDER_PYLAIA, "Base model: ");
		baseModelBtn.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, nCols, 1));
		
		if (true) {
		performLaBtn = new Button(this, SWT.CHECK);
		performLaBtn.setText("Perform Layout Analysis");
		performLaBtn.setToolTipText("Perform a new layout analysis for text alignment - uncheck to use the existing layout");
		performLaBtn.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, nCols, 1));
		}
		
		keepUnmatchedLinesBtn = new Button(this, SWT.CHECK);
		keepUnmatchedLinesBtn.setText("Keep unmatched lines");
		keepUnmatchedLinesBtn.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, nCols, 1));
		
		preserveLineOrderBtn = new Button(this, SWT.CHECK);
		preserveLineOrderBtn.setText("Preserve line order");
		preserveLineOrderBtn.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, nCols, 1));	
		
		writeSimilarityTagBtn = new Button(this, SWT.CHECK);
		writeSimilarityTagBtn.setText("Write similarity tag");
		writeSimilarityTagBtn.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, nCols, 1));	
		
		keepLineBreaks = new Button(this, SWT.CHECK);
		keepLineBreaks.setText("Keep the linebreaks");
		keepLineBreaks.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, nCols, 1));
		keepLineBreaks.setToolTipText("If linebreaks are given the t2i method tries to match complete text lines into the image lines.");

		blockThreshTxt = new LabeledText(this,  "Region threshold: ");
		blockThreshTxt.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, nCols, 1));
		
		lineThreshTxt = new LabeledText(this,  "Line threshold: ");
		lineThreshTxt.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, nCols, 1));
		
		setDefaultsBtn = new Button(this, 0);
		setDefaultsBtn.setText("Set defaults");
		setDefaultsBtn.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, nCols, 1));
		
		SWTUtil.onSelectionEvent(setDefaultsBtn, e -> {
			setUiFromGivenConf(null);
		});
		
		setUiFromGivenConf(conf);
	}
	
	private void setUiFromGivenConf(Text2ImageConf conf) {
		Text2ImageConf defaultConf = new Text2ImageConf();
		if (conf == null) {
			conf = new Text2ImageConf();
		}

		if (baseModelBtn != null) {
			baseModelBtn.setModel(conf.getModel());	
		}
		if (performLaBtn != null && conf.getParams().getBoolParam(Text2ImageConf.PERFORM_LA_PARAM)!=null) {
			performLaBtn.setSelection(conf.getParams().getBoolParam(Text2ImageConf.PERFORM_LA_PARAM));
		}
		
		keepUnmatchedLinesBtn.setSelection(conf.getParams().getBoolParam(Text2ImageConf.KEEP_UNMATCHED_LINES_PARAM));
		writeSimilarityTagBtn.setSelection(conf.getParams().getBoolParam(Text2ImageConf.WRITE_SIMILARITY_TAG_PARAM));
		keepLineBreaks.setSelection(conf.getParams().getBoolParam(Text2ImageConf.KEEP_LINE_BREAKS_PARAM));
		blockThreshTxt.setText(conf.getParams().getParameterValue(Text2ImageConf.BLOCK_THRESH_PARAM) != null ? conf.getParams().getParameterValue(Text2ImageConf.BLOCK_THRESH_PARAM) : defaultConf.getParams().getParameterValue(Text2ImageConf.BLOCK_THRESH_PARAM));
		lineThreshTxt.setText(conf.getParams().getParameterValue(Text2ImageConf.LINE_THRESH_PARAM) != null ? conf.getParams().getParameterValue(Text2ImageConf.LINE_THRESH_PARAM) : defaultConf.getParams().getParameterValue(Text2ImageConf.LINE_THRESH_PARAM));
		
	}
	
	public Text2ImageConf getConfigFromUi() throws IllegalArgumentException {
		Text2ImageConf conf = new Text2ImageConf();
		
		conf.setCurrentTranscript(pagesSelector.isCurrentTranscript());
		conf.setPagesStr(pagesSelector.getPagesStr());
		
		conf.setDocsSelection(pagesSelector.isDocsSelection());
		conf.setDocsSelected(pagesSelector.getDocSelections());
		
		if (baseModelBtn!=null) {
			conf.setModel(baseModelBtn.getModel());
			if (conf.getModel()==null) {
				throw new IllegalArgumentException("You have to specify a base model!");
			}
		}
		if (performLaBtn != null) {
			conf.getParams().addBoolParam(Text2ImageConf.PERFORM_LA_PARAM, performLaBtn.getSelection());
		}
		
		conf.getParams().addParameter(Text2ImageConf.KEEP_UNMATCHED_LINES_PARAM, keepUnmatchedLinesBtn.getSelection());
		conf.getParams().addParameter(Text2ImageConf.PRESERVE_LINE_ORDER_PARAM, preserveLineOrderBtn.getSelection());
		conf.getParams().addParameter(Text2ImageConf.WRITE_SIMILARITY_TAG_PARAM, writeSimilarityTagBtn.getSelection());
		
		//test to keep the linebreaks
		conf.getParams().addParameter(Text2ImageConf.KEEP_LINE_BREAKS_PARAM, keepLineBreaks.getSelection());
		
		if (blockThreshTxt.toDoubleVal()!=null && blockThreshTxt.toDoubleVal()>=0 && blockThreshTxt.toDoubleVal()<=1) {
			conf.getParams().addDoubleParam(Text2ImageConf.BLOCK_THRESH_PARAM, blockThreshTxt.toDoubleVal());
		}
		else {
			throw new IllegalArgumentException("Region threshold not valid (must be a floating point number between 0 and 1)!");
		}
		
		if (lineThreshTxt.toDoubleVal()!=null && lineThreshTxt.toDoubleVal()>=0 && lineThreshTxt.toDoubleVal()<=1) {
			conf.getParams().addDoubleParam(Text2ImageConf.LINE_THRESH_PARAM, lineThreshTxt.toDoubleVal());
		}
		else {
			throw new IllegalArgumentException("Line threshold not valid (must be a floating point number between 0 and 1)!");
		}		
		
		return conf;
	}
	
	public boolean isDocsSelection() {
		return pagesSelector.isDocsSelection();
	}
	
	public List<DocSelection> getDocs() {
		return pagesSelector.getDocSelections();
	}
	
		

}
