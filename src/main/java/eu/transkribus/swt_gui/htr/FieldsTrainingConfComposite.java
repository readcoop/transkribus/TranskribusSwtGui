package eu.transkribus.swt_gui.htr;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IInputValidator;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import com.ibm.icu.impl.locale.LocaleDistance.Data;

import eu.transkribus.core.model.beans.DocSelection;
import eu.transkribus.core.model.beans.DocumentSelectionDescriptor;
import eu.transkribus.core.model.beans.LA2InputType;
import eu.transkribus.core.model.beans.LA2TrainConfig;
import eu.transkribus.core.model.beans.TrpDoc;
import eu.transkribus.core.model.beans.TrpPage;
import eu.transkribus.core.model.beans.enums.EditStatus;
import eu.transkribus.core.util.CoreUtils;
import eu.transkribus.core.util.DocSelectionUtil;
import eu.transkribus.core.util.ModelUtil;
import eu.transkribus.core.util.MonitorUtil;
import eu.transkribus.core.util.StructTypesAnal;
import eu.transkribus.swt.progress.ProgressBarDialog;
import eu.transkribus.swt.util.ComboInputDialog;
import eu.transkribus.swt.util.DialogUtil;
import eu.transkribus.swt.util.Images;
import eu.transkribus.swt.util.LabeledText;
import eu.transkribus.swt.util.SWTUtil;
import eu.transkribus.swt_gui.htr.treeviewer.DataSetSelectionController.DataSetSelection;
import eu.transkribus.swt_gui.htr.treeviewer.DataSetSelectionSashForm;
import eu.transkribus.swt_gui.mainwidget.storage.Storage;

public class FieldsTrainingConfComposite extends Composite {
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory
			.getLogger(CITlabLATrainingConfComposite.class);

	public static final Integer DEFAULT_CYCLES_FIELDS = 15000;
	public static final Integer DEFAULT_CYCLES_TABLES = 5000;
	public static final Double DEFAULT_LEARNING_RATE = 0.001;
	public static final String[] MODEL_TYPES_FIELDS = new String[] { "instance_50", "instance_101", "panoptic_50", "panoptic_101" };
	public static final String[] MODEL_TYPES_TABLES = new String[] { "instance_50", "instance_101" };
	
	private Text numEpochsTxt;
	private Text learningRateTxt;
	private Combo modelTypeCombo;
	private LabeledText structureTypesText;
	private Button analStructTypesBtn;
	private Text structTypeAnalText;
	private Button recognizeUntaggedRegionsBtn;
	private Button trainOnLinePolygonsBtn;
	private Button addStructBtn;

	private ModelTrainingDialog trainDialog;
	private Composite fieldsParsComp, tablesComp;
	private Button typeFieldsBtn;
	private Button typeTablesBtn;
	private Composite content;
	
	public FieldsTrainingConfComposite(Composite parent, int style, ModelTrainingDialog trainDialog) {
		super(parent, style);
		this.trainDialog = trainDialog;
		// setLayout(new GridLayout(1, false));
		setLayout(SWTUtil.createGridLayout(1, false, 0, 0));

		ScrolledComposite sc = new ScrolledComposite(this, SWT.V_SCROLL | SWT.H_SCROLL);
		sc.setLayoutData(new GridData(GridData.FILL_BOTH));
		sc.setLayout(SWTUtil.createGridLayout(1, false, 0, 0));

		content = new Composite(sc, SWT.NONE);
		content.setLayout(new GridLayout(2, false));
		content.setLayoutData(new GridData(GridData.FILL_BOTH));	
		
		typeFieldsBtn = new Button(content, SWT.RADIO);
		typeFieldsBtn.setText("Fields");
		typeFieldsBtn.setToolTipText("Train a Fields model to detect region or line fields in a document");
		typeFieldsBtn.setSelection(true);
		SWTUtil.onSelectionEvent(typeFieldsBtn, e -> updateUi());

		typeTablesBtn = new Button(content, SWT.RADIO);
		typeTablesBtn.setText("Tables");
		typeTablesBtn.setToolTipText("Train a Tables model to detect tables in a document");
		SWTUtil.onSelectionEvent(typeTablesBtn, e -> updateUi());
		
		Label numEpochsLbl = new Label(content, SWT.NONE);
		numEpochsLbl.setText("Nr. of Iterations:");
		numEpochsTxt = new Text(content, SWT.BORDER);
		numEpochsTxt.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
		numEpochsTxt.setToolTipText("The number of iterations");
		numEpochsTxt.setText(""+DEFAULT_CYCLES_FIELDS);
		
		Label learningRateLbl = new Label(content, SWT.NONE);
		learningRateLbl.setText("Learning rate:");		
		learningRateTxt = new Text(content, SWT.BORDER);
		learningRateTxt.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
		learningRateTxt.setToolTipText("The learning rate");
		learningRateTxt.setText(""+DEFAULT_LEARNING_RATE);

		modelTypeCombo = new Combo(content, SWT.DROP_DOWN | SWT.READ_ONLY);
		modelTypeCombo.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 2, 1));
		modelTypeCombo.setItems(MODEL_TYPES_FIELDS);
		modelTypeCombo.select(0);
		modelTypeCombo.setToolTipText("The model type of the backbone - one of: 'instance_50', 'instance_101', 'panoptic_50', 'panoptic_101'\n"
			+ "The instance models are often more accurate but produce more overlapping. The panoptic models try to solve that problem.\n"
			+ "The 50 models are smaller and faster, the 101 models are larger and more accurate\n"
			+ "instance_50 is the default, everything else is experimental - use at your own risk ;-)");

		fieldsParsComp = new Composite(content, 0);
		fieldsParsComp.setLayout(SWTUtil.createGridLayout(2, false, 0, 0));
		fieldsParsComp.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		
		structureTypesText = new LabeledText(fieldsParsComp, "Structures: ");
		structureTypesText.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		structureTypesText.setToolTipText("A list of all structure types to be trained separated by whitespaces\n"
				+ "E.g.: 'paragraph heading footnote page-number'");
		
		addStructBtn = new Button(fieldsParsComp, 0);
		addStructBtn.setImage(Images.ADD);
		addStructBtn.setToolTipText("Add a structure type");
		SWTUtil.onSelectionEvent(addStructBtn, e -> {
			String[] items = Storage.i().getStructCustomTagSpecsTypeStrings().toArray(new String[0]);
			ComboInputDialog d = new ComboInputDialog(getShell(), "Specify a structure: ", items, SWT.DROP_DOWN, true);
			d.setValidator(new IInputValidator() {
				@Override public String isValid(String arg) {
					if (StringUtils.containsWhitespace(arg)) {
						return "No spaces allowed in structure types!";
					}
					return null;
				}
			});
			if (d.open() == Dialog.OK) {
				structureTypesText.setText((structureTypesText.getText()+" "+d.getSelectedText()).trim());
				logger.info("current struct types: "+getCurrentStructTypes());
			}
		});
		
		recognizeUntaggedRegionsBtn = new Button(fieldsParsComp, SWT.CHECK);
		recognizeUntaggedRegionsBtn.setText("Recognize untagged regions");
		recognizeUntaggedRegionsBtn.setToolTipText("If checked, untagged regions will be recognized as well");

		trainOnLinePolygonsBtn = new Button(fieldsParsComp, SWT.CHECK);
		trainOnLinePolygonsBtn.setText("Train on line polygons");
		trainOnLinePolygonsBtn.setToolTipText("If checked, the model will be trained to detect line polygons instead of regions");
		SWTUtil.onSelectionEvent(trainOnLinePolygonsBtn, e -> updateUi());
		
		if (true) {
		Composite analStructTypesComp = new Composite(content, 0);
		analStructTypesComp.setLayout(SWTUtil.createGridLayout(2, false, 0, 0));
		analStructTypesComp.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 2, 1));

		analStructTypesBtn = new Button(analStructTypesComp, 0);
		analStructTypesBtn.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 2, 1));
		analStructTypesBtn.setText("Find current structure types in documents");
		analStructTypesBtn.setToolTipText("Parses structure type information on the current training and validation set.\nThis information is solely for reference and is not automatically applied during training.");
		SWTUtil.onSelectionEvent(analStructTypesBtn, e -> {
			structTypeAnalText.setText("");
			StructTypesAnal anal = parseStrucutreTypesAnal();
			if (anal != null) {
				String analText = "Structure types:\n";
				analText += "\t"+anal.getStructTypesStrForP2PaLA()+"\n";
				analText += "Counts: \n";
				analText += "\t"+anal.getCounts();
				structTypeAnalText.setText(analText);
				analStructTypesComp.layout();
			}
		});
		
		structTypeAnalText = new Text(analStructTypesComp, SWT.MULTI | SWT.BORDER | SWT.WRAP | SWT.V_SCROLL | SWT.READ_ONLY);
		GridData d = new GridData(SWT.FILL, SWT.FILL, true, false, 2, 1);
		d.heightHint = 100;
		structTypeAnalText.setLayoutData(d);
		}	

		updateUi();
		setDefault();

		sc.setContent(content);
	    sc.setExpandHorizontal(true);
	    sc.setExpandVertical(true);
		content.layout();
	    sc.setMinSize(0, content.computeSize(SWT.DEFAULT, SWT.DEFAULT).y-1);		
	}

	public void updateUi() {
		learningRateTxt.setText(""+DEFAULT_LEARNING_RATE);
		if (typeFieldsBtn.getSelection()) {
			if (tablesComp != null) {
				tablesComp.setParent(SWTUtil.dummyShell);
			}
			fieldsParsComp.setParent(content);
			fieldsParsComp.moveBelow(learningRateTxt);
		} else {
			fieldsParsComp.setParent(SWTUtil.dummyShell);
			if (tablesComp != null) {
				tablesComp.setParent(content);
				tablesComp.moveBelow(learningRateTxt);
			}
		}
		content.layout();

		structureTypesText.setEnabled(!trainOnLinePolygonsBtn.getSelection());
		addStructBtn.setEnabled(!trainOnLinePolygonsBtn.getSelection());
		recognizeUntaggedRegionsBtn.setEnabled(!trainOnLinePolygonsBtn.getSelection());
		
		if (isFields()) {
			numEpochsTxt.setText(""+DEFAULT_CYCLES_FIELDS);
			modelTypeCombo.setItems(MODEL_TYPES_FIELDS);
		} else {
			numEpochsTxt.setText(""+DEFAULT_CYCLES_TABLES);
			modelTypeCombo.setItems(MODEL_TYPES_TABLES);
		}		
		modelTypeCombo.select(0);
	}

	public boolean isFields() {
		return typeFieldsBtn.getSelection();
	}

	public boolean isTables() {
		return typeTablesBtn.getSelection();
	}

	private StructTypesAnal parseStrucutreTypesAnal() {
		Storage store = Storage.getInstance();
		if (!store.isLoggedIn()) {
			DialogUtil.showErrorMessageBox(getShell(), "Not logged in", "Please login to our server to use this feature!");
			return null;
		}
		
		List<DocSelection> docs = getAllDocSelections();
		
		if (CoreUtils.isEmpty(docs)) {
			DialogUtil.showErrorMessageBox(getShell(), "No documents", "Please selects some documents for train/val/test set!");
			return null;
		}
		
		try {
			StructTypesAnal anal = new StructTypesAnal();
			anal.setWithBaselines(true);
			// EditStatus editStatus = parseEditStatus();
			EditStatus editStatus = null; // TODO?
			// boolean skipPagesWithMissingStatus = skipPagesWithMissingStatusCheck.getSelection();
			boolean skipPagesWithMissingStatus = false; // TODO?
			
			ProgressBarDialog.open(getShell(), new IRunnableWithProgress() {
				@Override
				public void run(IProgressMonitor monitor) throws InvocationTargetException, InterruptedException {
					MonitorUtil.beginTask(monitor, "Retrieving pages for "+docs.size()+" document selections...", docs.size());
					
					List<TrpPage> pages = new ArrayList<>();
					int i=0;
					for (DocSelection ds : docs) {
						if (MonitorUtil.isCanceled(monitor)) {
							throw new InterruptedException();
						}
						MonitorUtil.subTask(monitor, (i+1)+"/"+docs.size());
						
						try {
							logger.debug("getting doc "+ds.getDocId()+"pagesStr = "+ds.getPages()+", editStatus = "+editStatus+", skipPagesWithMissingStatus = "+skipPagesWithMissingStatus);
							TrpDoc doc = store.getConnection().getTrpDoc(store.getCollId(), ds.getDocId(), -1);
							doc.filterPagesByPagesStrAndEditStatus(ds.getPages(), editStatus, skipPagesWithMissingStatus);
							pages.addAll(doc.getPages());
						} catch (Exception e) {
							throw new InvocationTargetException(e);
						}
						
						MonitorUtil.worked(monitor, ++i);
					}
					
					try {
						anal.setPages(pages);
						anal.analyzeStructureTypes(monitor);
						if (MonitorUtil.isCanceled(monitor)) {
							throw new InterruptedException();
						}
					} catch (Exception e) {
						throw new InvocationTargetException(e);
					}
				}
			}, "", true);
			return anal;
		}
		catch (InterruptedException ie) {
			logger.debug("cancelled...");
			return null;
		}
		catch (Throwable e) {
			DialogUtil.showErrorMessageBox2(getShell(), "Error", e.getMessage(), e);
			logger.error(e.getMessage(), e);
			return null;
		}
	}	
	
	private List<DocSelection> getAllDocSelections() {
		List<DocSelection> docs = new ArrayList<>();
		if (trainDialog.treeViewerSelector == null) {
			return docs;
		}

		docs.addAll(trainDialog.treeViewerSelector.getTrainSetDocSelection());
		docs.addAll(trainDialog.treeViewerSelector.getValSetDocSelection());
		return docs;
	}

	public void setDefault() {
		LA2TrainConfig c = new LA2TrainConfig();
		c.setModelType("instance_50");
		if (isFields()) {
			c.setNumEpochs(DEFAULT_CYCLES_FIELDS);
		} else {
			c.setNumEpochs(DEFAULT_CYCLES_TABLES);
		}

		SWTUtil.set(numEpochsTxt, ""+c.getNumEpochs());
		SWTUtil.set(learningRateTxt, ""+c.getLearningRate());
		structureTypesText.setText("");
		recognizeUntaggedRegionsBtn.setSelection(false);
		trainOnLinePolygonsBtn.setSelection(false);
		modelTypeCombo.select(0);
	}

	private List<String> getCurrentStructTypes() {
		if (structureTypesText.getText().isEmpty()) {
			return new ArrayList<>();
		}
		return Arrays.asList(structureTypesText.getText().split("\\s+")).stream().distinct().sorted().collect(Collectors.toList());
	}
	
	public String getProvider() {
		return ModelUtil.PROVIDER_LA2;
	}

	public List<String> validateParameters(ArrayList<String> errorList) {
		if(errorList == null) {
			errorList = new ArrayList<>();
		}
		if (CoreUtils.parseInt(numEpochsTxt.getText(), -1) <= 0 ) {
			errorList.add("Number of Epochs must be a number > 0!");
		}
		if (CoreUtils.parseDouble(learningRateTxt.getText(), -1.0) <= 0) {
			errorList.add("Learning rate must be a floating point number > 0!");
		}
		if (isFields()) {
			if (!trainOnLinePolygonsBtn.getSelection() && !recognizeUntaggedRegionsBtn.getSelection() && getCurrentStructTypes().isEmpty()) {
				errorList.add("Please specify at least one structure type or check 'Recognize untagged regions' or 'Train on line polygons'!");	
			}
		}
		
		return errorList;
	}

	public LA2TrainConfig addParameters(LA2TrainConfig conf) {
		conf.getModelMetadata().setProvider(getProvider());
		conf.setNumEpochs(Integer.parseInt(numEpochsTxt.getText()));
		conf.setLearningRate(Double.parseDouble(learningRateTxt.getText()));
		conf.setModelType(modelTypeCombo.getText());

		if (isFields()) {
			if (trainOnLinePolygonsBtn.getSelection()) {
				// Note: for training on line polygons, the inputTypeJson field must be empty and the inputType field must be set to "text_line"
				conf.setInputType("text_line");
			}
			else {
				// Note: for training on regions, the inputTypeJson field must be set to a list of LA2InputType objects
				// this is a sample JSON for the inputTypeJson field:
				// "inputTypeJson": [
				// 	{
				// 	  "input_type": "TEXT_REGION",
				// 	  "tag_value": "paragraph",
				// 	  "type": "tag_obj"
				// 	},
				// 	{
				// 	  "input_type": "TEXT_REGION",
				// 	  "tag_value": "heading",
				// 	  "type": "tag_obj"
				// 	},
				// use this to train on all text-regions regardless of their tag:
				// 	{
				// 	  "input_type": "TEXT_REGION",
				// 	  "type": "page_obj"
				// 	}
				//   ]

				List<LA2InputType> inputTypes = new ArrayList<>();
				logger.info("current struct types: "+getCurrentStructTypes());
				for (String structType : getCurrentStructTypes()) {
					inputTypes = addInputType(inputTypes, LA2InputType.InputType.TEXT_REGION, structType, LA2InputType.Type.tag_obj);
				}

				if (recognizeUntaggedRegionsBtn.getSelection()) {
					inputTypes = addInputType(inputTypes, LA2InputType.InputType.TEXT_REGION, null, LA2InputType.Type.page_obj);
				} 
				conf.setInputTypeJsonList(inputTypes);
			}

		} else {
			
		}
		
		return conf;
	}

	private List<LA2InputType> addInputType(List<LA2InputType> inputTypes, LA2InputType.InputType inputType, String tagValue, LA2InputType.Type type) {
		LA2InputType inputTypeObj = new LA2InputType();
		inputTypeObj.setInputType(inputType);
		inputTypeObj.setTagValue(tagValue);
		inputTypeObj.setType(type);
		inputTypes.add(inputTypeObj);

		return inputTypes;
	}

}
