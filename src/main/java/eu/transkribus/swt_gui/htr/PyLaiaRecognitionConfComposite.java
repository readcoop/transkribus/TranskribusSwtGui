package eu.transkribus.swt_gui.htr;

import java.util.Properties;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.transkribus.core.model.beans.job.enums.JobImpl;
import eu.transkribus.core.rest.JobConst;
import eu.transkribus.swt.util.DialogUtil;
import eu.transkribus.swt.util.LabeledText;
import eu.transkribus.swt.util.SWTUtil;
import eu.transkribus.swt_gui.mainwidget.storage.Storage;
import eu.transkribus.util.TextRecognitionConfig;

public class PyLaiaRecognitionConfComposite extends ARecognitionConfComposite implements SelectionListener {
	private final static Logger logger = LoggerFactory.getLogger(PyLaiaRecognitionConfComposite.class);
	
	Button doWordSegBtn;
//	RecentModelsCombo recentModelsCombo;
	Composite kwsComposite;
	Button writeKwsIndexFiles;
	Button useLanguageModelBtn;
	Button writeConfScoresBtn;
	
	public static final int DEFAULT_NBEST=100;
	LabeledText nBest;

	public PyLaiaRecognitionConfComposite(Composite parent, HtrTextRecognitionDialog htrRecognitionDialog) {
		super(parent, htrRecognitionDialog);
		
		doWordSegBtn = new Button(this, SWT.CHECK);
		doWordSegBtn.setText("Add estimated word coordinates");
		doWordSegBtn.setToolTipText("Adds approximate bounding boxes for the recognized words inside the lines");
		doWordSegBtn.setSelection(false);
		doWordSegBtn.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1));
		doWordSegBtn.moveBelow(doLinePolygonSimplificationBtn);

		useLanguageModelBtn = new Button(this, SWT.CHECK);
		useLanguageModelBtn.setText("Use language model");
		useLanguageModelBtn.setToolTipText("Whether to use the language model from the training data or not");
		useLanguageModelBtn.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1));
		useLanguageModelBtn.setSelection(hasSelectedLM());
		SWTUtil.onSelectionEvent(useLanguageModelBtn, e -> {
			if (htrRecognitionDialog.getConfig()!=null) {
				htrRecognitionDialog.getConfig().setLanguageModel(useLanguageModelBtn.getSelection() ? JobConst.PROP_TRAIN_DATA_LM_VALUE : null);
				htrRecognitionDialog.syncConfigAndConfigPrint();
				Storage.getInstance().saveTextRecognitionConfig(htrRecognitionDialog.getConfig());
				htrRecognitionDialog.updateUi();
			}
		});		
		
		kwsComposite = new Composite(this, 0);
		kwsComposite.setLayout(SWTUtil.createGridLayout(2, false, 0, 0));
		kwsComposite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1));
		
		writeKwsIndexFiles = new Button(kwsComposite, SWT.CHECK);
		writeKwsIndexFiles.setText("Enable SmartSearch");
		writeKwsIndexFiles.setToolTipText("Additional variants of the automatic transcriptions will be computed and stored, thus enabling an improved search (SmartSearch)");
		writeKwsIndexFiles.setSelection(false);
		SWTUtil.onSelectionEvent(writeKwsIndexFiles, e -> {
			if (nBest!=null) {
				nBest.setEnabled(writeKwsIndexFiles.getSelection());
			}
		});
//		writeKwsIndexFiles.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		
		if (Storage.i()!=null && Storage.i().isAdminLoggedIn()) {
			nBest = new LabeledText(kwsComposite, "N-Best: ");
			nBest.setText(""+DEFAULT_NBEST);
			nBest.setEnabled(writeKwsIndexFiles.getSelection());
			nBest.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
			nBest.setToolTipText("How many best paths should be taken into account? The more, the higher the processing time and space requirement...");
		}

		writeConfScoresBtn = new Button(this, SWT.CHECK);
		writeConfScoresBtn.setSelection(false);
		writeConfScoresBtn.setText("Write confidence scores");
		writeConfScoresBtn.setToolTipText("Whether to write confidence scores for lines and words");
		writeConfScoresBtn.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1));
		writeConfScoresBtn.moveBelow(useLanguageModelBtn);
		
		updateUi();
	}

	private boolean hasSelectedLM() {
		return htrRecognitionDialog.getConfig()!=null && htrRecognitionDialog.getConfig().getLanguageModel()!=null;
	}
	
	@Override
	protected void updateUi() {
		super.updateUi();
		
		useLanguageModelBtn.setSelection(hasSelectedLM());
		boolean isUserAllowedForKws = Storage.i().isUserAllowedForJob(JobImpl.PyLaiaKwsDecodingJob.toString(), false);
		if (!isUserAllowedForKws || !hasSelectedLM()) {
			kwsComposite.setParent(SWTUtil.dummyShell);
			writeKwsIndexFiles.setSelection(false);
		}
		else {
			kwsComposite.setParent(this);
			kwsComposite.moveAbove(batchSizeText);
		}

		if (hasSelectedLM()) {
			writeConfScoresBtn.setParent(SWTUtil.dummyShell);
			writeConfScoresBtn.setSelection(false);
		}
		else {
			writeConfScoresBtn.setParent(this);
			writeConfScoresBtn.moveBelow(useLanguageModelBtn);
		}
	}
	
	@Override
	public void applyConfig(TextRecognitionConfig config) {
		super.applyConfig(config);
		
		config.setDoWordSeg(doWordSegBtn.getSelection());
		config.setWriteKwsIndexFiles(config!=null && config.getLanguageModel()!=null && writeKwsIndexFiles.getSelection());
		if (config.isWriteKwsIndexFiles()) {
			try {
				int nBestVal = PyLaiaRecognitionConfComposite.DEFAULT_NBEST;
				if (nBest != null) {
					nBestVal = Integer.parseInt(nBest.getText());
				}
				logger.debug("setting nBest = "+nBestVal);
				config.setNBest(nBestVal);
			}
			catch (Exception e) {
				DialogUtil.showErrorMessageBox(getShell(), "Error parsing nBest", "Invalid nBest: "+nBest.getText());
				return;
			}
		}		
		config.setWriteLineConfScore(writeConfScoresBtn.getSelection());
		config.setWriteWordConfScores(writeConfScoresBtn.getSelection());
	}

	public RecentModelsCombo getRecentModelsCombo() {
		return recentModelsCombo;
	}
	
	@Override
	public void widgetDefaultSelected(SelectionEvent arg0) {
	}

	@Override
	public void widgetSelected(SelectionEvent arg0) {
		Properties props = recentModelsCombo.getJobProperties();
		
		boolean useExistentLines = Boolean.valueOf(props.getProperty(JobConst.PROP_USE_EXISTING_LINE_POLYGONS));
		
		logger.debug("selection event handled in Pylaia recognition");
		logger.debug("isDoLinePolygonSimplification "+Boolean.valueOf(props.getProperty(JobConst.PROP_DO_LINE_POLYGON_SIMPLIFICATION)));
		logger.debug("useExistingLinePolygonsBtn "+useExistentLines);
		logger.debug("useComputedLinePolygonsBtn "+!useExistentLines);
		
		SWTUtil.setSelection(doLinePolygonSimplificationBtn, Boolean.valueOf(props.getProperty(JobConst.PROP_DO_LINE_POLYGON_SIMPLIFICATION)));
		useExistingLinePolygonsBtn.setSelection(useExistentLines);
		useComputedLinePolygonsBtn.setSelection(!useExistentLines);
	}

}
