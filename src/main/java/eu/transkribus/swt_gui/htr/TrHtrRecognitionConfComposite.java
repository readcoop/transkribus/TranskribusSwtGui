package eu.transkribus.swt_gui.htr;

import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.widgets.Composite;

import eu.transkribus.swt.util.SWTUtil;

public class TrHtrRecognitionConfComposite extends ARecognitionConfComposite implements SelectionListener {
	public TrHtrRecognitionConfComposite(Composite parent, HtrTextRecognitionDialog htrRecognitionDialog) {
		super(parent, htrRecognitionDialog);
		
		clearLinesBtn.setParent(SWTUtil.dummyShell);
		structureTagComp.setParent(SWTUtil.dummyShell);
//		doNotDeleteWorkDirBtn.setParent(SWTUtil.dummyShell);
		b2pBackendCombo.setParent(SWTUtil.dummyShell);
	}
	
	@Override
	protected void updateUi() {
		super.updateUi();
		
		clearLinesBtn.setParent(SWTUtil.dummyShell);
		structureTagComp.setParent(SWTUtil.dummyShell);
//		doNotDeleteWorkDirBtn.setParent(SWTUtil.dummyShell);
		b2pBackendCombo.setParent(SWTUtil.dummyShell);
	}
}
