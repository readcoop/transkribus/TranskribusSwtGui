package eu.transkribus.swt_gui.htr;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.transkribus.core.model.beans.DocSelection;
import eu.transkribus.core.model.beans.TrpDocMetadata;
import eu.transkribus.core.util.CoreUtils;
import eu.transkribus.swt.util.DialogUtil;
import eu.transkribus.swt.util.SWTUtil;
import eu.transkribus.swt_gui.mainwidget.storage.Storage;
import eu.transkribus.swt_gui.util.CurrentTranscriptOrDocPagesOrCollectionSelector;
import eu.transkribus.util.TextRecognitionConfig;

public class HtrTextRecognitionDialog extends Dialog implements SelectionListener {
	private static final Logger logger = LoggerFactory.getLogger(HtrTextRecognitionDialog.class);
	
	private HtrTextRecognitionConfigDialog trcd = null;

	private CurrentTranscriptOrDocPagesOrCollectionSelector dps;
	private boolean docsSelected = false;
	private List<DocSelection> selectedDocSelections;
	private Map<TrpDocMetadata, DocSelection> selectedDocSelectionDetails;
	
	PyLaiaRecognitionConfComposite pylaiaConfComp;
	TrHtrRecognitionConfComposite trHtrConfComp;
	
	private Storage store = Storage.getInstance();
	
	private TextRecognitionConfig config;
	private TextRecognitionConfig configPrint;
	private String pages;
	
	private Composite container;
	private Text configTxt;
	private Button configBtn;
	private Button configPrintBtn;
	
	public HtrTextRecognitionDialog(Shell parent) {
		super(parent);
	}
    
	public void setVisible() {
		if(super.getShell() != null && !super.getShell().isDisposed()) {
			super.getShell().setVisible(true);
		}
	}
	
	@Override
	protected Control createDialogArea(Composite parent) {
		Composite cont = (Composite) super.createDialogArea(parent);
		cont.setLayout(new GridLayout(3, false));
		
		container = cont;
		
		//FIXME the document selection is not initialized before the selection dialog is opened once
		//with this selector jobs can be started for complete collections
		dps = new CurrentTranscriptOrDocPagesOrCollectionSelector(cont, SWT.NONE, true, true, true, true);		
		dps.setLayoutData(new GridData(SWT.FILL, SWT.BOTTOM, true, false, 3, 1));
		
		pylaiaConfComp = new PyLaiaRecognitionConfComposite(container, this);
		pylaiaConfComp.setLayoutData(new GridData(SWT.FILL, SWT.BOTTOM, true, false, 3, 1));
		
		trHtrConfComp = new TrHtrRecognitionConfComposite(SWTUtil.dummyShell, this);
		trHtrConfComp.setLayoutData(new GridData(SWT.FILL, SWT.BOTTOM, true, false, 3, 1));
		
		configTxt = new Text(cont, SWT.MULTI | SWT.READ_ONLY | SWT.BORDER | SWT.V_SCROLL);
		GridData txtGridData = new GridData(SWT.FILL, SWT.FILL, true, false, 3, 1);
		txtGridData.minimumHeight = 75;
		configTxt.setLayoutData(txtGridData);
		configBtn = new Button(cont, SWT.PUSH);
		configBtn.setText("Select HTR model...");
		configBtn.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, true, 3, 1));
		
		configBtn.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if(trcd == null) {
					trcd = new HtrTextRecognitionConfigDialog(parent.getShell(), config);
					if(trcd.open() == IDialogConstants.OK_ID) {
						logger.info("OK pressed");
						config = trcd.getConfig();
						syncConfigAndConfigPrint();
						store.saveTextRecognitionConfig(config);
						updateUi();
					}
					trcd = null;
				} else {
					trcd.setVisible();
				}
			}
		});

		if (Storage.i().isAdminLoggedIn()) {
			configPrintBtn = new Button(cont, SWT.PUSH);
			configPrintBtn.setText("Select HTR model for printed pages...");
			configPrintBtn.setToolTipText("If set, a second job for printed pages only is started, and the original one only processes handwritten pages.");
			configPrintBtn.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, true, 3, 1));
			configPrintBtn.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent e) {
					if(trcd == null) {
						trcd = new HtrTextRecognitionConfigDialog(parent.getShell(), config);
						if(trcd.open() == IDialogConstants.OK_ID) {
							logger.info("OK printed pressed");
							configPrint = trcd.getConfig();							
							syncConfigAndConfigPrint();
							store.saveTextRecognitionConfig(config);
							updateUi();
						}
						trcd = null;
					} else {
						trcd.setVisible();
					}
				}
			});


		}
		
		config = store.loadTextRecognitionConfig();
		logger.debug("Config loaded:" + config);
		
		updateUi();
		return cont;
	}

	protected void syncConfigAndConfigPrint() {
		if (config != null && configPrint != null) {
			config.setPrintedHtrId(configPrint.getHtrId());
			config.setPrintedLanguageModel(configPrint.getPrintedLanguageModel());
		}
	}
	
	public boolean isDocsSelection(){
		return docsSelected;
	}
	
//	public List<DocumentSelectionDescriptor> getDocs(){
//		return selectedDocDescriptors;
//	}
	
	public List<DocSelection> getDocs() {
		return selectedDocSelections;
	}
	
	public Map<TrpDocMetadata, DocSelection> getDocSelectionDetails() {
		return selectedDocSelectionDetails;
	}
	
	@Override
	protected void okPressed() {

		if(dps.isCurrentTranscript()) {
			pages = ""+store.getPage().getPageNr();
		} else if(!dps.isDocsSelection()) {
			pages = dps.getPagesStr();
			if(pages == null || pages.isEmpty()) {
				DialogUtil.showErrorMessageBox(this.getParentShell(), "Error", "Please specify pages for recognition.");
				return;
			}
			
			try {
				CoreUtils.parseRangeListStr(pages, store.getDoc().getNPages());
			} catch (IOException e) {
				DialogUtil.showErrorMessageBox(this.getParentShell(), "Error", "Page selection is invalid.");
				return;
			}
		} else {
			docsSelected = dps.isDocsSelection();
//			selectedDocDescriptors = dps.getDocumentsSelected();
			selectedDocSelections = dps.getDocSelections();
			selectedDocSelectionDetails = dps.getDocSelectionDetails();
			if(CollectionUtils.isEmpty(selectedDocSelections)) {
				DialogUtil.showErrorMessageBox(this.getParentShell(), "Error", "No documents selected for recognition.");
				return;
			}
		}
		
		if (config == null) {
			DialogUtil.showErrorMessageBox(getShell(), "Bad Configuration", "Please define a configuration.");
			return;
		}
		
		if (config != null) {
			ARecognitionConfComposite confComposite = getConfComposite();
			if (confComposite == null) {
				DialogUtil.showErrorMessageBox(getShell(), "Bad Configuration", "Not a valid configuration (did you select an old CITlab model?)");
				return;				
			}
			confComposite.applyConfig(config);
		}
		
		super.okPressed();
	}
	
	private ARecognitionConfComposite getConfComposite() {
		if (config == null) {
			return null;
		}
		else if (config.isPyLaia()) {
			return pylaiaConfComp;
		}
		else if (config.isTrHtr()) {
			return trHtrConfComp;
		}
		else {
			return null;
		}
	}
	
	protected void updateUi() {
//		boolean isCitlab = config!=null && config.getMode()==Mode.CITlab;
		configTxt.setText(config!=null ? config.toString() : "");
		if (config == null) {
			pylaiaConfComp.setParent(SWTUtil.dummyShell);
			trHtrConfComp.setParent(SWTUtil.dummyShell);
		}
		else if (config.isPyLaia()) {
			pylaiaConfComp.setParent(container);
			pylaiaConfComp.moveAbove(configTxt);
			pylaiaConfComp.updateUi();
			trHtrConfComp.setParent(SWTUtil.dummyShell);
		}
		else if (config.isTrHtr()) {
			trHtrConfComp.setParent(container);
			trHtrConfComp.moveAbove(configTxt);
			trHtrConfComp.updateUi();
			pylaiaConfComp.setParent(SWTUtil.dummyShell);
		}
		container.layout();
	}

	@Override
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText("Text Recognition");
		if (Storage.getInstance().isAdminLoggedIn())
			newShell.setMinimumSize(300, 500);
		else{
			newShell.setMinimumSize(300, 460);
		}
	}

	@Override
	protected Point getInitialSize() {
		return SWTUtil.getPreferredOrMinSize(getShell(), 300, 470);
	}

	@Override
	protected void setShellStyle(int newShellStyle) {
		super.setShellStyle(SWT.CLOSE | SWT.MAX | SWT.RESIZE | SWT.TITLE);
		// setBlockOnOpen(false);
	}
	
	public TextRecognitionConfig getConfig() {
		return this.config;
	}
	
	public String getPages() {
		return this.pages;
	}

	@Override
	public void widgetDefaultSelected(SelectionEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void widgetSelected(SelectionEvent arg0) {
		int index = 0;
		if (pylaiaConfComp.getParent() == container) {
			index = pylaiaConfComp.getRecentModelsCombo().getCombo().getSelectionIndex();
			pylaiaConfComp.getRecentModelsCombo().getCombo().layout();
			pylaiaConfComp.getRecentModelsCombo().loadRecentHtr(pylaiaConfComp.getRecentModelsCombo().getCombo().getItem(index));
		}
		else if (trHtrConfComp.getParent() == container) {
			index = trHtrConfComp.getRecentModelsCombo().getCombo().getSelectionIndex();
			trHtrConfComp.getRecentModelsCombo().getCombo().layout();
			trHtrConfComp.getRecentModelsCombo().loadRecentHtr(trHtrConfComp.getRecentModelsCombo().getCombo().getItem(index));
		}
		
		config = store.loadTextRecognitionConfig();
		configTxt.requestLayout();
		updateUi();
		
		if (pylaiaConfComp.getParent() == container) {
			pylaiaConfComp.getRecentModelsCombo().getCombo().select(index);
		}
		
	}
}
