package eu.transkribus.swt_gui.htr;

import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import org.apache.commons.collections.CollectionUtils;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.transkribus.core.rest.JobConst;
import eu.transkribus.swt.util.DialogUtil;
import eu.transkribus.swt.util.LabeledCombo;
import eu.transkribus.swt.util.LabeledText;
import eu.transkribus.swt.util.SWTUtil;
import eu.transkribus.swt_gui.mainwidget.storage.Storage;
import eu.transkribus.util.TextRecognitionConfig;

public abstract class ARecognitionConfComposite extends Composite implements SelectionListener {
	private static final Logger logger = LoggerFactory.getLogger(TrHtrRecognitionConfComposite.class);
	protected Button doLinePolygonSimplificationBtn;
	protected LabeledText batchSizeText;
	protected Button useExistingLinePolygonsBtn;
	protected Button useComputedLinePolygonsBtn;
	protected RecentModelsCombo recentModelsCombo;
	protected HtrTextRecognitionDialog htrRecognitionDialog;
	protected StructureTagComposite structureTagComp;
	protected Button doNotDeleteWorkDirBtn;
	protected Button clearLinesBtn;
	protected LabeledCombo b2pBackendCombo;

	protected ARecognitionConfComposite(Composite parent, HtrTextRecognitionDialog htrRecognitionDialog) {
		super(parent, 0);
		this.setLayout(SWTUtil.createGridLayout(2, false, 0, 0));
		this.htrRecognitionDialog = htrRecognitionDialog;
		
		useComputedLinePolygonsBtn= new Button(this, SWT.RADIO);
		useComputedLinePolygonsBtn.setText("Compute line polygons");
		useComputedLinePolygonsBtn.setToolTipText("Line polygons are automatically computed from baselines - the existing ones are deleted!");
		useComputedLinePolygonsBtn.setSelection(true);
		useComputedLinePolygonsBtn.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		SWTUtil.onSelectionEvent(useComputedLinePolygonsBtn, e -> updateUi());
		
		useExistingLinePolygonsBtn = new Button(this, SWT.RADIO);
		useExistingLinePolygonsBtn.setText("Use existing line polygons");
		useExistingLinePolygonsBtn.setToolTipText("Do *not* perform a baseline to polygon computation but use the existing line polygons.\nUse this if you have exact line polygons e.g. from an OCR engine.");
		useExistingLinePolygonsBtn.setSelection(false);
		useExistingLinePolygonsBtn.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		SWTUtil.onSelectionEvent(useExistingLinePolygonsBtn, e -> updateUi());
		
		doLinePolygonSimplificationBtn = new Button(this, SWT.CHECK);
		doLinePolygonSimplificationBtn.setText("Do polygon simplification");
		doLinePolygonSimplificationBtn.setToolTipText("Perform a line polygon simplification after the recognition process to reduce the number of points");
		doLinePolygonSimplificationBtn.setSelection(true);
		doLinePolygonSimplificationBtn.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1));
		
		structureTagComp = new StructureTagComposite(this);
		structureTagComp.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1));
		
		clearLinesBtn = new Button(this, SWT.CHECK);
		clearLinesBtn.setText("Delete other text (if using structures)");
		clearLinesBtn.setToolTipText("If recognition is restricted to structure tags you can choose if you want to delete the text in other regions!");
		clearLinesBtn.setSelection(false);
		clearLinesBtn.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1));		
		
		recentModelsCombo = new RecentModelsCombo(this);
		recentModelsCombo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1));	
		recentModelsCombo.getCombo().addSelectionListener(htrRecognitionDialog);
		recentModelsCombo.getCombo().addSelectionListener(this);
		
		batchSizeText = new LabeledText(this, "Batch size: ");
		batchSizeText.setText(""+10);
		batchSizeText.setToolTipText("Number of lines that are simultaneously decoded - if you get a memory error, decrease this value");
		batchSizeText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1));	
		
		doNotDeleteWorkDirBtn = new Button(this, SWT.CHECK);
		doNotDeleteWorkDirBtn.setText("Do not delete workdir (for testing)");
		doNotDeleteWorkDirBtn.setToolTipText("Workdir on worker module does not get deleted - for testing purposes - use with care!");
		doNotDeleteWorkDirBtn.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 2, 1));
		
		b2pBackendCombo = new LabeledCombo(this, "B2P Method: ");
		b2pBackendCombo.setItems(new String[] { "Legacy", "Legacy_berlin" } );
		b2pBackendCombo.select(0);
		b2pBackendCombo.setToolTipText("The method to use for baseline-to-polygon computation");
	}

	public int getBatchSize() {
		if (batchSizeText == null) {
			return 10;
		}
		try {
			return Integer.parseInt(batchSizeText.getText());
		}
		catch (Exception e) {
			DialogUtil.showErrorMessageBox(getShell(), "Error parsing batch size", "Invalid batch size: "+batchSizeText.getText());
			return -1;
		}
	}

	protected void updateUi() {
		SWTUtil.setSelection(doLinePolygonSimplificationBtn, !useExistingLinePolygonsBtn.getSelection());
		boolean isAdminLoggedIn = Storage.getInstance()!=null && Storage.getInstance().isAdminLoggedIn();
		if (!isAdminLoggedIn) {
			doLinePolygonSimplificationBtn.setParent(SWTUtil.dummyShell);
			batchSizeText.setParent(SWTUtil.dummyShell);
			doNotDeleteWorkDirBtn.setParent(SWTUtil.dummyShell);
			b2pBackendCombo.setParent(SWTUtil.dummyShell);
		}
		else {
			doLinePolygonSimplificationBtn.setParent(this);
			doLinePolygonSimplificationBtn.moveBelow(useExistingLinePolygonsBtn);			
			batchSizeText.setParent(this);
			batchSizeText.moveBelow(recentModelsCombo);
			doNotDeleteWorkDirBtn.setParent(this);
			doNotDeleteWorkDirBtn.moveBelow(batchSizeText);
			b2pBackendCombo.moveBelow(doNotDeleteWorkDirBtn);
		}	
		
		if (useExistingLinePolygonsBtn.getSelection()) {
			doLinePolygonSimplificationBtn.setSelection(false);
		}
		else {
			doLinePolygonSimplificationBtn.setSelection(true);
		}
		
		if (!isAdminLoggedIn) {
			batchSizeText.setParent(SWTUtil.dummyShell);
		}
		else {
			batchSizeText.setParent(this);
			batchSizeText.moveAbove(recentModelsCombo.getCombo());
		}
	}
	
	public void applyConfig(TextRecognitionConfig config) {
		boolean isAdminLoggedIn = Storage.getInstance()!=null && Storage.getInstance().isAdminLoggedIn();
		logger.debug("structs = "+Arrays.toString(structureTagComp.getMultiCombo().getSelections()));
		List<String> structures = Arrays.asList(structureTagComp.getMultiCombo().getSelections());
		config.setStructures(structures);
		if (!CollectionUtils.isEmpty(structures)) {
			config.setClearLines(clearLinesBtn.getSelection());	
		}
		config.setUseExistingLinePolygons(useExistingLinePolygonsBtn.getSelection());
		config.setDoLinePolygonSimplification(!isAdminLoggedIn || doLinePolygonSimplification());
		config.setDoNotDeleteWorkDir(isDoNotDeleteWorkDir());
		config.setBatchSize(getBatchSize());
		config.setB2PBackend(getB2PBackend());
		if (config.getBatchSize() <= 0) {
			return;
		}
	}
	
	public boolean doLinePolygonSimplification() {
		return doLinePolygonSimplificationBtn==null || doLinePolygonSimplificationBtn.getSelection();
	}
	
	public boolean isDoNotDeleteWorkDir() {
		return doNotDeleteWorkDirBtn != null ? doNotDeleteWorkDirBtn.getSelection() : false;
	}	
	
	public String getB2PBackend() {
		if (b2pBackendCombo != null) {
			return b2pBackendCombo.getCombo().getText();
		}
		else {
			return "Legacy";
		}
	}

	public RecentModelsCombo getRecentModelsCombo() {
		return recentModelsCombo;
	}

	@Override
	public void widgetDefaultSelected(SelectionEvent arg0) {
	}

	@Override
	public void widgetSelected(SelectionEvent e) {
		Properties props = recentModelsCombo.getJobProperties();
		
		boolean useExistentLines = Boolean.valueOf(props.getProperty(JobConst.PROP_USE_EXISTING_LINE_POLYGONS));
		
		logger.debug("selection event handled in Pylaia recognition");
		logger.debug("isDoLinePolygonSimplification "+Boolean.valueOf(props.getProperty(JobConst.PROP_DO_LINE_POLYGON_SIMPLIFICATION)));
		logger.debug("useExistingLinePolygonsBtn "+useExistentLines);
		logger.debug("useComputedLinePolygonsBtn "+!useExistentLines);
		
		doLinePolygonSimplificationBtn.setSelection(Boolean.valueOf(props.getProperty(JobConst.PROP_DO_LINE_POLYGON_SIMPLIFICATION)));
		useExistingLinePolygonsBtn.setSelection(useExistentLines);
		useComputedLinePolygonsBtn.setSelection(!useExistentLines);
	}

}