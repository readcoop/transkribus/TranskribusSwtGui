package eu.transkribus.swt_gui.htr;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import eu.transkribus.core.model.beans.TrpModelMetadata;
import eu.transkribus.core.model.beans.rest.TrpModelMetadataList;
import eu.transkribus.core.util.ModelUtil;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.transkribus.core.model.beans.job.TrpJobStatus;
import eu.transkribus.core.rest.JobConst;
import eu.transkribus.swt.util.SWTUtil;
import eu.transkribus.swt_gui.mainwidget.storage.Storage;
import eu.transkribus.util.TextRecognitionConfig;

public class RecentModelsCombo extends Composite {
	private final static Logger logger = LoggerFactory.getLogger(RecentModelsCombo.class);
	
	private Storage store = Storage.getInstance();
	private Combo combo;
	
	Map<Integer, TrpModelMetadata> htrs = new HashMap<>();
	Map<Integer, TrpJobStatus> recentRecognitionJobs = null;
	
	Properties props = null;

	public RecentModelsCombo(Composite parent) {
		super(parent, 0);
		this.setLayout(SWTUtil.createGridLayout(1, false, 0, 0));
		
//		Color lightGreen =Colors.createColor(new RGB( 255, 255, 200 ));

		Label recentModelLbl = new Label(this, SWT.FILL);
		recentModelLbl.setText("Recently used HTR models");
//		recentModelLbl.setBackground(lightGreen);
		recentModelLbl.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		
		combo = new Combo(this, SWT.READ_ONLY);
		combo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
//		combo.setBackground(lightGreen);
		
		findRecentHtrs();
	}
	
	public Properties getJobProperties() {
		return props;
	}
	
	protected void findRecentHtrs() {
		Display.getDefault().asyncExec(new Runnable() {
		    public void run() {
		    	try {
		    		List<TrpJobStatus> pylaiaJobs = new ArrayList<>();
		    		recentRecognitionJobs = new HashMap<Integer, TrpJobStatus>();
		    		
		    		/*
		    		 * HTR+ not used anymore
		    		 */
					//recJobs = store.getConnection().getJobs(true, TrpJobStatus.FINISHED, "CITlab Handwritten Text Recognition", null,	0, 50, null, null);
				
					pylaiaJobs = store.getConnection().getJobs(true, TrpJobStatus.FINISHED, "PyLaia Decoding", null,
							0, 100, null, null);
					
					
					TrpModelMetadataList l = store.getConnection().getModelCalls()
							.getModels(ModelUtil.TYPE_TEXT, store.getCollId(), null, null, null,
									null, null, null, null, null, null, null, 0, -1, null, "desc");
					for (TrpJobStatus job : pylaiaJobs) {
						
						if (!recentRecognitionJobs.containsKey(job.getModelId())) {
							//logger.debug("model id: " + job.getModelId());
							recentRecognitionJobs.put(job.getModelId(), job);
						}
						
					}
					
					logger.debug( "nr of recognition jobs in history: " + pylaiaJobs.size());				
					logger.debug( "available htrs in collection (incl. public models) " + l.getTotal());
//					for (TrpHtr htr : l.getList()) {
//						logger.debug( "(htr name) " + htr.getName() + " (htr id) " + htr.getHtrId());
//					}
					
					//recentModelIds.forEach((K,V) -> logger.debug( K + " => " + V.getModelId() ));
			
					int i = 0;
					for (Integer key : recentRecognitionJobs.keySet()) {
						//TrpJobStatus currJob = recentModelIds.get(key);
						for (TrpModelMetadata htr : l.getList()) {
							if (Integer.valueOf(htr.getModelId()).equals(key)) {
								logger.trace( " => (recent model found) " + htr.getName() );
			
								htrs.put(htr.getModelId(), htr);
								if (!SWTUtil.isDisposed(combo)) {
									//logger.debug( " add to combo " + htr.getName() );
									combo.add(htr.getModelId() + ": " + htr.getName() + " (\t"  + htr.getProvider() + ")");
									TextRecognitionConfig config = store.loadTextRecognitionConfig();
									
									if (config != null && config.getHtrId() == htr.getModelId()) {
										logger.debug("previous config found: " + htr.getModelId());
										combo.select(i);
									}
									i++;
									
								}
							}
						}
					}
					if (combo != null && combo.getSelectionIndex() == -1) {
						combo.select(0);
					}
				} catch (Exception e) {
					logger.error(e.getMessage(), e);
				}
		    }
		});
	}

	protected void loadRecentHtr(String recentHtr) {
		TextRecognitionConfig config = null;
		try {
			
			String id = recentHtr.substring(0, recentHtr.indexOf(":"));
			
			TrpModelMetadata htr = htrs.get(Integer.valueOf(id));
			TrpJobStatus job = recentRecognitionJobs.get(Integer.valueOf(id));
			
			
			if (htr != null) {
				config = new TextRecognitionConfig(htr.getProvider());
				props = job.getJobDataProps().getProperties();

				config.setDictionary(props.getProperty(JobConst.PROP_DICTNAME));
				config.setLanguageModel(JobConst.PROP_TRAIN_DATA_LM_VALUE);
				config.setHtrId(htr.getModelId());
				config.setHtrName(htr.getName());
				config.setLanguage(htr.getLanguage());				
			}
			else {
				logger.debug("model was probably deleted - setting config to null!");
				config = null;
			}
		} catch (Exception e) {
			logger.error("Error while setting HTR: "+e.getMessage(), e);
		}
		finally {
			store.saveTextRecognitionConfig(config);
		}
	}
	
	public Combo getCombo() {
		return combo;
	}

}