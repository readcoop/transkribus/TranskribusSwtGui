package eu.transkribus.swt_gui.models;

import eu.transkribus.client.util.SessionExpiredException;
import eu.transkribus.client.util.TrpClientErrorException;
import eu.transkribus.client.util.TrpServerErrorException;
import eu.transkribus.core.model.beans.ReleaseLevel;
import eu.transkribus.core.model.beans.TrpCollection;
import eu.transkribus.core.model.beans.TrpModelMetadata;
import eu.transkribus.swt.util.Images;
import eu.transkribus.swt_gui.metadata.CollectionsTableWidget;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Shell;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class ShareModelMetadataDialog extends ShareModelDialog {
	private static final Logger logger = LoggerFactory.getLogger(ShareModelMetadataDialog.class);
	public ShareModelMetadataDialog(Shell parentShell, TrpModelMetadata model) {
		super(parentShell, model);
	}

	@Override
	protected Point getInitialSize() {
		return new Point(500, 400);
	}

	@Override
	protected CollectionsTableWidget createCollectionsTableWidget(Composite parent) {
		return new ModelCollectionsTableWidget(parent, SWT.BORDER);
	}
	protected TrpCollection acquireUserSelection() {
		ModelCollectionSelectorDialog d = new ModelCollectionSelectorDialog(getShell());
		if (d.open() != Dialog.OK) {
			return null;
		}
		TrpCollection c = d.getSelectedCollection();
		ReleaseLevel r = d.getReleaseLevel();
		logger.debug("User selection: ReleaseLevel = {}, Collection = {}", r, c.toShortString());
		c.setModelCollectionReleaseLevel("" + r);
		return c;
	}
	protected void addModelToCollection(TrpCollection c)
			throws TrpClientErrorException, TrpServerErrorException, SessionExpiredException {
		store.getConnection().getModelCalls().addModelToCollection(getModel(), c);
	}
	protected void removeModelFromCollection(int colId)
			throws TrpClientErrorException, TrpServerErrorException, SessionExpiredException {
		store.getConnection().getModelCalls().removeModelFromCollection(getModel(), colId);
	}
	protected List<TrpCollection> getCollectionList()
			throws TrpServerErrorException, TrpClientErrorException, SessionExpiredException {
		return store.getConnection().getModelCalls().getModelCollections(getModel());
	}

	/**
	 *  {@link CollectionsTableWidget} with adapted column config: shows {@link TrpCollection#getModelCollectionReleaseLevel()} but not the description
	 */
	static class ModelCollectionsTableWidget extends CollectionsTableWidget {
		public ModelCollectionsTableWidget(Composite parent, int style) {
			super(parent, style);
		}

		@Override
		protected void createColumns() {
			createColumn(ID_COL, 50, "colId", new CollectionsTableColumnLabelProvider("colId"));
			createColumn(NAME_COL, 200, "colName", new CollectionsTableColumnLabelProvider("colName"));
			createColumn("Dataset Access", 120, null, new ColumnLabelProvider() {
				@Override
				public String getText(Object element) {
					return "";
				}

				@Override
				public Image getImage(Object element) {
					TrpCollection c = (TrpCollection) element;
					ReleaseLevel r = ReleaseLevel.fromString(c.getModelCollectionReleaseLevel());
					if(ReleaseLevel.UndisclosedDataSet.equals(r)) {
						return Images.CROSS;
					}
					if(ReleaseLevel.DisclosedDataSet.equals(r)) {
						return Images.TICK;
					}
					return null;
				}
			});
			createColumn(ROLE_COL, 80, "role", new CollectionsTableColumnLabelProvider("role"));
		}
	}
}
