package eu.transkribus.swt_gui.models;

import eu.transkribus.core.model.beans.ReleaseLevel;
import eu.transkribus.core.model.beans.TrpCollection;
import eu.transkribus.swt.util.Fonts;
import eu.transkribus.swt_gui.collection_comboviewer.CollectionSelectorWidget;
import eu.transkribus.swt_gui.mainwidget.TrpMainWidget;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

import java.util.function.Predicate;

public class ModelCollectionSelectorDialog extends Dialog {

    CollectionSelectorWidget collSelector;
    Button releaseLevelChk;
    TrpCollection selectedCollection;
    ReleaseLevel releaseLevel;

    public ModelCollectionSelectorDialog(Shell parentShell) {
        super(parentShell);
    }

    @Override protected void configureShell(Shell shell) {
        super.configureShell(shell);
        shell.setText("Choose a collection to share your model");
    }

    @Override protected boolean isResizable() {
        return true;
    }

    @Override protected Control createDialogArea(Composite parent) {
        Composite container = (Composite) super.createDialogArea(parent);
        container.setLayout(new GridLayout(2, false));

        Label l = new Label(container, 0);
        l.setText("Selected collection: ");
        Fonts.setBoldFont(l);

        Predicate<TrpCollection> collSelectorPredicate = (c) -> { return c != null; }; // && AuthUtils.canManage(c.getRole()); }; // show only collections where user can upload to!
        collSelector = new CollectionSelectorWidget(container, 0, false, collSelectorPredicate );
        collSelector.setToolTipText("This is the collection the document will be added to - you can only upload to collections where you are at least editor");
        collSelector.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1));
        // set to current collection if possible:
        TrpMainWidget mw = TrpMainWidget.getInstance();
        if (mw != null) {
            TrpCollection c = mw.getSelectedCollection();
            if (collSelectorPredicate.test(c)) {
                collSelector.setSelectedCollection(c);
            }
        }

        releaseLevelChk = new Button(container, SWT.CHECK);
        releaseLevelChk.setText("Allow Access to Datasets");
        releaseLevelChk.setToolTipText("Specify if users in the collection you are sharing the model with are allowed to view the training and validation set of your model and use them for their own trainings.");
        releaseLevelChk.setSelection(true);
        return container;
    }

    @Override protected void okPressed() {
        selectedCollection = collSelector.getSelectedCollection();
        if (releaseLevelChk.getSelection()) {
            releaseLevel = ReleaseLevel.DisclosedDataSet;
        } else {
            releaseLevel = ReleaseLevel.UndisclosedDataSet;
        }
        super.okPressed();
    }

    public TrpCollection getSelectedCollection() { return selectedCollection; }

    public ReleaseLevel getReleaseLevel() {
        return releaseLevel;
    }

    @Override protected void createButtonsForButtonBar(Composite parent) {
        createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL, true);
        createButton(parent, IDialogConstants.CANCEL_ID, IDialogConstants.CANCEL_LABEL, false);
    }

    /**
     * Return the initial size of the dialog.
     */
    @Override protected Point getInitialSize() {
        return new Point(550, 200);
    }
}
