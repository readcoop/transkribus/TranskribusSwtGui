package eu.transkribus.swt_gui.doc_overview;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.viewers.ColumnViewerToolTipSupport;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.window.ToolTip;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.custom.CTabItem;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.TextToolItem;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;
import org.eclipse.ui.forms.events.ExpansionAdapter;
import org.eclipse.ui.forms.events.ExpansionEvent;
import org.eclipse.ui.forms.widgets.ExpandableComposite;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.transkribus.core.model.beans.TrpCollection;
import eu.transkribus.core.model.beans.TrpDocMetadata;
import eu.transkribus.core.model.beans.TrpModelMetadata;
import eu.transkribus.swt.util.Fonts;
import eu.transkribus.swt.util.Images;
import eu.transkribus.swt.util.SWTUtil;
import eu.transkribus.swt_gui.collection_comboviewer.CollectionSelectorWidget;
import eu.transkribus.swt_gui.htr.treeviewer.GroundTruthPagedTreeWidget;
import eu.transkribus.swt_gui.htr.treeviewer.ModelGroundTruthContentProvider;
import eu.transkribus.swt_gui.htr.treeviewer.ModelGroundTruthTableLabelAndFontProvider;
import eu.transkribus.swt_gui.mainwidget.storage.Storage;
import eu.transkribus.swt_gui.models.ModelDetailsDialog;
import eu.transkribus.swt_gui.util.DropDownButton;
import eu.transkribus.swt_gui.util.RecentDocsComboViewerWidget;
import eu.transkribus.swt_gui.util.RecentDocsDropDownButton;

public class ServerWidget extends Composite {
	private final static Logger logger = LoggerFactory.getLogger(ServerWidget.class);

	/**
	 * Switch for disabling the GT tab to be shown to all users during transition
	 */
	private static boolean SHOW_GT_TAB_ITEM = true;
	
	Label usernameLabel;
	DocTableWidgetPagination docTableWidget;

	CollectionSelectorWidget collectionSelectorWidget;
	Text quickLoadColId;
	
	CTabFolder tabFolder;
	CTabItem documentsTabItem, gtTabItem;
	
	GroundTruthPagedTreeWidget groundTruthTreeWidget;
	
	RecentDocsComboViewerWidget recentDocsComboViewerWidget;
	RecentDocsDropDownButton recentDocsDropDownBtn;
	
	Button manageCollectionsBtn;
	Button showActivityWidgetBtn;
	
	MenuItem openLocalDocBtn, importBtn, exportBtn;
	Button findBtn;
	
	Menu editCollectionMenu;
	MenuItem collectionUsersBtn;
	MenuItem createCollectionBtn;
	MenuItem deleteCollectionBtn, modifyCollectionBtn;
	
	Button openEditCollectionMenuBtn;

	Storage store = Storage.getInstance();
	Composite remoteDocsGroup;
	Composite container;
	
	Button loginBtn;
	
	Button docManager, userManager;
	Button showJobsBtn, showVersionsBtn;
	
	ServerWidgetListener serverWidgetListener;
	List<Object> userControls = new ArrayList<>();
	
	Menu docOverviewMenu;

	MenuItem moveToCollectionMenuItem;
	MenuItem addToCollectionMenuItem;
	MenuItem removeFromCollectionMenuItem;
	MenuItem deleteDocMenuItem;
	MenuItem duplicateDocMenuItem;
	
	ToolItem shareDocumentTi, deleteDocTi, manageUsersTi, duplicateDocTi, administerCollectionTi, recycleBin, exportCollInfoTi;
	TextToolItem quickLoadByDocId;

	ModelDetailsDialog modelDetailsDialog;
		
	public ServerWidget(Composite parent) {
		super(parent, SWT.NONE);
				
		init();
		addListener();
		updateLoggedIn();
	}
	
	private void addListener() {
		serverWidgetListener = new ServerWidgetListener(this);
	}
	
	void updateLoggedIn() {
		boolean isLoggedIn = store.isLoggedIn();

		for (Object c : userControls) {
			SWTUtil.setEnabled(c, isLoggedIn);
		}
		
		if (!isLoggedIn) {
			setSelectedCollection(null);
			clearDocList();
			
		}
		
		updateHTRTreeViewer();
	}
	
	private void init() {
		this.setLayout(new GridLayout());
				
		container = new Composite(this, SWT.NONE);
		container.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		container.setLayout(SWTUtil.createGridLayout(1, false, 0, 0));
		
		loginBtn = new Button(container, 0);
		loginBtn.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		loginBtn.setImage(Images.DISCONNECT);
		Fonts.setBoldFont(loginBtn);

		Composite btns1 = new Composite(container, 0);
		btns1.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		btns1.setLayout(SWTUtil.createGridLayout(2, true, 0, 0));
		
		DropDownButton docDropDown = new DropDownButton(btns1, SWT.PUSH, "Document...", Images.FOLDER, null);
		docDropDown.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		openLocalDocBtn = docDropDown.addItem("Open local document...", Images.FOLDER, SWT.PUSH);
		importBtn = docDropDown.addItem("Import document to server...", Images.FOLDER_IMPORT, SWT.PUSH);
		userControls.add(importBtn);
		exportBtn = docDropDown.addItem("Export document to your local machine...", Images.FOLDER_GO, SWT.PUSH);
		userControls.add(exportBtn);
		
		if (true) {
		
		findBtn = new Button(btns1, SWT.PUSH);
		findBtn.setText("Find");
		findBtn.setToolTipText("Find documents, text or tags");
		findBtn.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		findBtn.setImage(Images.FIND);
		userControls.add(findBtn);
		}
		
		docManager = new Button(btns1, 0);
		docManager.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		docManager.setText("Document Manager");
		docManager.setToolTipText("Add pages, add transcripts, choose symbolic images, ...");
		docManager.setImage(Images.FOLDER_WRENCH);
		userControls.add(docManager);
			
		userManager = new Button(btns1, 0);
		userManager.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		userManager.setText("User Manager");
		userManager.setToolTipText("Add/Remove users to your collections");
		userManager.setImage(Images.USER_EDIT);
		userControls.add(userManager);
		
		showVersionsBtn = new Button(btns1, 0);
		showVersionsBtn.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		showVersionsBtn.setText("Versions");
		showVersionsBtn.setToolTipText("Show versions of the current page");
		showVersionsBtn.setImage(Images.PAGE_WHITE_STACK);
		userControls.add(showVersionsBtn);
		
		showJobsBtn = new Button(btns1, 0);
		showJobsBtn.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		showJobsBtn.setText("Jobs");
		showJobsBtn.setToolTipText("Show jobs on server");
		showJobsBtn.setImage(Images.CUP);
		userControls.add(showJobsBtn);

		recentDocsDropDownBtn = new RecentDocsDropDownButton(btns1);
		recentDocsDropDownBtn.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		
		showActivityWidgetBtn = new Button(btns1, SWT.PUSH);
		showActivityWidgetBtn.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		showActivityWidgetBtn.setImage(Images.GROUP);
		showActivityWidgetBtn.setText("User activity");
		userControls.add(showActivityWidgetBtn);
		
		Composite collsCont = new Composite(container, 0);
		collsCont.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		collsCont.setLayout(SWTUtil.createGridLayout(3, false, 0, 0));
		
		Label collectionsLabel = new Label(collsCont, 0);
		collectionsLabel.setText("Collections:");
		Fonts.setBoldFont(collectionsLabel);
		
		collectionSelectorWidget = new CollectionSelectorWidget(collsCont, 0, false, null);
		collectionSelectorWidget.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1));
		
		quickLoadColId = new Text(collsCont, SWT.NONE);
		SWTUtil.addSelectOnFocusToText(quickLoadColId);
		quickLoadColId.setMessage("Col-ID");
		quickLoadColId.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, true, 1, 1));
		quickLoadColId.setToolTipText("Load collection with this ID");
		
		if (false) {
			Composite collComp = collectionSelectorWidget.getCollComposite();
//			collComp.setLayout(SWTUtil.createGridLayout(2, false, 0, 0)); // have to change nr of columns to add a new buttons
			openEditCollectionMenuBtn = new Button(collComp, SWT.PUSH);
			openEditCollectionMenuBtn.setImage(Images.PENCIL);
			openEditCollectionMenuBtn.setToolTipText("Manage collection...");
			openEditCollectionMenuBtn.addSelectionListener(new SelectionAdapter() {			
				@Override
				public void widgetSelected(SelectionEvent e) {
					Point loc = openEditCollectionMenuBtn.getLocation();
	                Rectangle rect = openEditCollectionMenuBtn.getBounds();
	                Point mLoc = new Point(loc.x-1, loc.y+rect.height);
	
	                editCollectionMenu.setLocation(getShell().getDisplay().map(openEditCollectionMenuBtn.getParent(), null, mLoc));
	                editCollectionMenu.setVisible(true);
				}
			});
			
			editCollectionMenu = new Menu(getShell(), SWT.POP_UP);
			
			createCollectionBtn = new MenuItem(editCollectionMenu, SWT.PUSH);
			createCollectionBtn.setImage(Images.ADD);
			createCollectionBtn.setText("Create a new collection...");
			
			deleteCollectionBtn = new MenuItem(editCollectionMenu, SWT.PUSH);
			deleteCollectionBtn.setImage(Images.DELETE);
			deleteCollectionBtn.setText("Delete this collection...");
			
			modifyCollectionBtn = new MenuItem(editCollectionMenu, SWT.PUSH);
			modifyCollectionBtn.setText("Edit metadata of collection...");
			
			collectionUsersBtn = new MenuItem(editCollectionMenu, SWT.PUSH);
			collectionUsersBtn.setImage(Images.USER_EDIT);
			collectionUsersBtn.setText("Manage users in collection...");
			
			collComp.layout();
		}
		userControls.add(collectionSelectorWidget);
		
		// tabFolder contains documents tab item and ground-truth tab item
		tabFolder = new CTabFolder(container, SWT.BORDER | SWT.FLAT);
		tabFolder.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));

		//create documents tab item
		documentsTabItem = new CTabItem(tabFolder, SWT.NONE);
		documentsTabItem.setText("Documents");		
		//container for documents tab elements
		remoteDocsGroup = new Composite(tabFolder, 0);
		remoteDocsGroup.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		remoteDocsGroup.setLayout(SWTUtil.createGridLayout(1, false, 0, 0));
		
		documentsTabItem.setControl(remoteDocsGroup);
		tabFolder.setSelection(documentsTabItem);

		docTableWidget = new DocTableWidgetPagination(remoteDocsGroup, SWT.MULTI, 100, false);
		docTableWidget.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		userControls.add(docTableWidget);
		
		ColumnViewerToolTipSupport.enableFor(docTableWidget.getPageableTable().getViewer(), ToolTip.NO_RECREATE);
		docTableWidget.getPageableTable().setToolTipText("");
		
		// 	get top toolbar of docTableWidget:
		Control c = docTableWidget.getPageableTable().getCompositeTop().getChildren()[0];
		ToolBar tb = (ToolBar) c;
		
		new ToolItem(tb, SWT.SEPARATOR);

		shareDocumentTi = new ToolItem(tb, SWT.PUSH);
		shareDocumentTi.setImage(Images.PAGE_LINK);
		shareDocumentTi.setToolTipText("Move or share the document to other collections...");
		
		deleteDocTi = new ToolItem(tb, SWT.PUSH);
		deleteDocTi.setImage(Images.FOLDER_DELETE);
		deleteDocTi.setToolTipText("Delete the selected documents from Transkribus...");
		
		manageUsersTi = new ToolItem(tb, SWT.PUSH);
		manageUsersTi.setImage(Images.USER_EDIT);
		manageUsersTi.setToolTipText("Manage users in this collection...");
		
		duplicateDocTi = new ToolItem(tb, SWT.PUSH);
		duplicateDocTi.setImage(Images.PAGE_COPY);
		duplicateDocTi.setToolTipText("Duplicate/Copy the selected documents into another collection...");
		
		administerCollectionTi = new ToolItem(tb, SWT.PUSH);
		administerCollectionTi.setImage(Images.FOLDER_WRENCH);
		administerCollectionTi.setToolTipText("Document Manager: add pages & transcripts, choose symbolic images, ...");
		
		recycleBin = new ToolItem(tb, SWT.PUSH);
		recycleBin.setImage(Images.BIN);
		recycleBin.setToolTipText("Contains deleted documents!");
		
		exportCollInfoTi = new ToolItem(tb, SWT.PUSH);
		exportCollInfoTi.setImage(Images.ARROW_DOWN);
		exportCollInfoTi.setToolTipText("Export document list as CSV...");		
		
		quickLoadByDocId = new TextToolItem(tb, SWT.NONE);
		quickLoadByDocId.setAutoSelectTextOnFocus();
		quickLoadByDocId.setMessage("Doc-ID");
		quickLoadByDocId.resizeToMessage();
		quickLoadByDocId.setToolTipText("Load document with this id from the first collection it is contained");
		
		if (remoteDocsGroup instanceof SashForm) {
			((SashForm)remoteDocsGroup).setWeights(new int[]{50, 50});
		}
		
		remoteDocsGroup.layout();
		
		/*
		 * Create ground truth treeviewer. 
		 * The tab item is created/disposed depending on availability of data in collection
		 */
		groundTruthTreeWidget = createGroundTruthTreeWidget(tabFolder);

		SWTUtil.setTabFolderBoldOnItemSelection(tabFolder);
		
		initDocOverviewMenu();
		
		updateHighlightedRow();
	}
	
	private GroundTruthPagedTreeWidget createGroundTruthTreeWidget(CTabFolder parent) {
		GroundTruthPagedTreeWidget tw = new GroundTruthPagedTreeWidget(parent, SWT.BORDER, null, null,
				new ModelGroundTruthContentProvider(null), new ModelGroundTruthTableLabelAndFontProvider(getFont()));
		tw.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		return tw;
	}

	public void updateHTRTreeViewer() {
		
		//run UI update asynchronously
		getDisplay().asyncExec(new Runnable() {
			public void run() {
				if(SHOW_GT_TAB_ITEM || store.isAdminLoggedIn()) {
					if (gtTabItem == null || gtTabItem.isDisposed()) {
						gtTabItem = new CTabItem(tabFolder, SWT.NONE);
						gtTabItem.setText("Model Data");
						gtTabItem.setControl(groundTruthTreeWidget);
					}
					groundTruthTreeWidget.loadFirstPage();
				} else if(gtTabItem != null) {
						logger.debug("dispose the htr tree widget");
						gtTabItem.dispose();
						gtTabItem = null;
				}
			}
		});
	}
	
	public void showModelDetailsDialog(TrpModelMetadata model) {
		if(model == null) {
			return;
		}
		if(modelDetailsDialog == null || modelDetailsDialog.isDisposed()) {
			modelDetailsDialog = new ModelDetailsDialog(getShell(), model);
			modelDetailsDialog.open();
		} else {
			modelDetailsDialog.setVisible();
			modelDetailsDialog.setModel(model);
		}
	}
	
	private void initDocOverviewMenu() {
		Table t = docTableWidget.getPageableTable().getViewer().getTable();
		docOverviewMenu = new Menu(t);
		t.setMenu(docOverviewMenu);

		moveToCollectionMenuItem = new MenuItem(docOverviewMenu, SWT.PUSH);
		moveToCollectionMenuItem.setImage(Images.ARROW_RIGHT);
		moveToCollectionMenuItem.setText("Move to different collection...");

		addToCollectionMenuItem = new MenuItem(docOverviewMenu, SWT.PUSH);
		addToCollectionMenuItem.setImage(Images.ADD);
		addToCollectionMenuItem.setText("Link to different collection...");
		
		removeFromCollectionMenuItem = new MenuItem(docOverviewMenu, SWT.PUSH);
		removeFromCollectionMenuItem.setImage(Images.DELETE);
		removeFromCollectionMenuItem.setText("Unlink from this collection...");		
		
		deleteDocMenuItem = new MenuItem(docOverviewMenu, SWT.PUSH);
		deleteDocMenuItem.setImage(Images.FOLDER_DELETE);
		deleteDocMenuItem.setText("Delete document...");
		
		duplicateDocMenuItem = new MenuItem(docOverviewMenu, SWT.PUSH);
		duplicateDocMenuItem.setImage(Images.PAGE_COPY);
		duplicateDocMenuItem.setText("Duplicate/Copy...");
	}
	
	private void configExpandable(ExpandableComposite exp, Composite client, String text, final Composite container, boolean expand) {
		exp.setClient(client);
		exp.setText(text);
		exp.addExpansionListener(new ExpansionAdapter() {
		      public void expansionStateChanged(ExpansionEvent e) {
		    	  container.layout();		    	  
		      }
		    });
		exp.setExpanded(expand);
	}
	
	public void selectCollectionsTab() {
		if(!isCollectionsTabSelected()) {
			tabFolder.setSelection(documentsTabItem);
			SWTUtil.setBoldFontForSelectedCTabItem(tabFolder);
		}
	}
	
	public boolean isCollectionsTabSelected() {
		return documentsTabItem.equals(tabFolder.getSelection());
	}
	
	public boolean isGroundTruthTabSelected() {
		return gtTabItem.equals(tabFolder.getSelection());
	}
	
	public void updateHighlightedRow() {
		docTableWidget.getTableViewer().refresh();
	}

	public void refreshDocListFromStorage() {
		docTableWidget.refreshList(getSelectedCollectionId(), true, false);
	}
		
	public void clearDocList() {
		docTableWidget.refreshList(0, true, false);
	}
	
	public DocTableWidgetPagination getDocTableWidget() { return docTableWidget; }
	
	public TableViewer getTableViewer() { return docTableWidget.getTableViewer(); }
	public Label getUsernameLabel() { return usernameLabel; }
	public Button getLoginBtn() { return loginBtn; }
		
	public TrpDocMetadata getSelectedDocument() {
		return docTableWidget.getFirstSelected();
	}
	
	public List<TrpDocMetadata> getSelectedDocuments() {
		return docTableWidget.getSelected();
	}
	
	public int getSelectedCollectionId() {
		return collectionSelectorWidget.getSelectedCollectionId();
	}
	
	public TrpCollection getSelectedCollection() {
		return collectionSelectorWidget.getSelectedCollection();
	}
	
	public String getSelectedRecentDoc(){
		return recentDocsComboViewerWidget!=null ? recentDocsComboViewerWidget.getSelectedDoc() : null;
	}
		
	public void setSelectedCollection(TrpCollection trpCollection) {
		collectionSelectorWidget.setSelectedCollection(trpCollection);
	}
				
	public void updateRecentDocs() {
		if (recentDocsComboViewerWidget!=null) {
			recentDocsComboViewerWidget.setRecentDocs(false);	
		}
	}
	
	public void updateBtnVisibility(boolean canManage){
		deleteDocTi.setEnabled(canManage);
		manageUsersTi.setEnabled(canManage);
		shareDocumentTi.setEnabled(canManage);
		duplicateDocTi.setEnabled(canManage);
		administerCollectionTi.setEnabled(canManage);
		recycleBin.setEnabled(canManage);
		exportCollInfoTi.setEnabled(canManage);

		moveToCollectionMenuItem.setEnabled(canManage);
		addToCollectionMenuItem.setEnabled(canManage);
		moveToCollectionMenuItem.setEnabled(canManage);
		removeFromCollectionMenuItem.setEnabled(canManage);
		duplicateDocMenuItem.setEnabled(canManage);
		deleteDocMenuItem.setEnabled(canManage);
	}

	public Button getShowJobsBtn() { return showJobsBtn; }
	public Button getShowVersionsBtn() { return showVersionsBtn; }
}
