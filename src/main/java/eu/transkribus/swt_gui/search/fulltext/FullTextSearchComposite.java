package eu.transkribus.swt_gui.search.fulltext;

import eu.transkribus.client.util.SessionExpiredException;
import eu.transkribus.core.exceptions.NotImplementedException;
import eu.transkribus.core.model.beans.TrpCollection;
import eu.transkribus.core.model.beans.TrpDoc;
import eu.transkribus.core.model.beans.TrpDocMetadata;
import eu.transkribus.core.model.beans.enums.SearchType;
import eu.transkribus.core.model.beans.job.TrpJobStatus;
import eu.transkribus.core.model.beans.pagecontent_trp.TrpLocation;
import eu.transkribus.core.model.beans.pagecontent_trp.TrpPageType;
import eu.transkribus.core.model.beans.rest.JaxbList;
import eu.transkribus.core.model.beans.searchresult.Facet;
import eu.transkribus.core.model.beans.searchresult.FulltextSearchResult;
import eu.transkribus.core.model.beans.searchresult.PageHit;
import eu.transkribus.core.model.beans.searchresult.SearchReplaceParams;
import eu.transkribus.core.util.JaxbUtils;
import eu.transkribus.core.util.SebisStopWatch;
import eu.transkribus.swt.mytableviewer.MyTableViewer;
import eu.transkribus.swt.progress.ProgressBarDialog;
import eu.transkribus.swt.util.DialogUtil;
import eu.transkribus.swt.util.Images;
import eu.transkribus.swt.util.LabeledText;
import eu.transkribus.swt.util.SWTUtil;
import eu.transkribus.swt_gui.mainwidget.TrpMainWidget;
import eu.transkribus.swt_gui.mainwidget.storage.Storage;
import eu.transkribus.swt_gui.metadata.CustomTagSpec;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.dea.fimagestore.core.util.FilekeyUtils;
import org.dea.fimgstoreclient.FimgStoreGetClient;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StyledCellLabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.ViewerCell;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.custom.StyleRange;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.events.TraverseEvent;
import org.eclipse.swt.events.TraverseListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.ClientErrorException;
import javax.ws.rs.ServerErrorException;
import javax.ws.rs.client.InvocationCallback;
import javax.xml.bind.JAXBException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FullTextSearchComposite extends Composite{
	private final static Logger logger = LoggerFactory.getLogger(FullTextSearchComposite.class);
	Group facetsGroup;
	LabeledText inputText;
	Button currentDocCheck, wholeWordCheck, caseSensitiveCheck, previewCheck, fuzzyCheck;
	Button searchBtn, searchPrevBtn, searchNextBtn;
	Composite parameters, booleanParameters;
	Composite facetComp;
	//Button[] textTypeBtn;
	Button[] booleanTypeButton;
	FulltextSearchResult fullTextSearchResult;
	Tree facetTree;
	Table resultsTable;
	TableViewer viewer;
	TreeViewer facetViewer;
//	SashForm resultsSf;
	Composite resultsSf;
	Label resultsLabel;
	String lastHoverCoords;
	Shell shell;
	
	Composite replaceComp1, replaceComp2;
	Composite annotateComp;
	
	Text replaceText;
	Button replaceBtn;
	Button replaceAllInDoc;
	Button replaceAllInColl;
	Combo tagSpecificationsCombo;
	Button annotateBtn;
	Button annotateAllBtn;
	
	boolean noMultiCombos = true;
	
	Label facetLabel;
	ToolBar facetToolBar;
	ToolItem colItem;
	ToolItem docItem;
	ToolItem authorItem;
	ToolItem uploaderItem;
	public MultiSelectionCombo collCombo;
	MultiSelectionCombo docCombo;
	MultiSelectionCombo authCombo;
	public MultiSelectionCombo uplCombo;
	
	//Button useNewIndexCheck;
	
	List<String> filters;
	Storage storage;
	
	volatile ArrayList<Hit> hits;
	
	Thread imgLoaderThread;
	volatile HashMap<String,Image> prevImages;
	
	FimgStoreGetClient imgStoreClient;
	
	boolean enableHover;
	
	final int rows = 100;
	int start = 0;
	String searchText;
	private String lastSearchText;
	private Integer numPageHits;
	private static final String BAD_SYMBOLS = "(,[,+,-,:,=,],),#,~,\"";
	private SearchType type;
	
	JaxbList<PageHit> pageHits = new JaxbList<PageHit>();
	Map<String, Hit> selectedHits = new HashMap<String, Hit>();

	public FullTextSearchComposite(Composite parent, int style){
		super(parent, style);
		shell = parent.getShell();	
		
		storage = Storage.getInstance();
		imgStoreClient = storage.getConnection().newFImagestoreGetClient();

		createContents();
		
	}
		
	protected void createContents(){
		
		this.setLayout(new FillLayout());
		Composite c = new Composite(this, 0);
		c.setLayout(new FillLayout());
				
		SashForm sf = new SashForm(c, SWT.VERTICAL);
		sf.setLayout(new GridLayout(1, false));		
		
		facetsGroup = new Group(sf, SWT.NONE);
		facetsGroup.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));	
		facetsGroup.setLayout(new GridLayout(2, false));
		facetsGroup.setText("Search transcribed text for words or phrases");
		
		TraverseListener findTagsOnEnterListener = new TraverseListener() {
			@Override public void keyTraversed(TraverseEvent e) {
				if (e.detail == SWT.TRAVERSE_RETURN) {
					findText(false,false);
				}
			}
		};

		booleanParameters = new Composite(facetsGroup, 0);
		booleanParameters.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 4, 1));
		booleanParameters.setLayout(new GridLayout(4, false));	

		Label booleanText = new Label(booleanParameters, SWT.LEFT);
		booleanText.setText("Search for: ");

		booleanTypeButton = new Button[3];
		booleanTypeButton[0] = new Button(booleanParameters, SWT.RADIO);
		booleanTypeButton[0].setSelection(true);
		booleanTypeButton[0].setText("Any word");
		booleanTypeButton[0].setToolTipText("Search any of the specified words");

		booleanTypeButton[1] = new Button(booleanParameters, SWT.RADIO);
		booleanTypeButton[1].setSelection(false);
		booleanTypeButton[1].setText("All words");
		booleanTypeButton[1].setToolTipText("Search all of the specified words");

		booleanTypeButton[2] = new Button(booleanParameters, SWT.RADIO);
		booleanTypeButton[2].setSelection(false);
		booleanTypeButton[2].setText("Exact phrase");
		booleanTypeButton[2].setToolTipText("Search for exact phrase");

		booleanTypeButton[0].addSelectionListener(new SelectionListener(){

			@Override
			public void widgetSelected(SelectionEvent e) {
				fuzzyCheck.setEnabled(true);
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {}
		});

		
		booleanTypeButton[1].addSelectionListener(new SelectionListener(){

			@Override
			public void widgetSelected(SelectionEvent e) {
				fuzzyCheck.setEnabled(true);
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {}
		});

		
		booleanTypeButton[2].addSelectionListener(new SelectionListener(){

			@Override
			public void widgetSelected(SelectionEvent e) {

				if(fuzzyCheck.getEnabled()){
					if(fuzzyCheck.getSelection()){
						DialogUtil.showErrorMessageBox(getShell(), "Warning", "Solr search does not support fuzzy phrases.");
					}
				}
				fuzzyCheck.setSelection(false);
				fuzzyCheck.setEnabled(false);
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {}
		});
		
		inputText = new LabeledText(facetsGroup, "Input:");
		inputText.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 2, 1));
		inputText.text.addTraverseListener(findTagsOnEnterListener);
		
		parameters = new Composite(facetsGroup, 0);
		parameters.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 2, 2));
		parameters.setLayout(new GridLayout(4, false));		
		
		previewCheck = new Button(parameters, SWT.CHECK);
		previewCheck.setText("Show word preview");
		previewCheck.setSelection(true);
		previewCheck.setToolTipText("Automatic loading of word image preview. Works better with word-based text. Guesses word coordinates for line-based text.");
		
		currentDocCheck = new Button(parameters, SWT.CHECK);
		currentDocCheck.setText("Current document");
		currentDocCheck.addSelectionListener(new SelectionListener(){

			@Override
			public void widgetSelected(SelectionEvent e) {

				if(currentDocCheck.getSelection()){
					if(storage.getDoc() == null){
						DialogUtil.showErrorMessageBox(getShell(), "Error", "No document loaded.");
						currentDocCheck.setSelection(false);
					}
					else{
//						TrpDoc currentDoc = storage.getDoc();	
//						String currentTitle = currentDoc.getMd().getTitle();
//						collCombo.txtCurrentSelection.setEnabled(false);
//						docCombo.txtCurrentSelection.setEnabled(false);
//						authCombo.txtCurrentSelection.setEnabled(false);
//						uplCombo.txtCurrentSelection.setEnabled(false);
//						
//						filters = new ArrayList<String>();
//						filters.add("(f_title:\""+currentTitle+"\")");
//						
//						collCombo.txtCurrentSelection.setText("");
//						docCombo.txtCurrentSelection.setText("");
//						authCombo.txtCurrentSelection.setText("");
//						uplCombo.txtCurrentSelection.setText("");
						setCurrentDoc();
					}
				}else{
					collCombo.txtCurrentSelection.setEnabled(true);
					docCombo.txtCurrentSelection.setEnabled(true);
					authCombo.txtCurrentSelection.setEnabled(true);
					uplCombo.txtCurrentSelection.setEnabled(true);
					
					filters = null;
					
					collCombo.txtCurrentSelection.setText("All collections");
					docCombo.txtCurrentSelection.setText("All documents");
					authCombo.txtCurrentSelection.setText("All authors");
					uplCombo.txtCurrentSelection.setText("All uploaders");					
				}
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				// TODO Auto-generated method stub
				
			}
			
		});

		
//		textTypeBtn = new Button[2];
//		textTypeBtn[0] = new Button(parameters, SWT.RADIO);
//		textTypeBtn[0].setSelection(false);
//		textTypeBtn[0].setText("Word-based text");
//		textTypeBtn[0].setToolTipText("Search documents transcribed word by word");
//		textTypeBtn[1] = new Button(parameters, SWT.RADIO);
//		textTypeBtn[1].setSelection(true);
//		textTypeBtn[1].setText("Line-based text");	
//		textTypeBtn[1].setToolTipText("Search documents transcribed line by line");
		
		caseSensitiveCheck = new Button(parameters, SWT.CHECK);
		caseSensitiveCheck.setText("Case sensitive");
		
		fuzzyCheck = new Button(parameters, SWT.CHECK);
		fuzzyCheck.setText("Fuzzy search");
		fuzzyCheck.setSelection(false);
		
//		if (Storage.i()!=null && Storage.i().isAdminLoggedIn()) {
//			useNewIndexCheck = new Button(parameters, SWT.CHECK);
//			useNewIndexCheck.setText("Use new index (alpha)");
//			useNewIndexCheck.setToolTipText("Uses the new SOLR8 based index for search - in alpha stage, data could be missing or invalid!");
//			useNewIndexCheck.setSelection(true);
//		}
		
//		Button helpBtn = new Button(parameters, SWT.PUSH);
//		helpBtn.setImage(Images.HELP);
//		helpBtn.setText("");
//		helpBtn.addSelectionListener(new SelectionAdapter(){
//			@Override
//			public void widgetSelected(SelectionEvent e){
//				DialogUtil.showInfoMessageBox(shell, "Solr search quickreference", 
//						"Solr search supports searching for single words or phrases.\n"
//						+ "\n"
//						+ "The following options are available:\n"
//						+ "Current document:\tLimit search to currently loaded document\n"
//						+ "Case sensitive:\tDifferentiate between upper and lower case\n"
//						+ "Show word preview:\tDisplays a small preview image of matched word\n"
//						+ "\t\tif available when hovering with mouse\n"
//						+ "Word based text:\tSearch text transcribed word by word\n"
//						+ "Line based text:\tSearch text transcribed line by line\n"
//						+ "\n"
//						+ "\n"
//						+ "The search field also supports various commands:\n"
//						+ "\"...\" for exact phrasing\n"
//						+ "? as single character wildcard\n"
//						+ "* as multi-character wildcard\n"
//						+ "~ for proximity searches\n"
//						+ "Example 1: \"word1 word2\"~10 will search for word1 and word2 with\n"
//						+ "a maximum of 10 characters in between\n"
//						+ "Example 2: roam~ will also match foam or foams\n"
//						+ "(roam~1 allows a maximum character distance of 1, so it will match foam but not foams)\n"
//						+ "Boolean operators:\n"
//						+ "&&\trequires both terms on either side to be present\n"
//						+ "!\trequires that the following term not be present\n"
//						+ "||\trequires that either (or both) terms be present\n"
//						+ "\n"
//						+ "Search results can be narrowed down to specific collections, authors,\n"
//						+ "documents and/or uploaders using the drop down tools.\n"
//						+ "\n"
//						
//						);
//			}
//		});
						
		Composite btnsComp = new Composite(facetsGroup, 0);
		btnsComp.setLayout(new FillLayout(SWT.HORIZONTAL));
		btnsComp.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 2, 1));
		
		searchBtn = new Button(btnsComp, SWT.PUSH);
		searchBtn.setImage(Images.FIND);
		searchBtn.setText("Search!");
		searchBtn.setToolTipText("Search for text");
		searchBtn.addSelectionListener(new SelectionAdapter() {
			@Override public void widgetSelected(SelectionEvent e) {
				start = 0;
				selectedHits.clear();
				findText(false,false);
			}
		});
		
		searchPrevBtn = new Button(btnsComp, SWT.PUSH);
		searchPrevBtn.setImage(Images.PAGE_PREV);
		searchPrevBtn.setText("Previous page");
		searchPrevBtn.addSelectionListener(new SelectionAdapter() {
			@Override public void widgetSelected(SelectionEvent e) {
					if(start > 0){
						start -= rows;
						selectedHits.putAll(getCheckedHitsAsMap());
						findText(false,false);
				}
				
			}
		});
		
		searchNextBtn = new Button(btnsComp, SWT.PUSH);
		searchNextBtn.setImage(Images.PAGE_NEXT);
		searchNextBtn.setText("Next page");
		searchNextBtn.addSelectionListener(new SelectionAdapter() {
			@Override public void widgetSelected(SelectionEvent e) {
				if(fullTextSearchResult != null){
					if((start+rows) <= numPageHits){
						start += rows;
						selectedHits.putAll(getCheckedHitsAsMap());
//						JaxbList<PageHit> newHits = getPageHits(getCheckedHits());
//						pageHits.getList().addAll(newHits.getList());
						findText(false,false);
						
					}
				}
				
			}
		});			

//		Composite filters;
//		filters = new Composite(facetsGroup, 0);
//		filters.setLayout(new GridLayout(3, false));
//		filters.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 2, 1));		

		initResultsTable(sf);
		sf.setWeights(new int[] { 25, 75 } );
	}	
	
	void initResultsTable(Composite container){
		Group resultsGroup = new Group(container, SWT.NONE);
		resultsGroup.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		resultsGroup.setText("Search results");
		resultsGroup.setLayout(new GridLayout(1, false));
		
		resultsLabel = new Label(resultsGroup, 0);
		resultsLabel.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1));		

		initFacetSf(resultsGroup);
        
//		resultsSf = new SashForm(resultsGroup, SWT.HORIZONTAL);
		resultsSf = new Composite(resultsGroup, SWT.NONE);
		resultsSf.setLayout(new GridLayout(1, false));
		resultsSf.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		
        viewer = new MyTableViewer(resultsSf,SWT.CHECK);
        viewer.getTable().setLayoutData(new GridData(GridData.FILL_BOTH));
        
        viewer.getTable().setHeaderVisible(true);
        viewer.getTable().setLinesVisible(true);
        viewer.setContentProvider(new ArrayContentProvider());
        
        //List<TrpDocMetadata> docList = storage.getDocList();

        TableColumn column = new TableColumn(viewer.getTable(), SWT.CHECK);
        column.setText("Context");
        column.setWidth(650);
        TableViewerColumn contextCol = new TableViewerColumn(viewer, column);
        
        contextCol.setLabelProvider(new StyledCellLabelProvider(){

        	  @Override
        	  public void update(ViewerCell cell) {
        		Hit h = (Hit)cell.getElement();
        		String hlText = h.getHighlightText();
        		//logger.debug("hlText = "+hlText+", url = "+h.getImgUrl());
        		
        		//Word highlighting
        	    cell.setText( hlText.replaceAll("<em>", "").replaceAll("</em>", "") );
        	    
        	    int hlCount = StringUtils.countMatches(hlText, "</em>");
        	    StyleRange[] myStyledRange = new StyleRange[hlCount];
        	    
        	    for(int i = 0; i<hlCount; i++){
            	    int hlStart = hlText.indexOf("<em>");
            	    int hlEnd = hlText.indexOf("</em>");
            	    int hlLen = hlEnd-hlStart-4;
            	    hlText = hlText.replaceFirst("<em>", "");
            	    hlText = hlText.replaceFirst("</em>", "");
        	    	myStyledRange[i] = 
                	        new StyleRange(hlStart, hlLen, null, 
                	            Display.getCurrent().getSystemColor(SWT.COLOR_YELLOW));
        	    	//logger.debug("hlText = "+hlText+", url = "+h.getImgUrl());
        	    }
        	    
        	    cell.setStyleRanges(myStyledRange);        
        	    super.update(cell);
        	  }  

        });
        
        column = new TableColumn(viewer.getTable(), SWT.NONE);
        column.setText("Document");
        column.setWidth(200);
        TableViewerColumn docCol = new TableViewerColumn(viewer, column);
        
        docCol.setLabelProvider(new ColumnLabelProvider(){
            @Override
            public String getText(Object element) {
                Hit hit = (Hit)element;
//                return Integer.toString(hit.getDocId());                
                return hit.getDocTitle();
            }
        });
        
        column = new TableColumn(viewer.getTable(), SWT.NONE);
        column.setText("DocID");
        column.setWidth(50);
        TableViewerColumn docIdCol = new TableViewerColumn(viewer, column);
        docIdCol.setLabelProvider(new ColumnLabelProvider(){
            @Override
            public String getText(Object element) {
                return ((Hit)element).getDocId()+"";
            }
        });        
        
        column = new TableColumn(viewer.getTable(), SWT.NONE);
        column.setText("Page");
        column.setWidth(50);
        TableViewerColumn pageCol = new TableViewerColumn(viewer, column);
        
        pageCol.setLabelProvider(new ColumnLabelProvider(){

            @Override
            public String getText(Object element) {
                Hit hit = (Hit)element;
                return Integer.toString(hit.getPageNr());
            }

        });
        
        
        
//        ############################################################
//        Additional columns showing debug search result information
//        ############################################################
//        column = new TableColumn(viewer.getTable(), SWT.NONE);
//        column.setText("Region");
//        column.setWidth(50);
//        TableViewerColumn regCol = new TableViewerColumn(viewer, column);
//        
//        regCol.setLabelProvider(new ColumnLabelProvider(){
//
//            @Override
//            public String getText(Object element) {
//                Hit hit = (Hit)element;
//                return hit.getRegionId();
//            }
//
//        });
//        
//        column = new TableColumn(viewer.getTable(), SWT.NONE);
//        column.setText("Line");
//        column.setWidth(50);
//        TableViewerColumn lineCol = new TableViewerColumn(viewer, column);
//        
//        lineCol.setLabelProvider(new ColumnLabelProvider(){
//
//            @Override
//            public String getText(Object element) {
//                Hit hit = (Hit)element;
//                return hit.getLineId();
//            }
//
//        });
//        
//        column = new TableColumn(viewer.getTable(), SWT.NONE);
//        column.setText("Word");
//        column.setWidth(50);
//        TableViewerColumn worCol = new TableViewerColumn(viewer, column);
//        
//        worCol.setLabelProvider(new ColumnLabelProvider(){
//        	
//            @Override
//            public String getText(Object element) {
//                Hit hit = (Hit)element;
//                return hit.getWordId();
//            }
//            
//        });
//        
//        column = new TableColumn(viewer.getTable(), SWT.NONE);
//        column.setText("Pixel Coords");
//        column.setWidth(150);
//        TableViewerColumn pixelCol = new TableViewerColumn(viewer, column);
//        pixelCol.setLabelProvider(new ColumnLabelProvider(){
//
//            @Override
//            public String getText(Object element) {
//                Hit hit = (Hit)element;
//
//                return hit.getPixelCoords();
//            }
//
//        });
        
		viewer.addDoubleClickListener(new IDoubleClickListener() {	
			@Override public void doubleClick(DoubleClickEvent event) {
				IStructuredSelection sel = (IStructuredSelection) event.getSelection();
				if (!sel.isEmpty()) {
					logger.debug("Clicked! Doc: "+ ((Hit)sel.getFirstElement()).getDocId()+ " Page: "+((Hit)sel.getFirstElement()).getPageNr());
					Hit clHit = (Hit)sel.getFirstElement();
					Storage s = Storage.getInstance();		
					
					ArrayList<Integer> userCols = new ArrayList<>();
					for(TrpCollection userCol : s.getCollections()){
						userCols.add(userCol.getColId());
//						logger.debug("User collection: " + userCol.getColId());
					}
//					logger.debug("Hit collections: " + clHit.getCollectionIds());
					int col = -1;
					for(Integer userColId : userCols){
						for(Integer hitColId : clHit.getCollectionIds()){
							
							if(userColId.equals(hitColId)){
								col = userColId;
							}
						}
					}
					//logger.debug("Col: " + col);
					if(col != -1){
						int docId = clHit.getDocId();
						int pageNr = clHit.getPageNr();
						TrpLocation l = new TrpLocation();
						
						l.collId = col;
						l.docId = docId;
						l.pageNr = pageNr;		
						l.shapeId=clHit.getLineId();
	
						TrpMainWidget.getInstance().showLocation(l);
					}
				}
				
			}
		});

		resultsTable = viewer.getTable();
		final HoverShell hShell = new HoverShell(shell);
		enableHover = true;
		resultsTable.addListener(SWT.MouseEnter, new Listener(){
			public void handleEvent(Event e){
				//enableHover = true;
//				hShell.hoverShell.setVisible(true);
//				logger.debug("Mouse inside table");
			}		
		});
		
		resultsTable.addListener(SWT.MouseExit, new Listener(){
			public void handleEvent(Event e){
				//enableHover = false;
				hShell.hoverShell.setVisible(false);
//				logger.debug("Mouse outside table");
			}
		});			
		
		resultsTable.addListener(SWT.MouseMove, new Listener(){
			public void handleEvent(Event e){
				if(enableHover == true && previewCheck.getSelection()){

					Point p = new Point(e.x,e.y);
					
					TableItem hoverItem = resultsTable.getItem(p);
					if(hoverItem != null){						
						
						String coords = ((Hit)hoverItem.getData()).getPixelCoords();
						Point mousePos = Display.getCurrent().getCursorLocation();							
						if(coords != lastHoverCoords){							
							try {						
								final String imgKey = FilekeyUtils.extractKey(new URL(((Hit)hoverItem.getData()).getImgUrl()));
								
								Image img;							
								int[] cropValues = getCropValues(coords);
								
								if(prevImages.keySet().contains(coords)){
									img = prevImages.get(coords);
								}else{
									URL url = imgStoreClient.getUriBuilder().getImgCroppedUri(imgKey, cropValues[0], cropValues[1], cropValues[2], cropValues[3]).toURL();
									img = ImageDescriptor.createFromURL(url).createImage();	
//									logger.debug("Forced loading of img coords:"+coords);
									prevImages.put(coords, img);
									
								}

								hShell.imgLabel.setImage(img);
								hShell.hoverShell.setVisible(true);
								
							} catch (Exception ex) {								
								logger.error("Could not load preview image!", ex);
								hShell.imgLabel.setText("Could not load preview image");
							}						
							
						}						
							hShell.hoverShell.setLocation(mousePos.x + 20 , mousePos.y - 20);
							hShell.hoverShell.pack();
							hShell.hoverShell.redraw();
//							logger.debug("Obj:"+((Hit)hoverItem.getData()).getPixelCoords());
							lastHoverCoords = coords;		

					}else{
						hShell.hoverShell.setVisible(false);
					}
				}
			}
		});
		
		logger.debug("can manage: " + storage.getUser().getRoleInCollection().canManage());

			replaceComp1 = new Composite(resultsSf, 0);
			replaceComp1.setLayout(new GridLayout(2, false));
			replaceComp1.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

			final Button selectAllBtn = new Button(replaceComp1, SWT.PUSH);
			selectAllBtn.setImage(Images.TICK);
			selectAllBtn.setToolTipText("Select all for replacement");
			selectAllBtn.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
			
			final Button deselectAllBtn = new Button(replaceComp1, SWT.PUSH);
			deselectAllBtn.setImage(Images.CROSS);
			deselectAllBtn.setToolTipText("Deselect all for replacement");
			deselectAllBtn.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
			
			SelectionAdapter selectAllLis = new SelectionAdapter() {
				@Override public void widgetSelected(SelectionEvent event) {
					selectAll(event.getSource() == selectAllBtn);
				}

				private void selectAll(boolean checked) {
					for (TableItem ti : resultsTable.getItems()) {
						ti.setChecked(checked);
					}
				}
			};
			
			selectAllBtn.addSelectionListener(selectAllLis);
			deselectAllBtn.addSelectionListener(selectAllLis);
			
			annotateComp = new Composite(resultsSf, 0);
			annotateComp.setLayout(new GridLayout(4, false));
			annotateComp.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
			
			Label annotateLabel = new Label(annotateComp, 0);
			annotateLabel.setText("Annotate with: ");
			annotateLabel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1));
			
			tagSpecificationsCombo = new Combo(annotateComp, SWT.SINGLE | SWT.BORDER);
			tagSpecificationsCombo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
			List<CustomTagSpec> tagSpecs = Storage.i().getCustomTagSpecs();
			for (CustomTagSpec tagSpec : tagSpecs) {
				tagSpecificationsCombo.add(tagSpec.getCustomTag().toString());
			}
			
			annotateBtn = new Button(annotateComp, 0);
			annotateBtn.setText("Annotate selected");
			annotateBtn.setToolTipText("Annotates selected search hits in current doc - also properties can be added. Adapt the tag specification for that!");
			annotateBtn.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
			annotateBtn.addSelectionListener(new SelectionAdapter() {
				@Override public void widgetSelected(SelectionEvent e) {
					if (numPageHits != null) {
						if (tagSpecificationsCombo.getSelection() == null || tagSpecificationsCombo.getSelectionIndex() == -1) {
							DialogUtil.showErrorBalloonToolTip(shell, "Annotation error", "No tag specification selected!");
							return;
						}
						selectedHits.putAll(getCheckedHitsAsMap());
						List<PageHit> newHits = getPageHitsFromMap(selectedHits);
						
						pageHits.getList().clear();
						pageHits.getList().addAll(newHits);
						if(logger.isDebugEnabled()) {
							try {
								logger.debug("ExportParameters JSON: " + JaxbUtils.marshalToJsonString(pageHits, true));
							} catch (JAXBException e1) {
								logger.error("Failed to start serialize search-replace params.", e1);
							}
						}
						try {
							
							String ccsTag = tagSpecificationsCombo.getItem(tagSpecificationsCombo.getSelectionIndex());
							if (ccsTag.isBlank() || ccsTag.isEmpty()) {
								DialogUtil.showErrorBalloonToolTip(shell, "Annotation error", "No tag specification selected!");
								return;
							}
							
							if (pageHits.getList().size() == 0) {
								DialogUtil.showErrorBalloonToolTip(shell, "Annotation error", "No hit selected!");
								return;
							}

							if (DialogUtil.showYesNoCancelDialog(shell, "Create Replace job", "Do you want to annotate " + pageHits.getList().size() + " search result(s) with '" + ccsTag +
									"'?" + System.lineSeparator()) == SWT.YES) {
								SearchReplaceParams params = new SearchReplaceParams();
								params.setPageHits(pageHits.getList());
								params.setReplaceTerm(ccsTag);
								params.setAnnotation(true);
								storage.getConnection().getSearchCalls().replaceStrings(params);
							}
						} catch (SessionExpiredException | ServerErrorException | ClientErrorException ex) {
							logger.error("Failed to start replace job.", ex);
						}
					}
				}
			});
			
			annotateAllBtn = new Button(annotateComp, 0);
			annotateAllBtn.setText("Annotate all in doc");
			annotateAllBtn.setToolTipText("Annotate selected search hits in current doc - also properties can be added. Adapt the tag specification for that!");
			annotateAllBtn.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
			annotateAllBtn.addSelectionListener(new SelectionAdapter() {
				@Override public void widgetSelected(SelectionEvent e) {
					if (numPageHits != null) {
						if (tagSpecificationsCombo.getSelection() == null || tagSpecificationsCombo.getSelectionIndex() == -1) {
							DialogUtil.showErrorBalloonToolTip(shell, "Annotation error", "No tag specification selected!");
							return;
						}
						ArrayList<Hit> myHits = null;
						try {
							setCurrentDoc();
							FulltextSearchResult myfullTextSearchResult = storage.getConnection().getSearchCalls().searchFulltext(searchText, type, start, numPageHits, filters);
							myHits = convertPageHitsIntoHits(myfullTextSearchResult);
							
						} catch (SessionExpiredException | ServerErrorException | ClientErrorException e2) {
							// TODO Auto-generated catch block
							e2.printStackTrace();
						}

						try {

							String ccsTag = tagSpecificationsCombo.getItem(tagSpecificationsCombo.getSelectionIndex());
							if (ccsTag.isBlank() || ccsTag.isEmpty()) {
								DialogUtil.showErrorBalloonToolTip(shell, "Annotation error", "No tag specification is chosen!");
								return;
							}
							if (myHits.size() == 0) {
								DialogUtil.showErrorBalloonToolTip(shell, "Annotation error", "No search hit(s) found in current doc!");
								return;
							}
							
							SearchReplaceParams params = new SearchReplaceParams();
							List<PageHit> pHits = convertHitsIntoPageHits(myHits);
							params.setPageHits(pHits);
							params.setReplaceTerm(ccsTag);
							params.setAnnotation(true);

							if (DialogUtil.showYesNoCancelDialog(shell, "Create Replace job", "Do you want to annotate " + pHits.size() + " search hits with '" + ccsTag +
									"'?" + System.lineSeparator()) == SWT.YES) {

								storage.getConnection().getSearchCalls().replaceStrings(params);
							}
						} catch (SessionExpiredException | ServerErrorException | ClientErrorException ex) {
							logger.error("Failed to start replace job.", ex);
						}
					}

				}
			});

			replaceComp2 = new Composite(resultsSf, 0);
			replaceComp2.setLayout(new GridLayout(5, false));
			replaceComp2.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
			
			Label replaceLabel = new Label(replaceComp2, 0);
			replaceLabel.setText("Replace with: ");
			replaceLabel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1));
			
			replaceText = new Text(replaceComp2, SWT.SINGLE | SWT.BORDER);
			replaceText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
			
			replaceBtn = new Button(replaceComp2, 0);
			replaceBtn.setText("Replace selected");
			replaceBtn.setToolTipText("Replaces selected hits with the text on the left and stores the affected pages");
			replaceBtn.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
			replaceBtn.addSelectionListener(new SelectionAdapter() {
				@Override public void widgetSelected(SelectionEvent e) {
					selectedHits.putAll(getCheckedHitsAsMap());
					List<PageHit> newHits = getPageHitsFromMap(selectedHits);
					
					pageHits.getList().clear();
					pageHits.getList().addAll(newHits);
					if(logger.isDebugEnabled()) {
						try {
							logger.debug("ExportParameters JSON: " + JaxbUtils.marshalToJsonString(pageHits, true));
						} catch (JAXBException e1) {
							logger.error("Failed to start serialize search-replace params.", e1);
						}
					}
					try {
						if (replaceText.getText().isBlank() || replaceText.getText().isEmpty()) {
							DialogUtil.showErrorBalloonToolTip(shell, "Replacement error", "No replacement text is set!");
							return;
						}
						if (pageHits.getList().size() == 0) {
							DialogUtil.showErrorBalloonToolTip(shell, "Replacement error", "No hit selected!");
							return;
						}

						if (DialogUtil.showYesNoCancelDialog(shell, "Create Replace job", "Do you want to replace " + pageHits.getList().size() + " search result(s) with '" + replaceText.getText() +
								"'?" + System.lineSeparator() + "The replacement will work for single words only.") == SWT.YES) {
							SearchReplaceParams params = new SearchReplaceParams();
							params.setPageHits(pageHits.getList());
							params.setReplaceTerm(replaceText.getText());
							storage.getConnection().getSearchCalls().replaceStrings(params);
						}
					} catch (SessionExpiredException | ServerErrorException | ClientErrorException ex) {
						logger.error("Failed to start search-replace job.", ex);
					}
				}
			});
			
			/*
			 * 	-> option to do a search/replace on the server for ALL search hits of one document (currently testing phase)
			 * users which 'can manage' the collection can call this method
			 * replacing in a complete collection is for admins only
			 */
			replaceAllInDoc = new Button(replaceComp2, 0);
			replaceAllInDoc.setText("Replace all in doc");
			replaceAllInDoc.setToolTipText("Replaces all search hits in the current document!");
			replaceAllInDoc.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
			replaceAllInDoc.addSelectionListener(new SelectionAdapter() {
				@Override public void widgetSelected(SelectionEvent e) {
					if (numPageHits != null) {
						if (replaceText.getText().isBlank() || replaceText.getText().isEmpty()) {
							DialogUtil.showErrorBalloonToolTip(shell, "Replacement error", "No replacement text is set!");
							return;
						}
						boolean replaceInDoc = true;
						boolean replaceInColl = false;
						findText(replaceInDoc, replaceInColl);
					}
				}
			});
			
			if (storage.isAdminLoggedIn()) {
				/*
				 * 	-> option to do a search/replace on the server for ALL search hits of one collection (currently testing phase)
				 */
				replaceAllInColl = new Button(replaceComp2, 0);
				replaceAllInColl.setText("Replace ALL in coll!");
				replaceAllInColl.setToolTipText("Replaces all search hits in the current collection - please be very carefully!");
				replaceAllInColl.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
				replaceAllInColl.addSelectionListener(new SelectionAdapter() {
					@Override public void widgetSelected(SelectionEvent e) {
						SebisStopWatch ssw = new SebisStopWatch();
						long start = ssw.start();
						if (numPageHits != null) {
							if (replaceText.getText().isBlank() || replaceText.getText().isEmpty()) {
								DialogUtil.showErrorBalloonToolTip(shell, "Replacement error", "No replacement text is set!");
								return;
							}
							boolean replaceInDoc = false;
							boolean replaceInColl = true;
							findText(replaceInDoc, replaceInColl);
						}
					}
				});
			}

		updateReplaceComp();
	}
	
	/*
	 * it takes too long to get all the hits and then send them to the server
	 * better is to search and replace without that extra step
	 */
	public void replaceAll(SebisStopWatch ssw) {
		
		logger.debug("time for search all: " + (ssw.getStart() - ssw.stop()));
		
		try {
			if (replaceText.getText().isBlank() || replaceText.getText().isEmpty()) {
				DialogUtil.showErrorBalloonToolTip(shell, "Replacement error", "No replacement text is set!");
				return;
			}

			if (DialogUtil.showYesNoCancelDialog(shell, "Create Replace job", "Do you want to search for " + searchText + " and replace all hits found with '" + replaceText.getText() +
					"'?" + System.lineSeparator() + "Please use the replacement method carefully!") == SWT.YES) {
				SearchReplaceParams params = new SearchReplaceParams();
				params.setPageHits(pageHits.getList());
				params.setReplaceTerm(replaceText.getText());
				storage.getConnection().getSearchCalls().replaceStrings(params);
			}
		} catch (SessionExpiredException | ServerErrorException | ClientErrorException ex) {
			logger.error("Failed to start search-replace job.", ex);
		}
		
	}
	
	public List<Boolean> getCheckedList() {
		List<Boolean> checked = new ArrayList<>();
		for (TableItem ti : resultsTable.getItems()) {
			checked.add(ti.getChecked());
		}
		
		return checked;
	}

	private List<PageHit> convertHitsIntoPageHits(Collection<Hit> hits) {
		logger.debug("checked hit size " + hits.size());
		List<PageHit> pageHitList = new ArrayList<>();
		for (Hit h : hits) {
			/*
			 * hit is shown in fulltext search table, contains several single hits; for several reasons only one line is shown to the user 
			 * and not every single hit of the page -> so here we create for each hit one PageHit to replace all the hits on the server
			 * for that we use the hit.
			 */
			
			List<String> allWordHits = h.getAllWordCoords();
			
			for (String wordHit : allWordHits) {
				
				PageHit currHit = new PageHit();
				currHit.setCollectionIds(h.getCollectionIds());
				currHit.setDocId(h.getDocId());
				currHit.setDocTitle(h.getDocTitle());
				//to add String as a list
				currHit.setHighlights(Stream.of(h.getHighlightText()).collect(Collectors.toList()));
				currHit.setPageNr(h.getPageNr());
				
				//logger.debug("wordHit " + wordHit);
				
				WordCoords wordCoords = getWordCoords(wordHit);

				currHit.setWordCoords(Stream.of(wordCoords.toString()).collect(Collectors.toList()));

				pageHitList.add(currHit);
			}

		}
		return pageHitList;
	}
	
	private WordCoords getWordCoords(String wCoords) {
		
		int idx=0;

		if (wCoords.split(":").length>2) {
    		String wordString = wCoords.split(":")[idx];
    		idx++;
    		if (wCoords.split(":")[idx].isEmpty()) {
    			idx++;
    		}
    		String regId = wCoords.split(":")[idx].split("/")[0];
    		String linId = wCoords.split(":")[idx].split("/")[1];
    		String worId = wCoords.split(":")[idx].split("/")[2];
    		idx++;
    		String pxCoords = wCoords.split(":")[idx];
		    		
    		return new WordCoords(wordString,regId,linId,worId,pxCoords);
		}
		return null;
	}

	private List<PageHit> getPageHitsFromMap(Map<String, Hit> hits) {
		logger.debug("checked hit size " + hits.size());
		
		return convertHitsIntoPageHits(hits.values());
	}
	
	
	private void initFacetSf(Group resultsGroup) {
		int noOfFacets = 4;
		SashForm facetSf = new SashForm(resultsGroup, SWT.HORIZONTAL);
		facetSf.setLayout(new GridLayout(1, false));
		facetSf.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, noOfFacets, 1));
				
        int[] selection = new int[] { 0 };
        
        noMultiCombos = true;
        
        String[] startItems = new String[] { "All collections" };
        collCombo = new MultiSelectionCombo(facetSf, startItems, selection, SWT.NONE, this);
        startItems = new String[] { "All documents" };
        docCombo = new MultiSelectionCombo(facetSf, startItems, selection, SWT.NONE, this);
        startItems = new String[] { "All authors" };
        authCombo = new MultiSelectionCombo(facetSf, startItems, selection, SWT.NONE, this);
        startItems = new String[] { "All uploaders" };
        uplCombo = new MultiSelectionCombo(facetSf, startItems, selection, SWT.NONE, this);
		
	}
	
	void updateFacets(){
		
		List<String> colNames = new ArrayList<String>();
		for(TrpCollection coll : storage.getCollections()){
			colNames.add(coll.getColName());
		}
		
		int i;
		int collNr = 0;
		int docNr = 0;
		for(Facet facet : fullTextSearchResult.getFacets()){
			//logger.debug(""+facet.getFacetMap().keySet().contains(colNames.get(0)));
			
			
						
			if(facet.getName().equals("f_collectionIdName")){

//				String[] facetItems = new String[facet.getFacetMap().keySet().size()+1];
				String[] facetItems = new String[colNames.size()+1];
				facetItems[0] = "All collections";
				i = 1;

				for(String key : facet.getFacetMap().keySet()){		
					String keyName = key.split(":",2)[1];				
					if(colNames.contains(keyName)){
						
						facetItems[i] = key + " ("+facet.getFacetMap().get(key)+")";
						i++;
						collNr++;
					}

				}
				if(facetItems != null){
//					collCombo.textItems = facetItems;
					collCombo.setTextItems(facetItems);

				}
				logger.debug("+search term found in " + (collNr) + " nr of collections");
			}			
			else if(facet.getName().equals("f_author")){

				String[] facetItems = new String[facet.getFacetMap().keySet().size()+1];
				facetItems[0] = "All authors";
				i = 1;

				for(String key : facet.getFacetMap().keySet()){
					
					facetItems[i] = key + " ("+facet.getFacetMap().get(key)+")";
//					logger.debug("author facet: "+ facetItems[i]);					
					i++;					
				}
//				logger.debug("author keyset: " + facet.getFacetMap().keySet());
				if(facetItems != null){
					authCombo.setTextItems(facetItems);
				}
				
			}
			else if(facet.getName().equals("f_uploader")){

				String[] facetItems = new String[facet.getFacetMap().keySet().size()+1];
				facetItems[0] = "All uploaders";
				i = 1;

				for(String key : facet.getFacetMap().keySet()){
					facetItems[i] = key + " ("+facet.getFacetMap().get(key)+")";
//					logger.debug("uploader facet: "+ facetItems[i]);
					i++;					
				}
//				logger.debug("uploader keyset: " + facet.getFacetMap().keySet());
				if(facetItems != null){
					uplCombo.setTextItems(facetItems);
				}
			}
			else if(facet.getName().equals("f_title")){

				String[] facetItems = new String[facet.getFacetMap().keySet().size()+1];
				facetItems[0] = "All documents";
				i = 1;

				for(String key : facet.getFacetMap().keySet()){
					facetItems[i] = key + " ("+facet.getFacetMap().get(key)+")";
					i++;
					docNr++;
				}
				if(facetItems != null){
					docCombo.setTextItems(facetItems);
				}
				logger.debug("+search term found in " + (docNr) + " nr of documents");
			}


		}
		
		

		
	}

//	private void initFacetToolBar(Composite parent) {
//	    facetToolBar = new ToolBar(parent, SWT.HORIZONTAL);
//	    facetToolBar.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 4, 1));
//	    
//	    colItem = new ToolItem(facetToolBar, SWT.DROP_DOWN);
//	    colItem.setText("Collections");
//	    
//	    docItem = new ToolItem(facetToolBar, SWT.DROP_DOWN);
//	    docItem.setText("Documents");
//	    
//	    authorItem = new ToolItem(facetToolBar, SWT.DROP_DOWN);
//	    authorItem.setText("Authors");
//	    
//	    uploaderItem = new ToolItem(facetToolBar, SWT.DROP_DOWN);
//	    uploaderItem.setText("Uploaders");	    
//	    
//	}

	public static int[] getCropValues(String coords){
		int[] values = new int[4];
		
		String[] singleCoords = coords.split(" ");
		
		ArrayList<Integer> xPts = new ArrayList<Integer>();
		ArrayList<Integer> yPts = new ArrayList<Integer>();	
		
		for(String s: singleCoords){
			String[] PtCoords = s.split(",");
			xPts.add(Integer.parseInt(PtCoords[0]));					//All X coords
			yPts.add(Integer.parseInt(PtCoords[1]));					//All Y coords
		}
		
		int CoordX1 = xPts.get(0);
		int CoordX2 = xPts.get(0);
		int CoordY1 = yPts.get(0);
		int CoordY2 = yPts.get(0);
		
		//find outlining points
		for(int x : xPts){
			if(CoordX1>x){
				CoordX1 = x;
			}
			if(CoordX2<x){
				CoordX2 = x;
			}
		}
		for(int y : yPts){
			if(CoordY1>y){
				CoordY1 = y;
			}
			if(CoordY2<y){
				CoordY2 = y;
			}
		}			
	
		int width = Math.abs(CoordX2-CoordX1);
		int height = Math.abs(CoordY2-CoordY1);
		
		width = (int)(width*2f);
		height = (int)(height*2f);
		CoordX1-=(int)(width*0.25f);
		CoordY1-=(int)(height*0.25f);
		
		values[0]=CoordX1;
		values[1]=CoordY1;
		values[2]=width;
		values[3]=height;		
		
		return values;
	}
	
	public void findText(boolean replaceInDoc, boolean replaceInColl){	
		prevImages = new HashMap<String,Image>();
		searchText = inputText.getText().toString().trim();
		
		for(String c : BAD_SYMBOLS.split(",")){
			searchText = searchText.replaceAll("\\"+c, "");
		}

		String booleanType = "or";

		if (booleanTypeButton[1].getSelection()){
			booleanType = "and";
		}else if (booleanTypeButton[2].getSelection()){
			booleanType = "phrase";
		}

		switch (booleanType){
			case "or":
				if (fuzzyCheck.getSelection()){
					searchText = searchText.replaceAll("\\s+", "~2 || ") + "~2";
				}else{
					searchText = searchText.replaceAll("\\s+", " || ");
				}
				break;
			case "and":
				if (fuzzyCheck.getSelection()){
					searchText = searchText.replaceAll("\\s+", "~2 && ") + "~2";
				}else{
					searchText = searchText.replaceAll("\\s+", " && ");
				}
				break;
			case "phrase":
				searchText = "\"" + searchText +"\"";
				break;
		}		
		
		searchText = searchText.trim();
		
		if(searchText.isEmpty()) {
			return;
		}
		
		if(!searchText.equals(lastSearchText)) start = 0;	
		
		if(caseSensitiveCheck.getSelection() == true){
			type = SearchType.UpperCase;
		}else if(caseSensitiveCheck.getSelection() == false){
			type = SearchType.LowerCase;
		}
		
		storage = Storage.getInstance();
		
		if(!currentDocCheck.getSelection()){
			generateFilters();
		}
		
		
		//Async search
		InvocationCallback<FulltextSearchResult> callback = new InvocationCallback<FulltextSearchResult>() {

			@Override
			public void completed(FulltextSearchResult response) {
				fullTextSearchResult = response;
				if(fullTextSearchResult != null){	
					Display.getDefault().asyncExec(()->{
						updateFacets();
						updateResultsTable();
						updateReplaceComp();
					}); 
					
				}
				
			}

			@Override
			public void failed(Throwable throwable) {
				logger.error("Fulltext search failed."+ throwable);
				Display.getDefault().asyncExec(() -> {
					TrpMainWidget.getInstance().onError("Error searching fulltext", throwable.getMessage(), throwable);
				});
			}
			
		};
		
		try {	
//			if(!filters.isEmpty()){
//				fullTextSearchResult = storage.searchFulltext(searchText, type, start, rows, filters);
//			}else{
//				fullTextSearchResult = storage.searchFulltext(searchText, type, start, rows, null);
//			}	
			
			/*
			 * use new index -> results of old index cannot be shown like 'new' results
			 */
			final boolean useLegacyIndex = false;
			
			/*
			 * this has not worked since the 'useNewIndexCheck' button was never disposed
			 */
			//final boolean useLegacyIndex = !SWTUtil.getSelection(useNewIndexCheck);
			
			//logger.debug("-_>_____>  !!!!!!! useLegacyIndex = "+useLegacyIndex);
			logger.debug("show replace options " + (searchText.split(" ").length<=1));
			updateReplaceComp();
			
			//for search/replace the page hits should be fetched at once and not paged -> limit is 1000 results for the replace job, can be increased if useful
			if (replaceInDoc || replaceInColl) {
				if (searchText.isBlank() || replaceText.getText().isBlank()) {
					DialogUtil.showErrorBalloonToolTip(shell, "Replacement error", "No search or replacement word is set!");
					return;
				}
				if (replaceInColl) {
					if (DialogUtil.showYesNoCancelDialog(shell, "Create Search/Replace job", "Do you want to search for ALL! " + searchText + " occurences on server in CURRENT collection and replace all found hits with '" + replaceText.getText() +
							"'?" + System.lineSeparator() + "Please use the replacement method carefully!") == SWT.YES) {
	
						storage.getConnection().getSearchCalls().searchReplaceFulltext(storage.getCollId(), searchText, type, 0, numPageHits, filters, useLegacyIndex, replaceText.getText());
					}
				}else if (replaceInDoc) {
					setCurrentDoc();
					FulltextSearchResult myfullTextSearchResult = storage.getConnection().getSearchCalls().searchFulltext(searchText, type, 0, numPageHits, filters);
					List<Hit> myHits = convertPageHitsIntoHits(myfullTextSearchResult);
					
					SearchReplaceParams params = new SearchReplaceParams();
					List<PageHit> pHits = convertHitsIntoPageHits(myHits);
					params.setPageHits(pHits);
					params.setReplaceTerm(replaceText.getText());
					params.setAnnotation(false);
					
					if (pHits.size() == 0) {
						DialogUtil.showErrorBalloonToolTip(shell, "Replacement error", "No hit(s) found in current doc!");
						return;
					}
					
					if (DialogUtil.showYesNoCancelDialog(shell, "Create Replace job", "Do you want to replace all " + pHits.size() + " occurences of " + searchText + " in CURRENT document on server with '" + replaceText.getText() +
							"'?" + System.lineSeparator()) == SWT.YES) {

						storage.getConnection().getSearchCalls().replaceStrings(params);

					}
				}
			}
			else {
				storage.getConnection().getSearchCalls().searchFulltextAsync(searchText, type, start, rows, filters, useLegacyIndex, callback);
			}

		} catch (SessionExpiredException e) {
			logger.error("Error when trying to search: Session expired!", e);
		} catch (ServerErrorException e) {
			logger.error("Error when trying to search: ServerError!", e);
		} catch (ClientErrorException e) {
			logger.error("Error when trying to search: ClientError!", e);
		} catch (UnsupportedEncodingException e) {
			logger.error(e.getMessage(), e);
		}
//		catch (NoConnectionException e) {
//			logger.error("Error when trying to search: No connection!"+e);
//			e.printStackTrace();
//		}		
		
		
		lastSearchText = searchText;
		
	}

	//Clean up some day
	void generateFilters() {
		filters = new ArrayList<String>();
		String subFilters = "";
		
		for(int i : collCombo.getSelections()){
			
			if(i>=collCombo.textItems.length) {
				return;
			}

			//Remove number of hits and only use facet name
			//String filterString = collCombo.textItems[i].replaceAll("\\(.*\\)", "").trim();
			//Get collection id from string
			String filterString = collCombo.textItems[i].split(":", 2)[0];
			if(!filterString.equals("All collections") && subFilters.isEmpty()){
				subFilters+="(colId:\""+filterString+"\""; 
			}else if(!filterString.equals("All collections") && !subFilters.isEmpty()){
				subFilters+=" OR colId:\""+filterString+"\""; 
			}
		}
		
		if(!subFilters.isEmpty()){
			subFilters+=")";
			filters.add(subFilters);
			subFilters="";
		}

		
		for(int i : authCombo.getSelections()){
			
			if(i>=authCombo.textItems.length) {
				return;
			}

			String filterString = authCombo.textItems[i].replaceAll("\\(.*\\)", "").trim();
			if(!filterString.equals("All authors") && subFilters.isEmpty()){
				subFilters+="(f_author:\""+filterString+"\""; 
			}else if(!filterString.equals("All collections") && !subFilters.isEmpty()){
				subFilters+=" OR f_author:\""+filterString+"\""; 
			}				
		}
		
		if(!subFilters.isEmpty()){
			subFilters+=")";
			filters.add(subFilters);
			subFilters="";
		}
		
		for(int i : uplCombo.getSelections()){
			
			if(i>=uplCombo.textItems.length) {
				return;
			}

			String filterString = uplCombo.textItems[i].replaceAll("\\(.*\\)", "").trim();
			if(!filterString.equals("All uploaders") && subFilters.isEmpty()){
				subFilters+="(f_uploader:\""+filterString+"\""; 
			}else if(!filterString.equals("All uploaders") && !subFilters.isEmpty()){			
				subFilters+=" OR f_uploader:\""+filterString+"\"";
			}
		}
		
		if(!subFilters.isEmpty()){
			subFilters+=")";
			filters.add(subFilters);
			subFilters="";
		}
		
		for(int i : docCombo.getSelections()){
			
			if(i>=docCombo.textItems.length) {
				return;
			}

			String filterString = docCombo.textItems[i].replaceAll("\\(.*\\)", "").trim();
			if(!filterString.equals("All documents") && subFilters.isEmpty()){
				subFilters+="(f_title:\""+filterString+"\""; 
			}else if(!filterString.equals("All documents") && !subFilters.isEmpty()){			
				subFilters+=" OR f_title:\""+filterString+"\"";
			}				
		}

		if(!subFilters.isEmpty()){
			subFilters+=")";
			filters.add(subFilters);
			subFilters="";
		}

		
		logger.debug("filters: "+filters);
		
	}
	
	/*
	 * we create PateHits from the checked search hits to handle on the server
	 */
	private void createReplaceJob() {
		ArrayList<Hit> checkedHits = (ArrayList<Hit>) viewer.getInput();
		List<PageHit> searchHits = new ArrayList<PageHit>();
		/*
		 * get all page hits that are checked in the list.
		 */
		for (Hit hit : checkedHits) {
			List<String> pageHitWordCoords = new ArrayList<String>();

			List<Integer> colId = hit.getCollectionIds();
			int docId = hit.getDocId();
			int pageNr = hit.getPageNr();
			String foundWord = hit.getFoundWord();
			String regionId = hit.getRegionId();
			String lineId = hit.getLineId();
			String wordId = hit.getWordId();
			String coords = hit.getPixelCoords();
			
			PageHit pHit = new PageHit();
			pHit.setCollectionIds(colId);
			pHit.setDocId(docId);
			pHit.setPageNr(pageNr);
			
			String wordCoords = regionId+"/"+lineId+"/"+wordId+":"+coords;
			pageHitWordCoords.add(wordCoords);
			
			//wordCoords looks like: r_000/r_000l1/r_000l1_w1:630,574 630,634 842,634 842,574
			pHit.setWordCoords(pageHitWordCoords);
			
			searchHits.add(pHit);
		}
		
		try {
			SearchReplaceParams params = new SearchReplaceParams();
			params.setPageHits(searchHits);
			params.setReplaceTerm("replacement");
			TrpJobStatus job = storage.getConnection().getSearchCalls().replaceStrings(params);
		} catch (SessionExpiredException | ServerErrorException | ClientErrorException e) {
			logger.error("Failed to start search-replace job.", e);
		}
	}
	
	private void updateReplaceComp() {
		boolean roleCheck = (storage.isAdminLoggedIn() || (storage.getRoleOfUserInCurrentCollection().canManage() && currentDocCheck.getSelection()));
		boolean showReplace = searchText != null && searchText.split(" ").length<=1 && (numPageHits!=null && numPageHits>0) && roleCheck;
		
		/*
		 * initially the annotations was planned to do for several words as well but the search delivers word by word in the wCoords - therefore not easily possible
		 */
		boolean showAnnotate = searchText != null && (numPageHits!=null && numPageHits>0) && roleCheck;
		
//		logger.debug("canManage " + storage.getRoleOfUserInCurrentCollection().canManage());
//		logger.debug("show replace " + showReplace);
//		logger.debug("show showAnnotate " + showAnnotate);
		
		if (replaceComp1 != null) {
			
			replaceComp1.setVisible(showReplace);
		}
		
		if (annotateComp != null) {

			annotateComp.setVisible(showReplace);
		}
		
		if (replaceComp2 != null) {
			
			replaceComp2.setVisible(showReplace);
		}

	}
	
	private void updateResultsTable() {
	     
		numPageHits = (int) fullTextSearchResult.getNumResults();
		
        hits = new ArrayList<Hit>();
        
        logger.debug("1nr of page hits: "+fullTextSearchResult.getNumResults()+" / "+fullTextSearchResult.getPageHits().size()+" nHits: "+hits.size());
        
        hits = convertPageHitsIntoHits(fullTextSearchResult);
        
        logger.debug("2nr of page hits: "+fullTextSearchResult.getNumResults()+" / "+fullTextSearchResult.getPageHits().size()+" nHits: "+hits.size());

        viewer.setInput(hits);
        int showNrRows = (rows>=hits.size()) ? hits.size() : rows;
        
        int maxHits = hits.size() > numPageHits ? hits.size() : numPageHits;
        int max = (start+rows) > maxHits ? maxHits : (start+rows);
        
//        resultsLabel.setText("Result pages found: " + numPageHits + " -> Showing search hits "+(start+1)+" to "+(max) + " (cleaned view - so there may be less displayed)");
        
        int startIdx = numPageHits==0 ? 0 : start+1;
        resultsLabel.setText("Showing lines "+(startIdx)+" to "+(max)+" (total hits: "+numPageHits+")");
        viewer.getTable().setItemCount(showNrRows);
        
        checkTableItems();
        
        viewer.getTable().redraw(); // MacOS fix
        
        Runnable loadPrevImg = new Runnable(){
        	
        	public boolean stopFlag = false;
        	
        	public void run() {
        		try{
	        		for(Hit hit : hits){
	        			
	        			if(Thread.currentThread().isInterrupted()) stopFlag = true;
	        			if (stopFlag) return;
	        			String coords = hit.getPixelCoords();
	        			
	        			//Extract key from URL
	        			String imgKey = StringUtils.substringBetween(hit.getImgUrl(), "Get?id=", "&fileType=view");	

						try {						
							Image img;							
							int[] cropValues = getCropValues(coords);
							
							if(prevImages.keySet().contains(coords)){
								//Do Nothing
							}else{
								URL url = imgStoreClient.getUriBuilder().getImgCroppedUri(imgKey, cropValues[0], cropValues[1], cropValues[2], cropValues[3]).toURL();
								img = ImageDescriptor.createFromURL(url).createImage();		
								prevImages.put(coords, img);
//								logger.debug("Background | Loaded img coords: "+coords);
							}
							
						} catch (Exception ex) {								
							logger.error("Could not load preview image!", ex);
						}	
		        			
        			}
	        		logger.debug("Background word thumbnail loading complete");
        		}catch(Exception iEx){
        			logger.debug("Exception", iEx);
        		}   	
        	}
        };
        
        if(imgLoaderThread != null){
        	imgLoaderThread.interrupt();
        	logger.debug("Thread interrupted");
        }
    	imgLoaderThread = new Thread(loadPrevImg,"WordThmbLoaderThread");
    	imgLoaderThread.start();
        imgLoaderThread.setPriority(Thread.MIN_PRIORITY);        
        logger.debug("Image loading thread started. Nr of imgages: "+hits.size());
		
	}

	private ArrayList<Hit> convertPageHitsIntoHits(FulltextSearchResult myfullTextSearchResult) {
		
		ArrayList<Hit> myHits = new ArrayList<Hit>();
		for (PageHit pHit : myfullTextSearchResult.getPageHits()){

        	Map<String,Integer> foundWords = new HashMap<String,Integer>();	
        	
        	logger.debug("-----> pHit " + pHit.toString());
        	
			/*
			 * with new index the hlString contains all highlights of a page -> old index several hl Strings for one page
			 * problem now is: you order the next n pageHits for the paged view but you get much more hits then page hits
			 * and you can't show all the hits because you do not know the amount in the pHits before you read them
			 */
        	for(String hlString : pHit.getHighlights()){
        		try {

	        		//logger.debug("hlString " + hlString);
	        		ArrayList<String> tags = getTagValues(hlString);
	        		
	        		for(String tag : tags){
	        			//logger.debug("tag: " + tag);
	        			boolean contained = false;
	        			for(String word : foundWords.keySet()){
	        				if(word.equals(tag)){
	        					contained = true;
	        					foundWords.replace(word, foundWords.get(word)+1);
	        				}
	        			}
	        			if(contained == false){
	        				foundWords.put(tag, 0);
	        			}
	        		}
	        		logger.trace("foundWords = "+foundWords);
	        		
	        		List<String> matchedCoords = new ArrayList<>();
	        		logger.trace("n-word-coords = "+pHit.getWordCoords().size());
	        		for(String word : pHit.getWordCoords()){
	        			//logger.debug("word = '"+word+"'");
	        			if(word.split(":")[0].equals(tags.get(0))){
	        				matchedCoords.add(word);
	        			}

	        		}       
	        		logger.trace("n-matchedCoords="+matchedCoords.size());
	        		
	        		/*
	        		 * bei mehreren tags (=3 Wörter in der Suche, jedes Wort is dann ein tag wir der erste Worteintrag für die Liste verwendet
	        		 * für Replace sollten alle in der Liste aufscheinen1?
	        		 */
	        		String wCoords;
	        		//logger.debug("tags.get(0) " + tags.get(0));
	        		if(matchedCoords.size() > foundWords.get(tags.get(0))){
	        			wCoords = matchedCoords.get(foundWords.get(tags.get(0)));
	        		}else if(!matchedCoords.isEmpty()){
	        			wCoords = matchedCoords.get(0);
	        		}else if(!pHit.getWordCoords().isEmpty()){
	        			wCoords = pHit.getWordCoords().get(0);
	        		}else{
	        			break;
	        		}
	
	        		WordCoords wordCoords = getWordCoords(wCoords);
	        		
	        		if (wordCoords != null) {
		        		List<Integer> colIds = pHit.getCollectionIds();
	
		        		String pUrl = pHit.getPageUrl();
		        		Hit hit = new Hit(hlString, (int)pHit.getDocId(), pHit.getDocTitle(), (int)pHit.getPageNr(), wordCoords.regId, wordCoords.linId, wordCoords.worId, wordCoords.pxCoords, pUrl, pHit.getWordCoords());
		        		hit.setCollectionIds(colIds);
		        		hit.setFoundWord(wordCoords.wordString);
		        		myHits.add(hit);
	        		}

            	} catch (Exception e) {
            		logger.error("Error updating results table hit: "+e.getMessage(), e);
            	}        		
        	}
        }
		return myHits;
	}

	/*
	 * changed for showing results of new index -> hlString contains now all highlights of a page 
	 * and we want to show every occurence contained in the wCoords
	 */
	@Deprecated
	private void updateResultsTable_newTry() {
     
		numPageHits = (int) fullTextSearchResult.getNumResults();
		
        hits = new ArrayList<Hit>();
        
        logger.debug("1nr of page hits: "+fullTextSearchResult.getNumResults()+" / "+fullTextSearchResult.getPageHits().size()+" nHits: "+hits.size());
        for (PageHit pHit : fullTextSearchResult.getPageHits()){

        	Map<String,Integer> foundWords = new HashMap<String,Integer>();	
        	
        	//logger.debug("-----> pHit " + pHit.toString());
        	
			/*
			 * with new index the hlString contains all highlights of a page -> old index several hl Strings for one page
			 * so the method was changed to get the same representations
			 */
        	for(String hlString : pHit.getHighlights()){
        		try {

	        		logger.debug("hlString " + hlString);
	        		ArrayList<String> tags = getTagValues(hlString);
	        		
	        		for(String tag : tags){
	        			logger.debug("tag: " + tag);
	        			boolean contained = false;
	        			for(String word : foundWords.keySet()){
	        				if(word.equals(tag)){
	        					contained = true;
	        					foundWords.replace(word, foundWords.get(word)+1);
	        				}
	        			}
	        			if(contained == false){
	        				foundWords.put(tag, 0);
	        			}
	        		}
	        		logger.trace("foundWords = "+foundWords);
	        		
	        		List<String> matchedCoords = new ArrayList<>();
	        		logger.trace("n-word-coords = "+pHit.getWordCoords().size());
	        		
	        		String [] newHlStrings = hlString.split("</em>");
	        		int i = 0;
	        		
	        		for(String word : pHit.getWordCoords()){
	        			
	        			logger.debug("word = '"+word+"'");
	   
		        		String wordString = word.split(":")[0];
		        		String regId = word.split(":")[1].split("/")[0];
		        		String linId = word.split(":")[1].split("/")[1];
		        		String worId = word.split(":")[1].split("/")[2];
		        		String pxCoords = word.split(":")[2];
		        		
		        		List<Integer> colIds = pHit.getCollectionIds();
		        		
		        		logger.debug("wordString: " + wordString);
		        		logger.debug("page: " + pHit.getPageNr());
		        		logger.debug("regId: " + regId);
		        		logger.debug("linId: " + linId);
		        		logger.debug("worId: " + worId);
		        		logger.debug("pxCoords: " + pxCoords);
		        		
		        		logger.debug("new hl string " + newHlStrings[i]);
		        		
		        		String pUrl = pHit.getPageUrl();
		        		Hit hit = new Hit(newHlStrings[i].concat("</em>"), (int)pHit.getDocId(), pHit.getDocTitle(), (int)pHit.getPageNr(), regId, linId, worId, pxCoords, pUrl);
		        		hit.setCollectionIds(colIds);
		        		hit.setFoundWord(wordString);
		        		hits.add(hit);
		        		i++;

	        		}       
	        		logger.trace("n-matchedCoords="+matchedCoords.size());
	        		


            	} catch (Exception e) {
            		logger.error("Error updating results table hit: "+e.getMessage(), e);
            	}        		
        	}
        }
        
        logger.debug("2nr of page hits: "+fullTextSearchResult.getNumResults()+" / "+fullTextSearchResult.getPageHits().size()+" nHits: "+hits.size());

        
        viewer.setInput(hits);
        int showNrRows = (rows>=hits.size()) ? hits.size() : rows;
        
        int maxHits = hits.size() > numPageHits ? hits.size() : numPageHits;
        int max = (start+rows) > maxHits ? maxHits : (start+rows);
        
//        resultsLabel.setText("Result pages found: " + numPageHits + " -> Showing search hits "+(start+1)+" to "+(max) + " (cleaned view - so there may be less displayed)");
        resultsLabel.setText("Showing lines "+(start+1)+" to "+(max)+" (total hits: "+numPageHits+")");
        viewer.getTable().setItemCount(showNrRows);
        
        checkTableItems();
        
        viewer.getTable().redraw(); // MacOS fix
        
        Runnable loadPrevImg = new Runnable(){
        	
        	public boolean stopFlag = false;
        	
        	public void run() {
        		try{
	        		for(Hit hit : hits){
	        			
	        			if(Thread.currentThread().isInterrupted()) stopFlag = true;
	        			if (stopFlag) return;
	        			String coords = hit.getPixelCoords();
	        			
	        			//Extract key from URL
	        			String imgKey = StringUtils.substringBetween(hit.getImgUrl(), "Get?id=", "&fileType=view");	

						try {						
							Image img;							
							int[] cropValues = getCropValues(coords);
							
							if(prevImages.keySet().contains(coords)){
								//Do Nothing
							}else{
								URL url = imgStoreClient.getUriBuilder().getImgCroppedUri(imgKey, cropValues[0], cropValues[1], cropValues[2], cropValues[3]).toURL();
								img = ImageDescriptor.createFromURL(url).createImage();		
								prevImages.put(coords, img);
//								logger.debug("Background | Loaded img coords: "+coords);
							}
							
						} catch (Exception ex) {								
							logger.error("Could not load preview image!", ex);
						}	
		        			
        			}
	        		logger.debug("Background word thumbnail loading complete");
        		}catch(Exception iEx){
        			logger.debug("Exception", iEx);
        		}   	
        	}
        };
        
        if(imgLoaderThread != null){
        	imgLoaderThread.interrupt();
        	logger.debug("Thread interrupted");
        }
    	imgLoaderThread = new Thread(loadPrevImg,"WordThmbLoaderThread");
    	imgLoaderThread.start();
        imgLoaderThread.setPriority(Thread.MIN_PRIORITY);        
        logger.debug("Image loading thread started. Nr of imgages: "+hits.size());   
//        else{
//        	try{
//	        	imgLoaderThread.interrupt(); 
//	        	logger.debug("Thread interrupted");
//	        	imgLoaderThread = new Thread(loadPrevImg);
//        	}catch(Exception e){
//        		
//        	}
//        }
    
        
        //loadPrevImg.run();
		
	}
	
	
	
	private List<Hit> getSelectedHits() {
		if (!viewer.getSelection().isEmpty()) {
			return ((IStructuredSelection) viewer.getSelection()).toList();
		} else {
			return new ArrayList<>();
		}
	}
	
	private List<Hit> getCheckedHits() {
		List<Hit> checked = new ArrayList<>();
		for (TableItem ti : resultsTable.getItems()) {
			if (ti.getChecked()) {
				logger.debug(" data :" + ((Hit)ti.getData()));
				logger.debug("checked data " + ti.getText());
				logger.debug("checked data " + ti.toString());
				checked.add((Hit) ti.getData());
			}
		}
		
		return checked;
	}
	
	private void checkTableItems() {
		
		for (TableItem ti : resultsTable.getItems()) {
			Hit tableHit = (Hit)ti.getData();
			if (selectedHits.containsKey(createKey(tableHit))) {
				logger.debug("found checked item in map");
				ti.setChecked(true);
			}
			else {
				ti.setChecked(false);
			}
		}
	
	}
	
	private Map<String, Hit> getCheckedHitsAsMap() {
		Map<String, Hit> checked = new HashMap<String, Hit>();
		for (TableItem ti : resultsTable.getItems()) {
			Hit found = (Hit) ti.getData();
			String wCoords = createKey(found);
			logger.debug("data :" + ((Hit)ti.getData()));
			logger.debug("checked data " + ti.getText());
			logger.debug("checked data " + wCoords);
			if (ti.getChecked()) {

				checked.put(wCoords, found);
			}
			else {

				selectedHits.remove(wCoords);
				
			}
		}
		
		return checked;
	}
	
	private String createKey(Hit found) {
		
		String pageNr = Integer.toString(found.getPageNr());
		String docId = Integer.toString(found.getDocId());
		String word = found.getFoundWord();
		String regId = found.getRegionId();
		String lineId = found.getLineId();
		String worId = found.getWordId();
		String pxCoords = found.getPixelCoords();

		String wordCoords = docId + "," + pageNr + "," + word + ":" + regId + "/" + lineId + "/" + worId + ":" + pxCoords;
		
		//logger.debug("key created: " + wordCoords);
		return wordCoords;
	}

	/**
	 * @deprecated TODO
	 */
	private void replaceSelectedHits() {
		if (true)
			throw new NotImplementedException();
		
		logger.debug("replaceSelectedHits");
		try {
			ProgressBarDialog.open(getShell(), new IRunnableWithProgress() {
				@Override public void run(IProgressMonitor monitor) throws InvocationTargetException, InterruptedException {
					try {
						final List<Hit> selectedHits = getSelectedHits();
						logger.debug("selectedHits: "+selectedHits.size());
						
	//					monitor.setTaskName("Saving");
						logger.info("Replacing text in hits: "+selectedHits.size());
						monitor.beginTask("Replacing text in hits", selectedHits.size());
						int c=0;
						Storage s = Storage.getInstance();
						
						// a pages cache: key is pageid, value is a pair of collection-id and corresponding TrpPageType
						Map<Integer, Pair<Integer, TrpPageType>> affectedPages = new HashMap<>();
						for (Hit t : selectedHits) {
							if (monitor.isCanceled())
								return;
							
							// compile list of affected pages and tags to be changed
							logger.debug("Updating text for hit: " + t);
							
							
							// --- COPIED FROM TagNormalizationWidget ---
//							// set attributes in tag result object
//							t.setCustomTagCss(propertyTable.getSelectedTag().getCssStr());
//							logger.trace("New value: " + selectedTags.get(c));
//							
//							// load page on which the tag is on (if not loaded yet)
//							Pair<Integer, TrpPageType> ptPair = affectedPages.get(t.get);
//							if (ptPair == null) {
//								TrpPage page = s.getConnection().getTrpDoc(t.getCollId(), t.getDocid(), 1).getPages().get(t.getPagenr()-1);
//								TrpPageType pt = s.getOrBuildPage(page.getCurrentTranscript(), true);
//								ptPair = Pair.of(t.getCollId(), pt);
//								affectedPages.put(t.getPageid(), ptPair);
//							}
//							
//							// convert DbTag to CustomTag
//							CssSyntaxTag cssTag =  CssSyntaxTag.parseSingleCssTag(t.getCustomTagCss());
//							CustomTag ct = CustomTagFactory.create(cssTag.getTagName(), t.getOffset(), t.getLength(), cssTag.getAttributes());
//
//							// retrieve parent line / shape
//							TrpTextLineType lt = ptPair.getRight().getLineWithId(t.getRegionid());
//							
//							// add or merge tag on line
//							lt.getCustomTagList().addOrMergeTag(ct, null, true);
							// --- COPIED FROM TagNormalizationWidget ---
							
							monitor.worked(c++);
						}
						
						logger.debug("nr of affected pages: "+affectedPages.size());
						s.saveTranscriptsMap(affectedPages, monitor);
					} catch (Exception e) {
						throw new InvocationTargetException(e);
					}
				}
			}, "Normalizing tag attributes", true);
		} catch (Throwable e) {
			TrpMainWidget.getInstance().onError("Error replacing text", e.getMessage(), e, true, false);
		}
		
	}
	
	  private static class Hit
	  {
		  
		String foundWord;
		String highlightText;
	    String imgUrl;

		String regionId, lineId, wordId, title;
	    private String pixelCoords;
	    private List<Integer> collectionIds;
	    private List<String> allWordCoords;

		int docId, pageNr;
		      
	    Hit(String hl, int doc, String docTitle, int page, String region, String line, String word, String coords, String url){
	    	highlightText = hl;
	    	docId = doc;
	    	title = docTitle;
	    	pageNr = page;
	    	regionId = region;
	    	lineId = line;
	    	wordId = word;	  
	    	pixelCoords = coords;
	    	imgUrl = url;
	    }
	    
	    Hit(String hl, int doc, String docTitle, int page, String region, String line, String word, String coords, String url, List<String> allWordCoordsOfPageHit){
	    	highlightText = hl;
	    	docId = doc;
	    	title = docTitle;
	    	pageNr = page;
	    	regionId = region;
	    	lineId = line;
	    	wordId = word;	  
	    	pixelCoords = coords;
	    	imgUrl = url;
	    	this.allWordCoords = allWordCoordsOfPageHit;
	    }
	    
	    Hit(String hl, int doc, int page){
	    	highlightText = hl;
	    	docId = doc;
	    	pageNr = page;	    	
	    }
	    
	    public String getFoundWord() {
			return foundWord;
		}

		public void setFoundWord(String foundWord) {
			this.foundWord = foundWord;
		}
		
	    public String getImgUrl() {
			return imgUrl;
		}

		public void setImgUrl(String imgUrl) {
			this.imgUrl = imgUrl;
		}
		      
	     public String getHighlightText() {
			return highlightText;
		}

		public void setHighlightText(String highlightText) {
			this.highlightText = highlightText;
		}
		
		public String getDocTitle(){
			return title;
		}
		
		public void setDocTitle(String title){
			this.title = title;
		}

		public int getDocId() {
			return docId;
		}

		public void setDocId(int docId) {
			this.docId = docId;
		}

		public int getPageNr() {
			return pageNr;
		}

		public void setPageNr(int pageNr) {
			this.pageNr = pageNr;
		}

		public String getLineId() {
			return lineId;
		}

		public void setLineId(String lineId) {
			this.lineId = lineId;
		}

		public String getWordId() {
			return wordId;
		}

		public void setWordId(String wordId) {
			this.wordId = wordId;
		}	
		
		public String getRegionId() {
			return regionId;
		}

		public void setRegionId(String regionId) {
			this.regionId = regionId;
		}

		public String getPixelCoords() {
			return pixelCoords;
		}

		public void setPixelCoords(String pixelCoords) {
			this.pixelCoords = pixelCoords;
		}

		public List<Integer> getCollectionIds() {
			return collectionIds;
		}

		public void setCollectionIds(List<Integer> collectionIds) {
			this.collectionIds = collectionIds;
		}

		@Override
		public String toString() {
			return "Hit [highlightText=" + highlightText + ", imgUrl=" + imgUrl + ", regionId=" + regionId + ", lineId="
					+ lineId + ", wordId=" + wordId + ", title=" + title + ", pixelCoords=" + pixelCoords
					+ ", collectionIds=" + collectionIds + ", docId=" + docId + ", pageNr=" + pageNr + "]";
		}

		public List<String> getAllWordCoords() {
			return allWordCoords;
		}

		public void setAllWordCoords(List<String> allWordCoords) {
			this.allWordCoords = allWordCoords;
		}
	  }
	  
	  private static final Pattern TAG_REGEX = Pattern.compile("<em>(.+?)</em>");
	  public static ArrayList<String> getTagValues(final String str) {
		  ArrayList<String> tagValues = new ArrayList<String>();
		  Matcher matcher = TAG_REGEX.matcher(str);
		  while (matcher.find()) {
			  tagValues.add(matcher.group(1));
			  }
		  return tagValues;
	  	}
	  
	  private class HoverShell{		  
		  Shell hoverShell;
		  Label imgLabel;
		  Image wordPrev;
		  public HoverShell(Shell shell){
		  hoverShell = new Shell(shell, SWT.ON_TOP | SWT.TOOL);
		  hoverShell.setLayout(new FillLayout());
		  imgLabel = new Label(hoverShell, SWT.NONE);	
		  imgLabel.setImage(wordPrev);
		  }

		 }

	public void checkMultiCombos() {
		
		MultiSelectionCombo[] combos = {authCombo, docCombo, uplCombo, collCombo};
		
		boolean comboOpen = false;
		for (MultiSelectionCombo multiCombo : combos){
			if(multiCombo.shell != null){
				if(!multiCombo.shell.isDisposed()){
					comboOpen = true;
				}
			}
			
		}		
		if(comboOpen){
			noMultiCombos = false;
		}else{
			noMultiCombos = true;
		}

		
	} 
	
	public void setCurrentDoc(){
		TrpDoc currentDoc = storage.getDoc();	
		String currentTitle = currentDoc.getMd().getTitle();
		collCombo.txtCurrentSelection.setEnabled(false);
		docCombo.txtCurrentSelection.setEnabled(false);
		authCombo.txtCurrentSelection.setEnabled(false);
		uplCombo.txtCurrentSelection.setEnabled(false);
		
		filters = new ArrayList<String>();
		//filters.add("(f_title:\""+currentTitle+"\")");
		filters.add("(docId:"+currentDoc.getId()+")");
		
		collCombo.txtCurrentSelection.setText("");
		docCombo.txtCurrentSelection.setText("");
		authCombo.txtCurrentSelection.setText("");
		uplCombo.txtCurrentSelection.setText("");
	}
	
	public void setSearchText(String text){
		inputText.setText(text);
	}	

	
	public void searchCurrentDoc(boolean bool){
		currentDocCheck.setSelection(bool);	
		setCurrentDoc();
		
	}
	
	class WordCoords {
		@Override
		public String toString() {
			return wordString + ":" + regId + "/" + linId + "/" + worId + ":" + pxCoords;
		}

		String wordString;
  		String regId; 
  		String linId;
  		String worId;
  		String pxCoords;
  		
		public WordCoords(String wordString, String regId, String linId, String worId, String pxCoords) {
			super();
			this.wordString = wordString;
			this.regId = regId;
			this.linId = linId;
			this.worId = worId;
			this.pxCoords = pxCoords;
		}

	}

}