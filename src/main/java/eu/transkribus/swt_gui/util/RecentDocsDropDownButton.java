package eu.transkribus.swt_gui.util;

import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.MenuItem;

import eu.transkribus.swt.util.Images;
import eu.transkribus.swt.util.SWTUtil;
import eu.transkribus.swt_gui.mainwidget.TrpMainWidget;
import eu.transkribus.util.RecentDocsPreferences;
import eu.transkribus.util.RecentDocsPreferences.IRecentDocsPrefListener;
import eu.transkribus.util.RecentDocsPreferences.RecentDoc;

public class RecentDocsDropDownButton extends Composite {
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(RecentDocsDropDownButton.class);
	
	DropDownButton btn;

	public RecentDocsDropDownButton(Composite parent) {
		super(parent, 0);
		this.setLayout(SWTUtil.createGridLayout(1, false, 0, 0));
		
		btn = new DropDownButton(this, 0, "Recent documents...", Images.FOLDER_STAR, null);
		btn.setShowDropDownOnLeftSide(true);
		btn.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		
		RecentDocsPreferences.addListener(new IRecentDocsPrefListener() {
			@Override
			public void valuesChanged(List<String> values) {
				updateUi();
			}
		});
	}
	
	private void updateUi() {
		logger.debug("RecentDocsDropDownButton -> updateUi()");
		btn.clearItems();
		
		List<RecentDoc> recentDocs = RecentDocsPreferences.getRecentDocs(true);
		logger.debug("n-recent-docs = "+recentDocs.size());
		for (RecentDoc rd : recentDocs) {
			MenuItem item = btn.addItem(rd.getLabel(), getBackgroundImage(), getStyle());
			SWTUtil.onSelectionEvent(item, e -> TrpMainWidget.i().loadRecentDoc(rd.recentDocStr));
		}

	}

}
