package eu.transkribus.swt_gui.auth;

import eu.transkribus.client.oidc.ITokenStorage;
import eu.transkribus.core.model.beans.auth.TrpUser;
import eu.transkribus.swt_gui.TrpGuiPrefs;

import java.util.List;
import java.util.Objects;

public class TrpGuiPrefsTokenStorage implements ITokenStorage {
    @Override
    public List<String> getUsernames(String authServerUrl) {
        return TrpGuiPrefs.getSsoUsernames(authServerUrl);
    }

    @Override
    public String getStoredRefreshToken(String authServerUrl, String username) {
        return TrpGuiPrefs.getRefreshToken(authServerUrl, username);
    }

    @Override
    public void updateStoredRefreshToken(String authServerUrl, TrpUser user, String refreshToken) {
        Objects.requireNonNull(user);
        Objects.requireNonNull(authServerUrl);
        if(refreshToken == null) {
            removeStoredRefreshToken(authServerUrl, user.getUserName());
        } else {
            TrpGuiPrefs.storeSsoToken(authServerUrl, user, refreshToken);
        }
    }

    @Override
    public void removeStoredRefreshToken(String authServerUrl, String username) {
        TrpGuiPrefs.clearSsoToken(authServerUrl, username);
    }
}
