//package eu.transkribus.swt_gui.auth.keycloak;
//
//import eu.transkribus.client.oidc.TrpSsoHandler;
//import org.keycloak.adapters.installed.KeycloakInstalled;
//
//import java.io.IOException;
//import java.net.URI;
//
//public class KeycloakDesktopProviderAdapter extends KeycloakInstalled.DesktopProvider {
//    private TrpSsoHandler.IDesktopProvider delegate;
//    public KeycloakDesktopProviderAdapter(TrpSsoHandler.IDesktopProvider delegate) {
//        this.delegate = delegate;
//    }
//
//    @Override
//    public void browse(URI uri) throws IOException {
//        delegate.browse(uri);
//    }
//}
