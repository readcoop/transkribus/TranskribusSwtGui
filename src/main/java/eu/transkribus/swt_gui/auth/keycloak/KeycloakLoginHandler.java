//package eu.transkribus.swt_gui.auth.keycloak;
//
//import eu.transkribus.swt_gui.auth.OAuthWaitForCallbackRunnableWithProgress;
//import org.keycloak.OAuthErrorException;
//import org.keycloak.adapters.ServerRequest;
//import org.keycloak.adapters.installed.KeycloakInstalled;
//import org.keycloak.common.VerificationException;
//
//import java.io.IOException;
//import java.net.URISyntaxException;
//
//public class KeycloakLoginHandler implements OAuthWaitForCallbackRunnableWithProgress.ILoginHandler {
//    final KeycloakInstalled keycloak;
//    public KeycloakLoginHandler(KeycloakInstalled keycloak) {
//        this.keycloak = keycloak;
//    }
//
//    @Override
//    public void login() throws IOException, InterruptedException {
//        try {
//            this.keycloak.login();
//        } catch (IOException | OAuthErrorException | URISyntaxException | ServerRequest.HttpFailure |
//                 VerificationException e) {
//            throw new IOException("User login failed.", e);
//        }
//    }
//
//    @Override
//    public boolean isLoggedIn() {
//        return keycloak.getToken() != null;
//    }
//}