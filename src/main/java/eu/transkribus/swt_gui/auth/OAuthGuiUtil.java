package eu.transkribus.swt_gui.auth;

import java.io.IOException;

import javax.ws.rs.ClientErrorException;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Deprecated
public class OAuthGuiUtil {
	private static final Logger logger = LoggerFactory.getLogger(OAuthGuiUtil.class);

	public static void revokeOAuthToken(final String refreshToken) throws IOException{
		final String uriStr = "https://accounts.google.com/o/oauth2/revoke?token=";
		CloseableHttpClient client = HttpClients.custom().setSSLHostnameVerifier(new NoopHostnameVerifier()).build();		
		HttpGet get = new HttpGet(uriStr + refreshToken);
		HttpResponse response = client.execute(get);
		final int status = response.getStatusLine().getStatusCode();		
		if (status != 200) {
			String reason = response.getStatusLine().getReasonPhrase();
			logger.error(reason);
			throw new ClientErrorException(reason, status);
		}
	}
}
