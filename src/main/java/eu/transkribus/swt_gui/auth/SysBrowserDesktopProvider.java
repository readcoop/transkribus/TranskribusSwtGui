package eu.transkribus.swt_gui.auth;

import eu.transkribus.client.oidc.TrpSsoHandler;
import eu.transkribus.core.util.SysUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URI;

public class SysBrowserDesktopProvider implements TrpSsoHandler.IDesktopProvider {
    private static final Logger logger = LoggerFactory.getLogger(SysBrowserDesktopProvider.class);

    public void browse(URI uri) throws IOException {
        if (SysUtils.isLinux()) {
            logger.debug("On linux.");
            try {
                Runtime.getRuntime().exec("xdg-open " + uri.toASCIIString());
            } catch (IOException e) {
                logger.error("Could not open browser.", e);
            }
        } else {
            TrpSsoHandler.IDesktopProvider.super.browse(uri);
        }
    }
}
