package eu.transkribus.swt_gui.auth;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

public class OAuthWaitForCallbackRunnableWithProgress implements IRunnableWithProgress {
    private static final Logger logger = LoggerFactory.getLogger(OAuthWaitForCallbackRunnableWithProgress.class);
    private final ILoginHandler loginHandler;

    public OAuthWaitForCallbackRunnableWithProgress(ILoginHandler loginHandler) {
        this.loginHandler = loginHandler;
    }

    @Override
    public void run(IProgressMonitor monitor) throws InvocationTargetException, InterruptedException {
        monitor.beginTask("Logging in...", IProgressMonitor.UNKNOWN);
        Runnable r = () -> {
            try {
                loginHandler.login();
            } catch (IOException e) {
                throw new RuntimeException("User login failed.", e);
            } catch (InterruptedException e) {
                logger.debug("User canceled login flow.");
            }
        };
        Thread t = new Thread(r);
        t.start();

        while(!loginHandler.isLoggedIn()){
            if(monitor.isCanceled()) {
                t.interrupt();
                throw new InterruptedException("User canceled login flow.");
            }
            if(!t.isAlive()) {
                //user did not give consent
                return;
            }
            Thread.sleep(500);
        }
        monitor.done();
    }

    public interface ILoginHandler {
        void login() throws IOException, InterruptedException;
        boolean isLoggedIn();
    }
}
