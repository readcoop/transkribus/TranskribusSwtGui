package eu.transkribus.swt_gui.auth;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @deprecated
 * This will fail with illegal thread access when the loginThread tries to close the dialog on success callback.
 */
public class OAuthWaitForCallbackDialog extends Dialog {
	private static final Logger logger = LoggerFactory.getLogger(OAuthWaitForCallbackDialog.class);
	protected Shell shell;
	private Thread loginThread;
	/**
	 * Create the dialog.
	 * @param parent
	 */
	public OAuthWaitForCallbackDialog(Shell parent, Runnable loginRunnable) {
		super(parent, SWT.NONE);
		this.loginThread = new Thread(loginRunnable);
		setText("Connecting Account...");
	}

	/**
	 * Open the dialog.
	 * @return the result
	 */
	public Object open() {
		shell = new Shell(getParent(), getStyle());
		shell.setSize(400, 200);
		shell.setText(getText());
		shell.setLayout(new GridLayout(1, false));
		
		shell.setLocation(getParent().getSize().x/2, getParent().getSize().y/3);
		
		GridData gd  = new GridData(300, 20);

		Composite buttonComposite = new Composite(shell, SWT.NONE);
		buttonComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		buttonComposite.setLayout(new FillLayout());

		Label info = new Label(buttonComposite, SWT.NONE);
		info.setText("Please follow the instructions in your browser to login.");
		GridDataFactory.fillDefaults().align(SWT.FILL, SWT.FILL).grab(true, true).span(1, 1).applyTo(info);

		Button closeButton = new Button(buttonComposite, SWT.PUSH);
		closeButton.setText("Cancel");
		closeButton.addSelectionListener(new SelectionAdapter() {
			@Override public void widgetSelected(SelectionEvent e) {
				cancel();
			}	
		});
		closeButton.setToolTipText("Cancel the login procedure");
		
		shell.pack();
		shell.open();
		shell.layout();
		Display display = getParent().getDisplay();

		//blocking operation
		try {
			browserLogin();
		} catch (InterruptedException e) {
			logger.debug("User canceled login flow.");
			return IDialogConstants.CANCEL_ID;
		}

		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		return IDialogConstants.OK_ID;
	}

	protected void browserLogin() throws InterruptedException {
		loginThread.start();
		loginThread.join();
	}

	protected void cancel() {
		loginThread.interrupt();
	}
}
