package eu.transkribus.swt_gui.dialogs;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.mihalis.opal.checkBoxGroup.CheckBoxGroup;

import eu.transkribus.client.util.SessionExpiredException;
import eu.transkribus.client.util.TrpClientErrorException;
import eu.transkribus.client.util.TrpServerErrorException;
import eu.transkribus.core.model.beans.LA2InferenceConfig;
import eu.transkribus.core.model.beans.TrpModelMetadata;
import eu.transkribus.core.model.beans.job.enums.JobImpl;
import eu.transkribus.core.model.beans.rest.ParameterMap;
import eu.transkribus.core.rest.JobConst;
import eu.transkribus.core.util.ModelUtil;
import eu.transkribus.swt.util.DialogUtil;
import eu.transkribus.swt.util.Images;
import eu.transkribus.swt.util.LabeledText;
import eu.transkribus.swt.util.SWTUtil;
import eu.transkribus.swt_gui.TrpGuiPrefs;
import eu.transkribus.swt_gui.mainwidget.storage.Storage;
import eu.transkribus.swt_gui.models.ModelChooserButton;
import eu.transkribus.swt_gui.util.CurrentTranscriptOrDocPagesOrCollectionSelector;
import eu.transkribus.swt_gui.util.CurrentTranscriptOrDocPagesOrCollectionSelector.DocSelectorData;

public class FieldsDialog extends Dialog {
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(FieldsDialog.class);
	
	public static final Double DEFAULT_THRESHOLD_FIELDS = 0.75;
	public static final Double DEFAULT_THRESHOLD_TABLES = 0.97;
	public static final Double DEFAULT_APPROX_DOUBLE_FIELDS = 0.7;

	CurrentTranscriptOrDocPagesOrCollectionSelector dps;
	// Combo modelCombo;
	Button typeLABtn;
	Button typeTableBtn;

	Composite cont, fieldsOptionsCont, tableOptionsCont;
	Button keepExistingRegionsBtn;
	CheckBoxGroup combineWithBaseLayoutGroup;
	// Button combineWithBaseLayoutBtn;
	LabeledText thresholdText;
	LabeledText approxPolyFracText;
	Button sortRegionsColumnwiseBtn;
	Button keepEmptyRegionsBtn;
	Button splitLinesBtn;
	LabeledText lineOverlapFractionText;
	Button clusterLinesWithoutRegionsBtn;
	CheckBoxGroup mergeOverlappingRegionsGroup;
	LabeledText mergeThresholdText;
	CheckBoxGroup splitTablesGroup;
	Button horizontalTableSplitBtn, verticalTableSplitBtn;
	LabeledText splitTableThresholdText;
	Button deskewBtn;

	ModelChooserButton modelChooserBtn;
	
	int docId;
	Integer modelId;
	// String modelName;
	DocSelectorData docSelectorData;
	boolean sortRegionsColumnwise=false;
	boolean keepExistingRegions=false;
	boolean combineWithBaseLayout=false;
	Double threshold=DEFAULT_THRESHOLD_FIELDS;
	Double approxPolyFrac=DEFAULT_APPROX_DOUBLE_FIELDS;
	boolean keepEmptyRegions=LA2InferenceConfig.DEFAULT_KEEP_EMPTY_REGIONS;
	boolean splitLines=LA2InferenceConfig.DEFAULT_SPLIT_LINES;
	double lineOverlapFraction=LA2InferenceConfig.DEFAULT_LINE_OVERLAP_FRACTION;
	boolean clusterLinesWithoutRegions=LA2InferenceConfig.DEFAULT_CLUSTER_LINES_WITHOUT_REGIONS;
	
	boolean mergeOverlappingRegions=false;
	Double mergeThreshold=0.6;

	boolean horizontalTableSplit=false;
	boolean verticalTableSplit=false;
	Integer splitTableThreshold=100;
	boolean deskew=false;

	boolean isTableDetection=false;
	ParameterMap params = new ParameterMap();

	private final static JobImpl jobImpl = JobImpl.LayoutAnalysis2Job;

	public FieldsDialog(Shell parentShell) {
		super(parentShell);
		params = TrpGuiPrefs.getLaParameters(jobImpl);
	}
	
    protected void configureShell(Shell shell) {
        super.configureShell(shell);
        shell.setText("Fields Recognition");
    }
	
	@Override
	protected Point getInitialSize() {
		Point s = SWTUtil.getPreferredOrMinSize(getShell(), 300, 0);
		return new Point(Math.max(300, s.x+30), s.y+15);
	}
	
	@Override
	protected void setShellStyle(int newShellStyle) {           
	    super.setShellStyle(SWT.CLOSE | SWT.MODELESS| SWT.BORDER | SWT.TITLE | SWT.MIN | SWT.RESIZE);
	    setBlockOnOpen(false);
	}
	
	@Override
	protected boolean isResizable() {
	    return true;
	}
	
	@Override
	protected Control createDialogArea(Composite parent) {
		cont = (Composite) super.createDialogArea(parent);
		cont.setLayout(new GridLayout(2, false));
		
		dps = new CurrentTranscriptOrDocPagesOrCollectionSelector(cont, SWT.NONE, false, true, true);
		dps.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1));
		
		typeLABtn = new Button(cont, SWT.RADIO);
		typeLABtn.setText("Fields");
		SWTUtil.onSelectionEvent(typeLABtn, e -> updateUi());
		typeLABtn.setSelection(true);
		
		typeTableBtn = new Button(cont, SWT.RADIO);
		typeTableBtn.setText("Table-Detection");
		SWTUtil.onSelectionEvent(typeTableBtn, e -> updateUi());
		
		Label modelLbl = new Label(cont, 0);
		modelLbl.setText("Model: ");
		// modelCombo = new Combo(cont, SWT.DROP_DOWN /*| SWT.READ_ONLY*/);
		// modelCombo.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

		modelChooserBtn = new ModelChooserButton(cont, true, ModelUtil.TYPE_LAYOUT, ModelUtil.PROVIDER_LA2, null) {
			@Override
			protected void onModelSelectionChanged(TrpModelMetadata selectedModel) {
				// if (selectedModel != null) {
				// 	modelCombo.setText(selectedModel.getName());
				// }
				// else {
				// 	modelCombo.select(0);
				// }

				if (selectedModel != null) {
					params.addIntParam(JobConst.PROP_MODEL_ID, selectedModel.getModelId());
					params.addParameter(JobConst.PROP_MODELNAME, selectedModel.getBaseModelName());
					params.addParameter("provider", selectedModel.getProvider());
				}
			}
		};
		modelChooserBtn.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		modelChooserBtn.setImage(Images.MODEL_ICON);
		
		fieldsOptionsCont = new Composite(cont, SWT.NONE);
		fieldsOptionsCont.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1));
		fieldsOptionsCont.setLayout(new GridLayout(2, false));

		thresholdText = new LabeledText(cont, "Threshold: ");
		thresholdText.setText(""+DEFAULT_THRESHOLD_FIELDS);
		thresholdText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1));
		thresholdText.setToolTipText("Confidence threshold for region detection (0.0 - 1.0)");

		approxPolyFracText = new LabeledText(fieldsOptionsCont, "Approx. Poly Frac: ");
		approxPolyFracText.setText("0.7");
		approxPolyFracText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		approxPolyFracText.setToolTipText("How much the approximation of a polygon may deviate from the original polygon (0.0 - 1.0)");
		
		sortRegionsColumnwiseBtn = new Button(fieldsOptionsCont, SWT.CHECK);
		sortRegionsColumnwiseBtn.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1));
		sortRegionsColumnwiseBtn.setText("Sort regions columnwise");
		sortRegionsColumnwiseBtn.setToolTipText("Sort regions columnwise after the recogniton and set the reading order accordingly - check if the default sorting of the platform is working for you.");

		keepExistingRegionsBtn = new Button(fieldsOptionsCont, SWT.CHECK);
		keepExistingRegionsBtn.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1));
		keepExistingRegionsBtn.setText("Keep existing regions");
		keepExistingRegionsBtn.setToolTipText("Append detected regions to current PAGE-XML");

		combineWithBaseLayoutGroup = new CheckBoxGroup(fieldsOptionsCont, SWT.NONE);
		combineWithBaseLayoutGroup.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1));
		combineWithBaseLayoutGroup.setLayout(new GridLayout(2, false));
		combineWithBaseLayoutGroup.setText("Combine with base layout");
		combineWithBaseLayoutGroup.setToolTipText("Combine detected regions with base layout");

		keepEmptyRegionsBtn = new Button(combineWithBaseLayoutGroup.getContent(), SWT.CHECK);
		keepEmptyRegionsBtn.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1));
		keepEmptyRegionsBtn.setText("Keep empty regions");
		keepEmptyRegionsBtn.setToolTipText("Keep empty regions in the result");

		splitLinesBtn = new Button(combineWithBaseLayoutGroup.getContent(), SWT.CHECK);
		splitLinesBtn.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		splitLinesBtn.setText("Split lines");
		splitLinesBtn.setToolTipText("Split lines according to region borders");

		lineOverlapFractionText = new LabeledText(combineWithBaseLayoutGroup.getContent(), "Line Overlap Fraction: ");
		lineOverlapFractionText.setText("0.05");
		lineOverlapFractionText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		lineOverlapFractionText.setToolTipText("The minimum overlap fraction between a line and a region to split the line.");

		clusterLinesWithoutRegionsBtn = new Button(combineWithBaseLayoutGroup.getContent(), SWT.CHECK);
		clusterLinesWithoutRegionsBtn.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1));
		clusterLinesWithoutRegionsBtn.setText("Cluster lines without regions");
		clusterLinesWithoutRegionsBtn.setToolTipText("Cluster lines or parts of lines that are not overlapping with any region after the combination with the base layout.");

		mergeOverlappingRegionsGroup = new CheckBoxGroup(fieldsOptionsCont, SWT.NONE);
		mergeOverlappingRegionsGroup.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1));
		mergeOverlappingRegionsGroup.setLayout(new GridLayout(2, false));
		mergeOverlappingRegionsGroup.setText("Merge overlapping regions");
		mergeOverlappingRegionsGroup.setToolTipText("Merge overlapping after the inference and the (optional) combination with the base layout.");
		
		mergeThresholdText = new LabeledText(mergeOverlappingRegionsGroup.getContent(), "Merge threshold");
		mergeThresholdText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1));
		mergeThresholdText.setToolTipText("The minimum overlap fraction between two regions to merge them.");
		mergeThresholdText.setText(""+mergeThreshold);

		combineWithBaseLayoutGroup.deactivate();
		mergeOverlappingRegionsGroup.deactivate();

		tableOptionsCont = new Composite(cont, SWT.NONE);
		tableOptionsCont.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1));
		tableOptionsCont.setLayout(new GridLayout(2, false));
		
		splitTablesGroup = new CheckBoxGroup(tableOptionsCont, SWT.NONE);
		splitTablesGroup.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1));
		splitTablesGroup.setLayout(new GridLayout(2, false));
		splitTablesGroup.setText("Split tables");
		splitTablesGroup.setToolTipText("Split tables if the distance between rows/columns is beyond a certain threshold.");

		horizontalTableSplitBtn = new Button(splitTablesGroup.getContent(), SWT.RADIO);
		horizontalTableSplitBtn.setSelection(false);
		horizontalTableSplitBtn.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		horizontalTableSplitBtn.setText("Horizontal");
		horizontalTableSplitBtn.setToolTipText("Split tables horizontally if the distance between successive rows is greater than a certain amount.");

		verticalTableSplitBtn = new Button(splitTablesGroup.getContent(), SWT.RADIO);
		verticalTableSplitBtn.setSelection(true);
		verticalTableSplitBtn.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		verticalTableSplitBtn.setText("Vertical");
		verticalTableSplitBtn.setToolTipText("Split tables vertically if the distance between successive columns is greater than a certain amount.");

		splitTableThresholdText = new LabeledText(splitTablesGroup.getContent(), "Distance threshold: ");
		splitTableThresholdText.setText(""+splitTableThreshold);
		splitTableThresholdText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		splitTableThresholdText.setToolTipText("The distance in pixels between successive rows/columns that triggers the split.");

		deskewBtn = new Button(tableOptionsCont, SWT.CHECK);
		deskewBtn.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1));
		deskewBtn.setText("Deskew");
		deskewBtn.setToolTipText("Deskew the image before inference. This is only required in case the predictions are limited by skewness.");		

		splitTablesGroup.deactivate();
		
		updateUi();
		
		return cont;
	}

	private void updateModelChooseBtnFromParams() throws TrpServerErrorException, TrpClientErrorException, SessionExpiredException {
		if (params != null && params.containsKey(JobConst.PROP_MODEL_ID)) {
			int modelId = params.getIntParam(JobConst.PROP_MODEL_ID);
			// String provider = params.getParameterValue("provider");
			// if (modelChooserBtn.getModelId()!=null && modelChooserBtn.getModelId()==modelId && modelChooserBtn.getModel().getProvider().equals(provider)) {
			// 	return;
			// }

			TrpModelMetadata model = new TrpModelMetadata();
			model.setType("layout");
			model.setModelId(modelId);
			model = Storage.getInstance().getConnection().getModelCalls().getModel(model);
			System.out.println("Got Fields model from previously stored parameters: " + model);
			if (model!=null && typeLABtn.getSelection() && !model.getProvider().equals(ModelUtil.PROVIDER_LA2) || 
				model!=null && typeTableBtn.getSelection() && !model.getProvider().equals(ModelUtil.PROVIDER_TABLE)) {
				modelChooserBtn.setModel(null);
			}
			else {
				modelChooserBtn.setModel(model);
			}
		}
		else {
			modelChooserBtn.setModel(null);
		}
	}
	
	private void updateUi() {
		// if (typeLABtn.getSelection()) {
		// 	modelCombo.setItems("Berlin101", "Darmstadt", "Koeln", "Stabz_101", "gradsko_101");
		// }
		// else {
		// 	modelCombo.setItems("Amsterdam_Demo");
		// }
		// modelCombo.select(0);

		try {
			updateModelChooseBtnFromParams();
		} catch (Exception e) {
			logger.error("Could not update model chooser button from parameters", e);
			modelChooserBtn.setModel(null);
		}
		// modelChooserBtn.setModel(null);
		modelChooserBtn.setProviderFilter(typeLABtn.getSelection() ? ModelUtil.PROVIDER_LA2 : ModelUtil.PROVIDER_TABLE);

		if (typeLABtn.getSelection()) {
			tableOptionsCont.setParent(SWTUtil.dummyShell);
			fieldsOptionsCont.setParent(cont);
			fieldsOptionsCont.moveBelow(thresholdText);

			thresholdText.setText(""+DEFAULT_THRESHOLD_FIELDS);
		}
		else {
			fieldsOptionsCont.setParent(SWTUtil.dummyShell);
			tableOptionsCont.setParent(cont);
			tableOptionsCont.moveBelow(thresholdText);

			thresholdText.setText(""+DEFAULT_THRESHOLD_TABLES);
		}
		cont.layout();
	}
	
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		createButton(parent, IDialogConstants.CANCEL_ID, "Cancel", false);
		
	    Button runBtn = createButton(parent, IDialogConstants.OK_ID, "Run...", false);
	    runBtn.setImage(Images.ARROW_RIGHT);
	}
	
	// public String getSelectedModel() {
	// 	return modelCombo.getText();
	// }
	
	// public int getSelectedModelIndex() {
	// 	return modelCombo.getSelectionIndex();
	// }
	
	@Override
	protected void okPressed() {
		this.modelId = modelChooserBtn.getModelId();
		if (this.modelId == null) {
			DialogUtil.showErrorMessageBox(getParentShell(), "Error", "No model selected");
			return;
		}
		
		this.docSelectorData = dps.getData();
		if (this.docSelectorData == null) {
			return;
		}
		this.threshold = thresholdText.toDoubleVal();
		if (this.threshold == null) {
			DialogUtil.showErrorMessageBox(getParentShell(), "Error", "Invalid threshold value");
			return;
		}

		this.approxPolyFrac = approxPolyFracText.toDoubleVal();
		if (this.approxPolyFrac == null) {
			DialogUtil.showErrorMessageBox(getParentShell(), "Error", "Invalid approxPolyFrac value");
			return;
		}

		this.sortRegionsColumnwise = sortRegionsColumnwiseBtn.getSelection();
		this.keepExistingRegions=keepExistingRegionsBtn.getSelection();
		this.isTableDetection = typeTableBtn.getSelection();
		this.combineWithBaseLayout = combineWithBaseLayoutGroup.isActivated();
		this.keepEmptyRegions = keepEmptyRegionsBtn.getSelection();
		this.splitLines = splitLinesBtn.getSelection();
		this.lineOverlapFraction = lineOverlapFractionText.toDoubleVal();
		this.clusterLinesWithoutRegions = clusterLinesWithoutRegionsBtn.getSelection();

		this.mergeOverlappingRegions = mergeOverlappingRegionsGroup.isActivated();
		this.mergeThreshold = this.mergeThresholdText.toDoubleVal();
		if (this.mergeThreshold == null) {
			DialogUtil.showErrorMessageBox(getParentShell(), "Error", "Invalid merge threshold value");
			return;
		}		

		this.horizontalTableSplit = this.splitTablesGroup.isActivated() && horizontalTableSplitBtn.getSelection();
		this.verticalTableSplit = this.splitTablesGroup.isActivated() && verticalTableSplitBtn.getSelection();
		this.splitTableThreshold = splitTableThresholdText.toIntVal();
		if (this.splitTableThreshold == null) {
			DialogUtil.showErrorMessageBox(getParentShell(), "Error", "Invalid split table threshold value");
			return;
		}
		this.deskew = deskewBtn.getSelection();

		TrpGuiPrefs.storeLaParameters(jobImpl, params);
		super.okPressed();
	}

	public CurrentTranscriptOrDocPagesOrCollectionSelector getDps() {
		return dps;
	}

	public DocSelectorData getDocSelectorData() {
		return docSelectorData;
	}

	// public String getModelName() {
	// 	return modelName;
	// }	

	public Integer getModelId() {
		return modelId;
	}
	
	public boolean isKeepExistingRegions() {
		return keepExistingRegions;
	}

	public boolean isTableDetection() {
		return isTableDetection;
	}

	public boolean isCombineWithBaseLayout() {
		return combineWithBaseLayout;
	}

	public Double getThreshold() {
		return threshold;
	}

	public Double getApproxPolyFrac() {
		return approxPolyFrac;
	}

	public boolean isSortRegionsColumnwise() {
		return sortRegionsColumnwise;
	}

	public boolean isKeepEmptyRegions() {
		return keepEmptyRegions;
	}

	public boolean isSplitLines() {
		return splitLines;
	}

	public double getLineOverlapFraction() {
		return lineOverlapFraction;
	}

	public boolean isClusterLinesWithoutRegions() {
		return clusterLinesWithoutRegions;
	}

	public boolean isMergeOverlappingRegions() {
		return mergeOverlappingRegions;
	}

	public Double getMergeThreshold() {
		return mergeThreshold;
	}

	public boolean isHorizontalTableSplit() {
		return horizontalTableSplit;
	}

	public boolean isVerticalTableSplit() {
		return verticalTableSplit;
	}

	public Integer getSplitTableThreshold() {
		return splitTableThreshold;
	}

	public boolean isDeskew() {
		return deskew;
	}

}
