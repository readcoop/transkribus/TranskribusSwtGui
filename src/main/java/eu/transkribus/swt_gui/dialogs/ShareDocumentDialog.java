package eu.transkribus.swt_gui.dialogs;

import eu.transkribus.client.util.SessionExpiredException;
import eu.transkribus.client.util.TrpClientErrorException;
import eu.transkribus.client.util.TrpServerErrorException;
import eu.transkribus.core.model.beans.TrpCollection;
import eu.transkribus.core.model.beans.TrpDocMetadata;
import eu.transkribus.core.model.beans.rest.TrpCollectionList;
import eu.transkribus.swt.util.DialogUtil;
import eu.transkribus.swt.util.Images;
import eu.transkribus.swt.util.SWTUtil;
import eu.transkribus.swt_gui.collection_comboviewer.CollectionSelectorDialog;
import eu.transkribus.swt_gui.mainwidget.storage.Storage;
import eu.transkribus.swt_gui.metadata.CollectionsTableWidget;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.function.Predicate;

public class ShareDocumentDialog extends Dialog {
    private static final Logger logger = LoggerFactory.getLogger(ShareDocumentDialog.class);

    private TrpCollection currentCollection;
    private final TrpDocMetadata docMd;
    private boolean reloadDocListOnClose;

    private CollectionsTableWidget collectionsTableWidget;

    Button addBtn, moveBtn, removeBtn, reloadBtn;

    protected Storage store;

    public ShareDocumentDialog(Shell parentShell, TrpCollection currentCollection, TrpDocMetadata docMd) {
        super(parentShell);
        this.currentCollection = currentCollection;
        this.docMd = docMd;
        this.store = Storage.i();
        this.reloadDocListOnClose = false;
    }

    @Override
    protected void configureShell(Shell shell) {
        super.configureShell(shell);
        shell.setText("Share document '" + getDocMd().getTitle() + "'");
        SWTUtil.centerShell(shell, false);
    }

    @Override
    protected boolean isResizable() {
        return true;
    }

    @Override
    protected Point getInitialSize() {
        //FIXME getPreferredSize returns bad values on wide screens, e.g. multi-monitor setups
//		Point s = SWTUtil.getPreferredSize(getShell());
//		return new Point(s.x + 200, s.y + 50);
        return new Point(400, 400);
    }

    @Override
    protected void createButtonsForButtonBar(Composite parent) {
        createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL, true);
    }

    @Override
    protected Control createDialogArea(Composite parent) {
        Composite cont = (Composite) super.createDialogArea(parent);
        cont.setLayout(new GridLayout(4, false));

        collectionsTableWidget = createCollectionsTableWidget(cont);
        collectionsTableWidget.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 4, 1));

        addBtn = new Button(cont, 0);
        addBtn.setText("Link...");
        addBtn.setToolTipText("Link this document to a collection. Users in that collection will be able to edit the document according to their role there!");
        addBtn.setImage(Images.ADD);
        addBtn.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
        SWTUtil.onSelectionEvent(addBtn, e -> addOrMoveSelectedDocumentToCollection(false));

        moveBtn = new Button(cont, 0);
        moveBtn.setText("Move...");
        moveBtn.setToolTipText("Move the document to a collection. This will remove the document from it's current main collection.");
        moveBtn.setImage(Images.ARROW_RIGHT);
        moveBtn.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
        SWTUtil.onSelectionEvent(moveBtn, e -> addOrMoveSelectedDocumentToCollection(true));

        removeBtn = new Button(cont, 0);
        removeBtn.setText("Unlink");
        removeBtn.setToolTipText("Remove the document from the selected collection");
        removeBtn.setImage(Images.DELETE);
        removeBtn.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
        SWTUtil.onSelectionEvent(removeBtn, e -> removeSelectedDocumentFromCollection());

        reloadBtn = new Button(cont, 0);
        reloadBtn.setToolTipText("Reload collections for this document");
        reloadBtn.setImage(Images.REFRESH);
        reloadBtn.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_END));
        SWTUtil.onSelectionEvent(reloadBtn, e -> updateGui());

        updateGui();

        return cont;
    }

    protected CollectionsTableWidget createCollectionsTableWidget(Composite parent) {
        CollectionsTableWidget widget = new CollectionsTableWidget(parent, SWT.BORDER) {
            @Override
            protected void createColumns() {
                createColumn("", 25, null, new MainDocumentCollectionColumnLabelProvider());
                super.createColumns();
            }
        };
        widget.enableToolTipSupport();
        return widget;
    }

    private void addOrMoveSelectedDocumentToCollection(boolean move) {
        TrpCollection c = acquireUserSelection();
        logger.debug("{} document to collection: {}", move ? "Moving" : "Linking", c);
        if (c == null) {
            return;
        }
        try {
            addDocToCollection(c, move);
            updateGui();
        } catch (TrpClientErrorException | TrpServerErrorException e) {
            logger.error("Could not add document to collection: " + e.getMessage(), e);
            String errorMsg = e.getMessageToUser();
            DialogUtil.showErrorMessageBox(getShell(), "Error sharing document", errorMsg);
        } catch (Exception e) {
            logger.error("Could not add document to collection: " + e.getMessage(), e);
            String errorMsg = e.getMessage();
            DialogUtil.showErrorMessageBox(getShell(), "Error sharing document", errorMsg);
        }
    }

    protected TrpCollection acquireUserSelection() {
        Predicate<TrpCollection> collSelectorPredicate = null;
//		Predicate<TrpCollection> collSelectorPredicate = (c) -> { return c!=null && AuthUtils.canManage(c.getRole()); }; // show only collections users can manage
        CollectionSelectorDialog d = new CollectionSelectorDialog(getShell(), collSelectorPredicate, null);
        if (d.open() != Dialog.OK) {
            return null;
        }
        return d.getSelectedCollection();
    }

    private void removeSelectedDocumentFromCollection() {
        TrpCollection selection = collectionsTableWidget.getSelection();
        if (selection == null) {
            logger.debug("No selection. Doing nothing...");
            return;
        } else {
            logger.debug("Removing document from collection: {}", selection.getColName());
        }

        try {
            removeDocFromCollection(selection);
            updateGui();
        } catch (TrpClientErrorException | TrpServerErrorException e) {
            logger.error("Could not remove document from collection: " + e.getMessage(), e);
            String errorMsg = e.getMessageToUser();
            DialogUtil.showErrorMessageBox(getShell(), "Error removing document from collection", errorMsg);
        } catch (Exception e) {
            logger.error("Could not remove document from collection: " + e.getMessage(), e);
            DialogUtil.showErrorMessageBox(getShell(), "Error removing document from collection", e.getMessage());
        }
    }

    private void updateGui() {
        try {
            List<TrpCollection> colls = getCollectionList();
            logger.debug("loaded n-colls = " + colls.size());
            collectionsTableWidget.refreshList(colls);
        } catch (TrpClientErrorException | TrpServerErrorException e) {
            logger.error("Error updating collections for document: " + e.getMessage());
            String errorMsg = e.getMessageToUser();
            DialogUtil.showErrorBalloonToolTip(collectionsTableWidget, "Error updating collections for document", errorMsg);
        } catch (Exception e) {
            logger.error("Error updating collections for document: " + e.getMessage(), e);
            DialogUtil.showErrorBalloonToolTip(collectionsTableWidget, "Error updating collections for document",
                    e.getMessage());
        }
    }

    private void storeResults() {
        // nothing to do here I guess...
    }

    @Override
    protected void okPressed() {
        storeResults();

        super.okPressed();
    }

    public void setVisible() {
        if (super.getShell() != null && !super.getShell().isDisposed()) {
            super.getShell().setVisible(true);
        }
    }

    @Override
    protected void setShellStyle(int newShellStyle) {
        super.setShellStyle(SWT.CLOSE | SWT.MAX | SWT.RESIZE | SWT.TITLE);
    }

    protected TrpDocMetadata getDocMd() {
        return new TrpDocMetadata(docMd);
    }

    protected void addDocToCollection(TrpCollection c, boolean move)
            throws TrpClientErrorException, TrpServerErrorException, SessionExpiredException {
        store.getConnection().addDocToCollection(c.getColId(), docMd.getDocId(), move);
        reloadDocListOnClose = true;
    }

    protected void removeDocFromCollection(TrpCollection c)
            throws TrpClientErrorException, TrpServerErrorException, SessionExpiredException {
        store.getConnection().removeDocFromCollection(c.getColId(), docMd.getDocId());
        reloadDocListOnClose = true;
    }

    protected List<TrpCollection> getCollectionList()
            throws TrpServerErrorException, TrpClientErrorException, SessionExpiredException {
        TrpCollectionList colList = store.getConnection().getCollectionsByDocId(currentCollection.getColId(), docMd.getDocId(),
                0, -1, "collectionId", "asc");
        logger.debug("Retrieved {}/{} collections for docId {}", colList.getTotal(), colList.getTotal(), docMd.getDocId());
        colList.getList().stream().forEach(c -> logger.debug(c.toShortString()));
        return colList.getList();
    }

    public boolean isReloadDocListOnClose() {
        return reloadDocListOnClose;
    }

    static class MainDocumentCollectionColumnLabelProvider extends ColumnLabelProvider {
        public Image getImage(Object element) {
            TrpCollection c = toCollection(element);
            if(c == null) {
                return null;
            }
            if(c.getIsMainDocumentCollection() == null) {
                return Images.CROSS;
            }
            return c.getIsMainDocumentCollection() ? Images.ANCHOR : Images.LINK;
        }

        @Override
        public String getToolTipText(Object element) {
            TrpCollection c = toCollection(element);
            if(c == null) {
                return "Invalid input object";
            }
            if(c.getIsMainDocumentCollection() == null) {
                return "No link info available";
            }
            return c.getIsMainDocumentCollection() ? "Main Collection" : "Collection Link";
        }

        @Override
        public String getText(Object element) {
            return "";
        }

        private TrpCollection toCollection(Object element) {
            if(element == null || !(element instanceof TrpCollection)) {
                return null;
            }
            return (TrpCollection) element;
        }
    }
}
