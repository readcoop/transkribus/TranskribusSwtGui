package eu.transkribus.swt_gui.dialogs;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.function.ToIntFunction;

import org.apache.commons.lang3.StringUtils;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import eu.transkribus.core.model.beans.pagecontent_trp.TrpPageType;
import eu.transkribus.core.util.StructTypesAnal;
import eu.transkribus.swt.util.DialogUtil;
import eu.transkribus.swt.util.Fonts;
import eu.transkribus.swt.util.Images;
import eu.transkribus.swt.util.LabeledText;
import eu.transkribus.swt.util.SWTUtil;
import eu.transkribus.swt_gui.mainwidget.TrpMainWidget;
import eu.transkribus.swt_gui.util.CurrentTranscriptOrCurrentDocPagesSelector;

public class StructTypeReplaceDialog extends Dialog {
	CurrentTranscriptOrCurrentDocPagesSelector pagesSelector;
	LabeledText toReplace, replacement;
	Button addPairBtn, clearBtn;
	Button dryRunBtn, runBtn;
	Button ignoreCaseBtn;
	Map<String, String> replacementMap = new LinkedHashMap<>();
	Text replacementText;

	public StructTypeReplaceDialog(Shell parentShell) {
		super(parentShell);
	}
	
	@Override
	protected Point getInitialSize() {
		return new Point(600, getShell().computeSize(SWT.DEFAULT, SWT.DEFAULT).y+100);
	}
	
	@Override
	protected void setShellStyle(int newShellStyle) {           
	    super.setShellStyle(SWT.CLOSE | SWT.MODELESS| SWT.BORDER | SWT.TITLE | SWT.RESIZE);
	    setBlockOnOpen(false);
	}
	
	@Override
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText("Replace structure tags");
	}
	
	@Override
	protected boolean isResizable() {
	    return true;
	}	
	
	@Override
	protected Control createButtonBar(Composite parent) {
		return super.createButtonBar(parent);
	}
	
	@Override
	protected Control createContents(Composite parent) {
		Composite cont = (Composite) super.createDialogArea(parent);
		cont.setLayoutData(new GridData(GridData.FILL_BOTH));
		cont.setLayout(new GridLayout(2, false));
		
		pagesSelector = new CurrentTranscriptOrCurrentDocPagesSelector(cont, SWT.NONE, true,true);
		pagesSelector.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1));
		pagesSelector.getCurrentTanscriptRadio().setText("Current collection");
		
		toReplace = new LabeledText(cont, "Struct-tag to replace: ");
		toReplace.setToolTipText("Enter name of struct-tag to replace - leave empty for shapes with no structure tag");
		toReplace.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		
		replacement = new LabeledText(cont, "Struct-tag to replace: ");
		replacement.setToolTipText("Enter name of struct-tag replacement - leave empty to unassign struct tags");
		replacement.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		
		addPairBtn = new Button(cont, 0);
		addPairBtn.setText("Add");
		addPairBtn.setImage(Images.ADD);
		addPairBtn.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		
		SWTUtil.onSelectionEvent(addPairBtn, e -> {
			if (StringUtils.equals(toReplace.getText(), replacement.getText())) {
				DialogUtil.showErrorMessageBox(getShell(), "Error adding replacement", "Tag to replace and replacement cannot be the same");
				return;
			}
			replacementMap.put(toReplace.getText(), replacement.getText());
			updateUi();
		});		
		
		clearBtn = new Button(cont, 0);
		clearBtn.setText("Clear all");
		clearBtn.setImage(Images.DELETE);
		clearBtn.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		
		SWTUtil.onSelectionEvent(clearBtn, e -> {
			replacementMap.clear();
			updateUi();
		});
		
		Label lbl = new Label(cont, 0);
		lbl.setText("Replacements: ");
		lbl.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1));
		Fonts.setBoldFont(lbl);
		
		replacementText = new Text(cont, SWT.MULTI | SWT.READ_ONLY | SWT.V_SCROLL);
		replacementText.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 2, 1));
		
		ignoreCaseBtn = new Button(cont, SWT.CHECK);
		ignoreCaseBtn.setText("Ignore case");
		ignoreCaseBtn.setToolTipText("Ignore case when replacing tags");
		ignoreCaseBtn.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1));
		
		dryRunBtn = new Button(cont, 0);
		dryRunBtn.setText("Dry run...");
		dryRunBtn.setToolTipText("Perform replacement without storing transcripts");
		dryRunBtn.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		SWTUtil.onSelectionEvent(dryRunBtn, e -> runReplacement(false));
		
		runBtn = new Button(cont, 0);
		runBtn.setText("Run");
		runBtn.setToolTipText("Perform replacement and store transcripts");
		runBtn.setImage(Images.ARROW_RIGHT);
		runBtn.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));	
		SWTUtil.onSelectionEvent(runBtn, e -> runReplacement(true));
		
		return cont;
	}
	
	public Map<String, String> getReplacementMap() {
		return replacementMap;
	}

	private String getTagName(String tagName) {
		return StringUtils.isEmpty(tagName) ? "<unassigned>" : tagName;
	}
	
	private void updateUi() {
		String txt = "";
		for (String replaceTagName : replacementMap.keySet()) {
			txt += getTagName(replaceTagName) + " -> " + getTagName(replacementMap.get(replaceTagName))+"\n";
		}
		replacementText.setText(txt);
	}
	
	private void runReplacement(boolean doStoreTranscripts) {
		List<String> toReplaceList = new ArrayList<>();
		List<String> replacementsList = new ArrayList<>();
		boolean ignoreCase = ignoreCaseBtn.getSelection(); 
		for (String key : replacementMap.keySet()) {
			toReplaceList.add(key);
			replacementsList.add(replacementMap.get(key));
		}
		
		if (toReplaceList.isEmpty()) {
			DialogUtil.showErrorMessageBox(getShell(), "No replacements defined", "No structure tag replacements defined");
			return;
		}
		
		ToIntFunction<TrpPageType> f = t -> StructTypesAnal.replaceStructType(t.getPcGtsType(), toReplaceList.toArray(new String[0]), replacementsList.toArray(new String[0]), ignoreCase);
		Integer nReplaced = TrpMainWidget.i().getShapeEditController().performTaskForPagesOrCurrentCollection("Replace structure tags"+(doStoreTranscripts?"":" (dry-run)"), 
				pagesSelector.isCurrentTranscript(), pagesSelector.getPagesStr(), f, doStoreTranscripts);
		
		if (nReplaced != null) {
			String msg = "Replaced "+nReplaced+" structure tags";
			msg += !doStoreTranscripts ? " (dry-run)" : " (stored new transcripts)";
			DialogUtil.showInfoMessageBox(getShell(), "Completed", msg);
		}
	}

}
