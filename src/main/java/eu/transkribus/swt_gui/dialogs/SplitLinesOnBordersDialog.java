package eu.transkribus.swt_gui.dialogs;

import java.util.function.ToIntFunction;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;

import eu.transkribus.core.model.beans.pagecontent_trp.TrpPageType;
import eu.transkribus.swt.util.DialogUtil;
import eu.transkribus.swt.util.Images;
import eu.transkribus.swt.util.SWTUtil;
import eu.transkribus.swt_gui.mainwidget.ShapeEditController;
import eu.transkribus.swt_gui.mainwidget.TrpMainWidget;
import eu.transkribus.swt_gui.util.CurrentTranscriptOrCurrentDocPagesSelector;

public class SplitLinesOnBordersDialog extends Dialog {
	CurrentTranscriptOrCurrentDocPagesSelector pagesSelector;
	Button dryRunBtn, runBtn;

	public SplitLinesOnBordersDialog(Shell parentShell) {
		super(parentShell);
	}
	
	@Override
	protected Point getInitialSize() {
		return new Point(600, getShell().computeSize(SWT.DEFAULT, SWT.DEFAULT).y+100);
	}
	
	@Override
	protected void setShellStyle(int newShellStyle) {           
	    super.setShellStyle(SWT.CLOSE | SWT.MODELESS| SWT.BORDER | SWT.TITLE | SWT.RESIZE);
	    setBlockOnOpen(false);
	}
	
	@Override
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText("Split baselines on borders");
	}
	
	@Override
	protected boolean isResizable() {
	    return true;
	}	
	
	@Override
	protected Control createButtonBar(Composite parent) {
		return super.createButtonBar(parent);
	}
	
	@Override
	protected Control createContents(Composite parent) {
		Composite cont = (Composite) super.createDialogArea(parent);
		cont.setLayoutData(new GridData(GridData.FILL_BOTH));
		cont.setLayout(new GridLayout(2, false));
		
		pagesSelector = new CurrentTranscriptOrCurrentDocPagesSelector(cont, SWT.NONE, true,true);
		pagesSelector.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1));
		pagesSelector.getCurrentTanscriptRadio().setText("Current collection");
		pagesSelector.getPagesRadio().setSelection(true);
		pagesSelector.getCurrentTanscriptRadio().setSelection(false);
		pagesSelector.updateGui();

//		dryRunBtn = new Button(cont, 0);
//		dryRunBtn.setText("Dry run...");
//		dryRunBtn.setToolTipText("Perform replacement without storing transcripts");
//		dryRunBtn.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
//		SWTUtil.onSelectionEvent(dryRunBtn, e -> runReplacement(false));
		
		runBtn = new Button(cont, 0);
		runBtn.setText("Run");
		runBtn.setToolTipText("Perform splitting and store transcripts");
		runBtn.setImage(Images.ARROW_RIGHT);
		runBtn.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));	
		SWTUtil.onSelectionEvent(runBtn, e -> runReplacement(true));
		
		return cont;
	}
	

	private void runReplacement(boolean doStoreTranscripts) {
				
		ToIntFunction<TrpPageType> f = t -> ShapeEditController.splitLinesOnRegionBorders(t.getPcGtsType());
		Integer nReplaced = TrpMainWidget.i().getShapeEditController().performTaskForPagesOrCurrentCollection("Split lines on borders", 
				pagesSelector.isCurrentTranscript(), pagesSelector.getPagesStr(), f, doStoreTranscripts);
		
		if (nReplaced != null) {
			String msg = "Splitted "+nReplaced+" baselines";
			DialogUtil.showInfoMessageBox(getShell(), "Completed", msg);
		}
	}

}
