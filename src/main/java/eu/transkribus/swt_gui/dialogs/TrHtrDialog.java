package eu.transkribus.swt_gui.dialogs;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

import eu.transkribus.core.util.ModelUtil;
import eu.transkribus.swt.util.Images;
import eu.transkribus.swt.util.SWTUtil;
import eu.transkribus.swt_gui.mainwidget.storage.Storage;
import eu.transkribus.swt_gui.util.CurrentTranscriptOrDocPagesOrCollectionSelector;
import eu.transkribus.swt_gui.util.CurrentTranscriptOrDocPagesOrCollectionSelector.DocSelectorData;
import eu.transkribus.util.TextRecognitionConfig;

/**
 * @deprecated shall not be used anymore -> TrHtr is started as any other HTR in HtrTextRecognitionConfigDialog
 *
 */
public class TrHtrDialog extends Dialog {
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(TrHtrDialog.class);
	
	CurrentTranscriptOrDocPagesOrCollectionSelector dps;
	Combo modelCombo;
	
	int docId;
	String modelName;
	DocSelectorData docSelectorData;
	TextRecognitionConfig config = new TextRecognitionConfig(ModelUtil.PROVIDER_TRHTR);
	
	private static class TrHtrModel {
		public TrHtrModel(String name, int prodId, int testId) {
			super();
			this.name = name;
			this.prodId = prodId;
			this.testId = testId;
		}
		
		public String name;
		public int prodId;
		public int testId;
	}
	
	private static final List<TrHtrModel> MODELS = new ArrayList<>();
	static {
		MODELS.add(new TrHtrModel("T100", 0, 0));
		MODELS.add(new TrHtrModel("Berlin_1", 0, 0));
	}

	public TrHtrDialog(Shell parentShell) {
		super(parentShell);
	}
	
    protected void configureShell(Shell shell) {
        super.configureShell(shell);
        shell.setText("TrHtr Recognition");
    }
	
	@Override
	protected Point getInitialSize() {
		Point s = SWTUtil.getPreferredOrMinSize(getShell(), 300, 0);
		return new Point(Math.max(300, s.x+30), s.y+15);
	}
	
	@Override
	protected void setShellStyle(int newShellStyle) {           
	    super.setShellStyle(SWT.CLOSE | SWT.MODELESS| SWT.BORDER | SWT.TITLE | SWT.MIN | SWT.RESIZE);
	    setBlockOnOpen(false);
	}
	
	@Override
	protected boolean isResizable() {
	    return true;
	}
	
	@Override
	protected Control createDialogArea(Composite parent) {
		Composite cont = (Composite) super.createDialogArea(parent);
		cont.setLayout(new GridLayout(2, false));
		
		dps = new CurrentTranscriptOrDocPagesOrCollectionSelector(cont, SWT.NONE, false, true, true);
		dps.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1));
		
		Label modelLbl = new Label(cont, 0);
		modelLbl.setText("Model: ");
		modelCombo = new Combo(cont, SWT.DROP_DOWN | SWT.READ_ONLY);
		
		List<String> modelNames = MODELS.stream().map(m -> m.name).collect(Collectors.toList());
		modelCombo.setItems(modelNames.toArray(new String[0]));
		modelCombo.select(0);
		modelCombo.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		
		return cont;
	}
	
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		createButton(parent, IDialogConstants.CANCEL_ID, "Cancel", false);
		
	    Button runBtn = createButton(parent, IDialogConstants.OK_ID, "Run...", false);
	    runBtn.setImage(Images.ARROW_RIGHT);
	}
	
	public String getSelectedModel() {
		return modelCombo.getText();
	}
	
	public int getSelectedModelIndex() {
		return modelCombo.getSelectionIndex();
	}
	
	@Override
	protected void okPressed() {
		this.modelName = modelCombo.getText();
		this.docSelectorData = dps.getData();
		if (this.docSelectorData == null) {
			return;
		}
		config.setHtrName(this.modelName);
		TrHtrModel m = MODELS.stream().filter(m1 -> m1.name.equals(this.modelName)).findFirst().orElse(null);
		if (m != null) {
			config.setHtrId(Storage.getInstance().isLoggedInAtTestServer() ? m.testId : m.prodId);
		}
		
		super.okPressed();
	}

	public CurrentTranscriptOrDocPagesOrCollectionSelector getDps() {
		return dps;
	}

	public DocSelectorData getDocSelectorData() {
		return docSelectorData;
	}

	public String getModelName() {
		return modelName;
	}
	
	public TextRecognitionConfig getConfig() {
		return config;
	}
	
}
