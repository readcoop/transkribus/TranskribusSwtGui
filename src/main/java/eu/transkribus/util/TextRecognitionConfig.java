package eu.transkribus.util;

import java.util.List;

import org.apache.commons.lang3.StringUtils;

import eu.transkribus.core.model.beans.enums.CreditSelectionStrategy;
import eu.transkribus.core.rest.JobConst;
import eu.transkribus.core.util.ModelUtil;
import eu.transkribus.swt_gui.htr.HtrDictionaryComposite;

public class TextRecognitionConfig {
	
	private String provider;
	//common
	private String language;
	//CITLab and UPVLC, TODO replace with LangResource-ID
	private String dictionary;
	private String languageModel;
	//CITlab and UPVLC
	private int htrId;
	private String htrName;
	private List<String> structures;

	private boolean useExistingLinePolygons = false;
	private boolean doLinePolygonSimplification = true;
	private boolean keepOriginalLinePolygons = false;
	private boolean doStoreConfMats = true;
	private boolean clearLines = true;
	private boolean doWordSeg = true;
	private int batchSize = 10;
	private boolean writeKwsIndexFiles = false;
	private int nBest = 1;
	private boolean doNotDeleteWorkDir = false;
	private CreditSelectionStrategy creditSelectionStrategy;
	private String b2pBackend = "Legacy";

	private Integer printedHtrId;
	private String printedLanguageModel;

	private boolean writeLineConfScore = false;
	private boolean writeWordConfScores = false;
	
	public TextRecognitionConfig(String provider) {
		this.provider = provider;
	}
	
	public boolean isDoLinePolygonSimplification() {
		return doLinePolygonSimplification;
	}

	public void setDoLinePolygonSimplification(boolean doLinePolygonSimplification) {
		this.doLinePolygonSimplification = doLinePolygonSimplification;
	}

	public boolean isUseExistingLinePolygons() {
		return useExistingLinePolygons;
	}

	public void setUseExistingLinePolygons(boolean useExistingLinePolygons) {
		this.useExistingLinePolygons = useExistingLinePolygons;
	}

	public boolean isKeepOriginalLinePolygons() {
		return keepOriginalLinePolygons;
	}



	public void setKeepOriginalLinePolygons(boolean keepOriginalLinePolygons) {
		this.keepOriginalLinePolygons = keepOriginalLinePolygons;
	}

	public boolean isDoStoreConfMats() {
		return doStoreConfMats;
	}
	
	public void setDoStoreConfMats(boolean doStoreConfMats) {
		this.doStoreConfMats = doStoreConfMats;
	}
	
	public boolean isClearLines() {
		return clearLines;
	}

	public void setClearLines(boolean clearLines) {
		this.clearLines = clearLines;
	}
	
	public boolean isDoWordSeg() {
		return doWordSeg;
	}

	public void setDoWordSeg(boolean doWordSeg) {
		this.doWordSeg = doWordSeg;
	}

	public void setBatchSize(int batchSize) {
		this.batchSize = batchSize;
	}
	
	public int getBatchSize() {
		return batchSize;
	}
	
	public boolean isWriteKwsIndexFiles() {
		return writeKwsIndexFiles;
	}

	public void setWriteKwsIndexFiles(boolean writeKwsIndexFiles) {
		this.writeKwsIndexFiles = writeKwsIndexFiles;
	}

	public int getNBest() {
		return nBest;
	}

	public void setNBest(int nBest) {
		this.nBest = nBest;
	}
	
	public boolean isDoNotDeleteWorkDir() {
		return doNotDeleteWorkDir;
	}

	public void setDoNotDeleteWorkDir(boolean doNotDeleteWorkDir) {
		this.doNotDeleteWorkDir = doNotDeleteWorkDir;
	}

	public boolean isCitlabHtrPlus() {
		return StringUtils.equals(provider, ModelUtil.PROVIDER_CITLAB_PLUS);
	}
	
	public boolean isCitlab() {
		return StringUtils.equals(provider, ModelUtil.PROVIDER_CITLAB_PLUS) || StringUtils.equals(provider, ModelUtil.PROVIDER_CITLAB);
	}
	
	public boolean isPyLaia() {
		return StringUtils.equals(provider, ModelUtil.PROVIDER_PYLAIA);
	}
	
	public boolean isTrHtr() {
		return StringUtils.equals(provider, ModelUtil.PROVIDER_TRHTR);
	}		

	public String getLanguage() {
		return language;
	}



	public void setLanguage(String language) {
		this.language = language;
	}


	public String getDictionary() {
		return dictionary;
	}



	public void setDictionary(String dictionary) {
		this.dictionary = dictionary;
	}



	public int getHtrId() {
		return htrId;
	}



	public void setHtrId(int htrId) {
		this.htrId = htrId;
	}



	public String getHtrName() {
		return htrName;
	}



	public void setHtrName(String htrName) {
		this.htrName = htrName;
	}
	
	public List<String> getStructures() {
		return structures;
	}

	public void setStructures(List<String> structures) {
		this.structures = structures;
	}
	

	public String getLanguageModel() {
		return languageModel;
	}

	public void setLanguageModel(String languageModel) {
		this.languageModel = languageModel;
	}

	@Override
	public String toString() {
		String str = getProviderLabel() + " HTR "+htrId+"\n"
				+ "\t- Net Name: " + htrName + "\n"
				+ "\t- Language: " + language + "\n"
				+ "\t- " + getDictOrLMLabel();
		if (printedHtrId != null) {
			str += "\n\t- Printed HTR: "+printedHtrId+"\n";
			str += "\t- Printed " + getLMLabel(printedLanguageModel);
		}
		return str;
	}
	
	private String getProviderLabel() {
//		return mode == Mode.CITlab ? "CITlab" : "PyLaia";
		String lbl = "";
		if (isCitlab()) {
			lbl = "CITlab";
		}
		else if (isPyLaia()) {
			lbl = "PyLaia";
		}
		else if (isTrHtr()) {
			lbl = "TrHtr";
		}
		else {
			lbl = provider;
		}
		return lbl;
	}
	
	public String getProvider() {
		return provider;
	}	
	
	private String getDictOrLMLabel() {
		return languageModel != null ? getLMLabel() : getDictLabel(); 
	}

	private String getLMLabel() {
		return getLMLabel(languageModel);
	}
	
	private String getLMLabel(String languageModel) {
		if(languageModel == null) {
			return "Language Model: "+HtrDictionaryComposite.NO_DICTIONARY;
		}
		else if (JobConst.PROP_TRAIN_DATA_LM_VALUE.equals(languageModel)) {
			return HtrDictionaryComposite.INTEGRATED_LM;
		}
		else {
			return "Language Model: "+languageModel;
		}
	}	
	
	private String getDictLabel() {
		String dictLabel;
		if(dictionary == null) {
			dictLabel = "Language Model: "+HtrDictionaryComposite.NO_DICTIONARY;
		} else if (JobConst.PROP_TRAIN_DATA_DICT_VALUE.equals(dictionary)) {
			dictLabel = HtrDictionaryComposite.INTEGRATED_DICTIONARY;
		} else if (JobConst.PROP_TRAIN_DATA_LM_VALUE.equals(dictionary)) {
			dictLabel = HtrDictionaryComposite.INTEGRATED_LM;
		} else {
			dictLabel = "Dictionary: " + dictionary;
		}
		return dictLabel;
	}

	public CreditSelectionStrategy getCreditSelectionStrategy() {
		return creditSelectionStrategy;
	}

	public void setCreditSelectionStrategy(CreditSelectionStrategy creditSelectionStrategy) {
		this.creditSelectionStrategy = creditSelectionStrategy;
	}

	public String getB2PBackend() {
		return b2pBackend;
	}

	public void setB2PBackend(String b2pBackend) {
		this.b2pBackend = b2pBackend;
	}

	// getter and setters for printedHtrId and printedLanguageModel
	public Integer getPrintedHtrId() {
		return printedHtrId;
	}

	public void setPrintedHtrId(Integer printedHtrId) {
		this.printedHtrId = printedHtrId;
	}

	public String getPrintedLanguageModel() {
		return printedLanguageModel;
	}

	public void setPrintedLanguageModel(String printedLanguageModel) {
		this.printedLanguageModel = printedLanguageModel;
	}

	public boolean isWriteLineConfScore() {
		return writeLineConfScore;
	}

	public void setWriteLineConfScore(boolean writeLineConfScore) {
		this.writeLineConfScore = writeLineConfScore;
	}

	public boolean isWriteWordConfScores() {
		return writeWordConfScores;
	}

	public void setWriteWordConfScores(boolean writeWordConfScores) {
		this.writeWordConfScores = writeWordConfScores;
	}

}
