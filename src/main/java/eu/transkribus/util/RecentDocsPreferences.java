package eu.transkribus.util;

import java.util.ArrayList;
import java.util.List;
import java.util.prefs.Preferences;

import org.apache.commons.lang3.StringUtils;

import eu.transkribus.swt_gui.mainwidget.storage.Storage;
import eu.transkribus.util.RecentDocsPreferences.RecentDoc;

public class RecentDocsPreferences {
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(RecentDocsPreferences.class);

	public static final String SEP = ";;;";
	public static final String SERVER_SEP = ",,,";
	public static final String RECENT_DOC_STRING = "LAST_DOC_";

	static Preferences prefNode = Preferences.userRoot().node("/trp/recent_docs");
	private static int nrDocs = 10;

	public static boolean showOnStartup = false;

	public static boolean isShowOnStartup() {
		return showOnStartup;
	}

	public static void setShowOnStartup(boolean showOnStartup) {
		RecentDocsPreferences.showOnStartup = showOnStartup;
	}

	private static List<String> values = new ArrayList<>();

	public static interface IRecentDocsPrefListener {
		void valuesChanged(List<String> values);
	}

	private static List<IRecentDocsPrefListener> listener = new ArrayList<>();
	
	public static final class RecentDoc {
		public String recentDocStr;
		
		public String docName;
		public int docId = -1;
		public int colId = -1;
		public int pageNr = -1;
		public String server;
		
		public RecentDoc(String recentDocStr) {
			this.recentDocStr = recentDocStr;
		}

		public String getLabel() {
			if (StringUtils.isEmpty(docName)) {
				return recentDocStr;
			}
			else {
				String lbl = docName;
				if (docId >= 0) {
					lbl += " (docId: " + docId + ", colId: " + colId;
					if (pageNr > 1) {
						lbl += ", pageNr: " + pageNr;
					}
					lbl += ")";
				}
				else {
					lbl += " (local doc)";
				}
				return lbl;
			}
		}
	}
	
	public static RecentDoc parseRecentDocString(String recentDocStr) {
		try {
			RecentDoc r = new RecentDoc(recentDocStr);
			if (StringUtils.isEmpty(recentDocStr)) {
				return r;
			}

			String docStr = "";
			String[] split1 = recentDocStr.trim().split(RecentDocsPreferences.SERVER_SEP);
			if (split1.length == 0) {
				return r;
			} else if (split1.length == 1) {
				docStr = recentDocStr.trim();
			} else if (split1.length > 1) {
				if (split1[0].trim().contains(SEP)) {
					docStr = split1[0].trim();
					r.server = split1[1].trim();
				}
				else {
					docStr = split1[0].trim();
				}
			}

			String[] split2 = docStr.split(SEP);
			if (split2.length >= 1) {
				r.docName = split2[0];
				if (split2.length >= 3) {
					r.docId = Integer.valueOf(split2[1]);
					r.colId = Integer.valueOf(split2[2]);
					if (split2.length >= 4) {
						r.pageNr = Integer.valueOf(split2[3]);
					}
				}
			}

			return r;
		} catch (Exception e) {
			logger.error("Error parsing recent doc: " + recentDocStr, e);
			RecentDoc r = new RecentDoc(recentDocStr);
			return r;
		}
	}	

	public static void addListener(IRecentDocsPrefListener l) {
		listener.add(l);
	}

	public static void removeListener(IRecentDocsPrefListener l) {
		listener.remove(l);
	}

	private static void onValuesChanged() {
		listener.forEach(l -> l.valuesChanged(values));
	}

	public static void init() {
		loadFromPreferences();
		onValuesChanged();
	}
	
	public static void pushRecentDoc(String docName, int docId, int colId, int pageNr) {
		String item = docName;
		if (docId >= 0) { // remote doc
			item += SEP+docId+SEP+colId;
			if (pageNr > 1) {
				item += SEP+pageNr;
			}
			item += SERVER_SEP + Storage.i().getCurrentServer();
		}
		push(item);
	}

	private static void push(String item) {
//		values.remove(item);
		values.removeIf(i -> i.startsWith(item));
		values.add(0, item + SERVER_SEP + Storage.i().getCurrentServer());

		if (values.size() > nrDocs) {
			values.remove(values.size() - 1);
		}

		storeToPreferences();
		onValuesChanged();
	}

	public static List<String> getItems() {
		return values;
	}
	
	public static List<RecentDoc> getRecentDocs(boolean filterByCurrentServer) {
		List<RecentDoc> rds = new ArrayList<>();
		for (String item : values) {
			RecentDoc r = RecentDocsPreferences.parseRecentDocString(item);
			if (r==null) {
				continue;
			}
			if (!filterByCurrentServer || r.docId < 0 || StringUtils.isEmpty(r.server) || StringUtils.equals(r.server, Storage.i().getCurrentServer())) {
				rds.add(r);
			}
		}
		return rds;
	}

	public int size() {
		return values.size();
	}

	private static void loadFromPreferences() {
		values.clear();
		for (int i = 0; i < nrDocs; i++) {

			String val = prefNode.get(RECENT_DOC_STRING + i, "");

			if (!val.equals("")) {
				values.add(val);
			} else {
				break;
			}
		}
	}

	private static void storeToPreferences() {
		for (int i = 0; i < nrDocs; i++) {
			if (i < values.size()) {
				prefNode.put(RECENT_DOC_STRING + i, (String) values.get(i));
			} else {
				prefNode.remove(RECENT_DOC_STRING + i);
			}
		}
	}

	public static void clearRecentDocs() {
		values.clear();
		storeToPreferences();
		onValuesChanged();
	}
}
