package eu.transkribus.swt.util;

import eu.transkribus.client.oidc.ITokenStorage;
import eu.transkribus.core.model.beans.auth.TrpUser;
import eu.transkribus.swt_gui.auth.LoginDialog;
import org.eclipse.jface.window.ApplicationWindow;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;

import java.util.Collections;
import java.util.List;

public class LoginDialogTest {
	public static void main(String[] args) {
		ApplicationWindow aw = new ApplicationWindow(null) {
			@Override
			protected Control createContents(Composite parent) {
				LoginDialog dialog = new LoginDialog(getShell(), null,"creds!", new String[0], new String[]{"test"},
						0) {
					@Override
					protected ITokenStorage getSsoTokenStore() {
						return new ITokenStorage() {

							@Override
							public List<String> getUsernames(String serverBaseUrl) {
								return Collections.singletonList("my.user@na.me");
							}

							@Override
							public String getStoredRefreshToken(String serverBaseUrl, String username) {
								return "1543";
							}

							@Override
							public void updateStoredRefreshToken(String serverBaseUrl, TrpUser user, String refreshToken) {}

							@Override
							public void removeStoredRefreshToken(String authServerUrl, String username) {}
						};
					}
				};
				dialog.open();
				return parent;
			}
		};
		aw.setBlockOnOpen(false);
		aw.open();

		Display.getCurrent().dispose();
	}
}


