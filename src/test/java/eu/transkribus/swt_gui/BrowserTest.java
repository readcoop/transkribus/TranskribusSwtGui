package eu.transkribus.swt_gui;

import eu.transkribus.client.oidc.TrpSsoHandler;
import eu.transkribus.swt_gui.auth.SwtBrowserDesktopProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

public class BrowserTest {
    private static final Logger logger = LoggerFactory.getLogger(BrowserTest.class);

    public static void main(String[] args) throws URISyntaxException, IOException {
        TrpSsoHandler.IDesktopProvider dp = new SwtBrowserDesktopProvider();
        dp.browse(new URI("https://account.readcoop.eu/auth"));
    }
}
